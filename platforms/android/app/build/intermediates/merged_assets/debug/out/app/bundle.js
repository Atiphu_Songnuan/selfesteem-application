require("./runtime.js");require("./vendor.js");module.exports =
(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["bundle"],{

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.css": "./app.css"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app-root-main.xml": "./app-root-main.xml",
	"./app-root.xml": "./app-root.xml",
	"./app.css": "./app.css",
	"./app.js": "./app.js",
	"./configs.js": "./configs.js",
	"./models/auth-service.js": "./models/auth-service.js",
	"./models/check-connection.js": "./models/check-connection.js",
	"./models/liveUpdate.js": "./models/liveUpdate.js",
	"./models/refresh-token.js": "./models/refresh-token.js",
	"./views/activity-page/activity.js": "./views/activity-page/activity.js",
	"./views/activity-page/activity.xml": "./views/activity-page/activity.xml",
	"./views/agerange-page/agerange.css": "./views/agerange-page/agerange.css",
	"./views/agerange-page/agerange.js": "./views/agerange-page/agerange.js",
	"./views/agerange-page/agerange.xml": "./views/agerange-page/agerange.xml",
	"./views/login-page/login-model.js": "./views/login-page/login-model.js",
	"./views/login-page/login.css": "./views/login-page/login.css",
	"./views/login-page/login.js": "./views/login-page/login.js",
	"./views/login-page/login.xml": "./views/login-page/login.xml",
	"./views/main-page/main-view-model.js": "./views/main-page/main-view-model.js",
	"./views/main-page/main.css": "./views/main-page/main.css",
	"./views/main-page/main.js": "./views/main-page/main.js",
	"./views/main-page/main.xml": "./views/main-page/main.xml",
	"./views/register-page/register.css": "./views/register-page/register.css",
	"./views/register-page/register.js": "./views/register-page/register.js",
	"./views/register-page/register.xml": "./views/register-page/register.xml"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$";

/***/ }),

/***/ "./app-root-main.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Frame id=\"main\" defaultPage=\"views/main-page/main\">\n</Frame>\n"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app-root-main.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./app-root-main.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app-root.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Frame id=\"login\" defaultPage=\"views/login-page/login\">\n</Frame>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app-root.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./app-root.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("~@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"comment","comment":"\nIn NativeScript, the app.css file is where you place CSS rules that\nyou would like to apply to your entire application. Check out\nhttp://docs.nativescript.org/ui/styling for a full list of the CSS\nselectors and properties you can use to style UI components.\n\n/*\nIn many cases you may want to use the NativeScript core theme instead\nof writing your own CSS rules. You can learn more about the \nNativeScript core theme at https://github.com/nativescript/theme\nThe imported CSS rules must precede all other types of rules.\n"},{"type":"import","import":"\"~@nativescript/theme/css/core.css\""},{"type":"comment","comment":" @import \"~@nativescript/theme/css/default.css\"; "},{"type":"comment","comment":" Place any CSS rules you want to apply on both iOS and Android here.\nThis is where the vast majority of your CSS code goes. "},{"type":"comment","comment":"\nThe following CSS rule changes the font size of all Buttons that have the\n\"-primary\" class modifier.\n"},{"type":"rule","selectors":["Button.-primary"],"declarations":[{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".kanit-text-font"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"color","value":"black"}]},{"type":"rule","selectors":[".kanit-text-font-semibold"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-SemiBold\""},{"type":"declaration","property":"color","value":"black"}]},{"type":"rule","selectors":[".header"],"declarations":[{"type":"comment","comment":" color: white; "},{"type":"declaration","property":"text-align","value":"center"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".fas"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Font Awesome 5 Free\", \"fa-solid-900\""},{"type":"declaration","property":"font-weight","value":"900"},{"type":"declaration","property":"font-size","value":"36"}]},{"type":"rule","selectors":[".far"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Font Awesome 5 Free\", \"fa-regular-400\""},{"type":"declaration","property":"font-weight","value":"400"},{"type":"comment","comment":" font-size: 10; "}]},{"type":"rule","selectors":[".btn-text-font"],"declarations":[{"type":"declaration","property":"font-size","value":"16px"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Medium\""}]},{"type":"rule","selectors":[".headerContent"],"declarations":[{"type":"declaration","property":"font-size","value":"22px"}]},{"type":"rule","selectors":[".listview-content"],"declarations":[{"type":"comment","comment":" background-image: url(\"app/views/main-page/src/checkin.jpg\") no-repeat; "},{"type":"declaration","property":"width","value":"100%"},{"type":"comment","comment":" height: 100%; "},{"type":"declaration","property":"margin","value":"8"},{"type":"comment","comment":" margin-left: 8;\n  margin-right: 8; "},{"type":"comment","comment":" margin-bottom: 8; "},{"type":"declaration","property":"border-radius","value":"8"},{"type":"comment","comment":" color: white; "}]},{"type":"rule","selectors":[".confirm-button"],"declarations":[{"type":"declaration","property":"font-size","value":"16"},{"type":"declaration","property":"font-weight","value":"bold"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"background-color","value":"#4a774e"},{"type":"declaration","property":"border-radius","value":"20"}]},{"type":"rule","selectors":[".error-confirm-button"],"declarations":[{"type":"declaration","property":"font-size","value":"16"},{"type":"declaration","property":"font-weight","value":"bold"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"background-color","value":"#4a774e"},{"type":"declaration","property":"border-radius","value":"20"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./app.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
        let applicationCheckPlatform = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("../node_modules/@nativescript/core/ui/frame/frame.js");
__webpack_require__("../node_modules/@nativescript/core/ui/frame/activity.js");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (true) {
            const hmrUpdate = __webpack_require__("../node_modules/nativescript-dev-webpack/hmr/index.js").hmrUpdate;
            global.__coreModulesLiveSync = global.__onLiveSync;

            global.__onLiveSync = function () {
                // handle hot updated on LiveSync
                hmrUpdate();
            };

            global.hmrRefresh = function({ type, path } = {}) {
                // the hot updates are applied, ask the modules to apply the changes
                setTimeout(() => {
                    global.__coreModulesLiveSync({ type, path });
                });
            };

            // handle hot updated on initial app start
            hmrUpdate();
        }
        
            const context = __webpack_require__("./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$");
            global.registerWebpackModules(context);
            if (true) {
                module.hot.accept(context.id, () => { 
                    console.log("HMR: Accept module '" + context.id + "' from '" + module.i + "'"); 
                });
            }
            
        __webpack_require__("../node_modules/@nativescript/core/bundle-entry-points.js");
        let application = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const LiveUpdate = __webpack_require__("./models/liveUpdate.js");
const { configureOAuthProviders } = __webpack_require__("./models/auth-service.js");
global.conf = __webpack_require__("./configs.js");

new LiveUpdate(global.conf.updateKey);
configureOAuthProviders();

let token = appSettings.getString("token", "tokenexpired");
// console.log("Root-User Token: " + token);
if (token != "tokenexpired") {
    application.run({ moduleName: "app-root-main" });
} else { 
    application.run({ moduleName: "app-root" });
}


/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./app.js" });
    });
} 
    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./configs.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {config = {
  name: "First Check",
  ver: "Version 1.1 (build 20200616)",

  api: {
    // access: "https://medhr.medicine.psu.ac.th/app-api/v1/?/access",
    // profile: "https://medhr.medicine.psu.ac.th/app-api/v1/?/info",
    // display: "https://medhr.medicine.psu.ac.th/app-api/v1/?/display",

    login: "https://4421bf2fbe5e.ngrok.io/api/login",
    token: "https://4421bf2fbe5e.ngrok.io/api/token",
    refreshtoken: "https://4421bf2fbe5e.ngrok.io/api/refreshtoken",
    userinfo: "https://4421bf2fbe5e.ngrok.io/api/userinfo",
    // getmispin: "https://1aefbf39098f.ngrok.io/api/getmispin",
    // firstcheck: "https://1aefbf39098f.ngrok.io/api/curfirstcheck",
    // firstcheckhistory: "https://1aefbf39098f.ngrok.io/api/history",
    // firstcheckconfirm: "https://1aefbf39098f.ngrok.io/api/confirm",
    // firstcheckduplicated: "https://1aefbf39098f.ngrok.io/api/duplicated",
    // drugname: "https://1aefbf39098f.ngrok.io/api/drugname",
    // drugmistake: "https://1aefbf39098f.ngrok.io/api/drugmistake",
    // newdrugmistake: "https://1aefbf39098f.ngrok.io/api/newdrugmistake",
    // updatedrugmistake:
    //     "https://1aefbf39098f.ngrok.io/api/updatedrugmistake",
    // predispnumber: "https://1aefbf39098f.ngrok.io/api/predispnumber",
    // updatedrugmistakeamount:
    //     "https://1aefbf39098f.ngrok.io/api/updatedrugmistakeamount",
    // sendreport: "https://1aefbf39098f.ngrok.io/api/sendreport",

    // login: "http://61.19.201.20:19539/firstcheck/stafflogin",
    // getmispin: "http://61.19.201.20:19539/firstcheck/getmispin",
    // firstcheck: "http://61.19.201.20:19539/firstcheck/curfirstcheck",
    // firstcheckhistory: "http://61.19.201.20:19539/firstcheck/history",
    // firstcheckconfirm: "http://61.19.201.20:19539/firstcheck/confirm",
    // firstcheckduplicated: "http://61.19.201.20:19539/firstcheck/duplicated",
    // drugname: "http://61.19.201.20:19539/firstcheck/drugname",
    // drugmistake: "http://61.19.201.20:19539/firstcheck/drugmistake",
    // newdrugmistake: "http://61.19.201.20:19539/firstcheck/newdrugmistake",
    // updatedrugmistake: "http://61.19.201.20:19539/firstcheck/updatedrugmistake",
    // predispnumber:"http://61.19.201.20:19539/firstcheck/predispnumber",
    // updatedrugmistakeamount:"http://61.19.201.20:19539/firstcheck/updatedrugmistakeamount",
    // sendreport:"http://61.19.201.20:19539/firstcheck/sendreport"
  },

  updateKey: {
    ios: "lDTkDroiiAWdOa89adEaL37vALFFpbVYic0w5",
    android: "u0LSSDirU2FAgOpymSeo9WREqxo9pbVYic0w5",
  },
};

module.exports = config;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./configs.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./configs.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./models/auth-service.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const LoginViewModel = __webpack_require__("./views/login-page/login-model.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const { configureTnsOAuth, TnsOAuthClient } = __webpack_require__("../node_modules/nativescript-oauth2/oauth.js");
const {
  TnsOaProvider,
  TnsOaProviderOptionsGoogle,
  TnsOaProviderGoogle,
  TnsOaProviderOptionsFacebook,
  TnsOaProviderFacebook,
} = __webpack_require__("../node_modules/nativescript-oauth2/providers/index.js");

let user = LoginViewModel();
let client = null;

exports.configureOAuthProviders = () => {
  const googleProvider = configureOAuthProviderGoogle();
  const facebookProvider = configureOAuthProviderFacebook();
  configureTnsOAuth([googleProvider, facebookProvider]);
};

function configureOAuthProviderGoogle() {
  const googleProviderOptions = {
    openIdSupport: "oid-full",
    clientId:
      "196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth.apps.googleusercontent.com",
    redirectUri:
      "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth:/auth",
    urlScheme:
      "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth",
    scopes: ["email"],
  };
  const googleProvider = new TnsOaProviderGoogle(googleProviderOptions);
  return googleProvider;
}

function configureOAuthProviderFacebook() {
  const facebookProviderOptions = {
    openIdSupport: "oid-none",
    clientId: "877532503002711",
    clientSecret: "2e969e6f9036555e114fc9225df55cdb",
    redirectUri: "https://www.facebook.com/connect/login_success.html",
    scopes: ["email"],
  };
  const facebookProvider = new TnsOaProviderFacebook(facebookProviderOptions);
  return facebookProvider;
}

exports.tnsOauthLogin = (providerType) => {
  client = new TnsOAuthClient(providerType);
  client.loginWithCompletion((tokenResult, err) => {
    if (err) {
      console.error("loging in somthings went wrong!");
      console.error(err);
    } else {
      console.log("=====================================");
      if (tokenResult.accessToken != "") {
        let accessToken = tokenResult.accessToken;
        appSettings.setString("token", accessToken);
        switch (providerType) {
          case "google":
            // *** Get info โดยใช้ "idToken"
            let idToken = tokenResult.idToken;
            user
              .googleIdTokenInfo(idToken)
              .catch(function (error) {
                console.log(error);
                return Promise.reject();
              })
              .then(function (response) {
                let key = Object.keys(response.data);
                if (key.length > 2) {
                  appSettings.setString("social", "google");
                  let gmail = response.data.email.split("@");
                  appSettings.setString("name", gmail[0]);
                } else {
                  console.log("Invalid Token");
                }
              });
            break;
          case "facebook":
            // *** Get info โดยใช้ "accesstoken"
            user
              .facebookIdTokenInfo(accessToken)
              .catch(function (error) {
                console.log(error);
                return Promise.reject();
              })
              .then(function (response) {
                let key = Object.keys(response.data);
                if (key[0] != "error") {
                  appSettings.setString("social", "facebook");
                  appSettings.setString("name", response.data.name);
                } else {
                  console.log("Invalid Token");
                }
              });
            break;
          default:
            break;
        }
      }
    }
  });
};

exports.tnsOauthLogout = () => {
  if (client) {
    console.log("===Client===");
    console.log(client);
    client.logout();
  }
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./models/auth-service.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./models/auth-service.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./models/check-connection.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const Observable = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").Observable;
const connectivityModule = __webpack_require__("../node_modules/@nativescript/core/connectivity/connectivity.js");
const dialogs = __webpack_require__("../node_modules/@nativescript/core/ui/dialogs/dialogs.js");

function Connection() {
    let viewModel = new Observable();
    viewModel.checkConnection = () => {
        const type = connectivityModule.getConnectionType();
        let message;
        let connectionStatus = true;
        switch (type) {
            case connectivityModule.connectionType.none:
                message = "No connection!";
                connectionStatus = false;
                break;
            case connectivityModule.connectionType.wifi:
                message = "Wifi connection";
                connectionStatus = true;
                break;
            case connectivityModule.connectionType.mobile:
                message = "3G/4G/5G connection";
                connectionStatus = true;
                break;
            default:
                break;
        }

        connectivityModule.startMonitoring((newConnection) => {
            switch (newConnection) {
                case connectivityModule.connectionType.none:
                    console.log("No connection!");
                    connectionStatus = false;
                    break;
                case connectivityModule.connectionType.wifi:
                    console.log("Connection via wifi");
                    connectionStatus = true;
                    break;
                case connectivityModule.connectionType.mobile:
                    console.log("Connection via 3G/4G/5G");
                    connectionStatus = true;
                    break;
                default:
                    break;
            }
        });

        connectivityModule.stopMonitoring();

        let result = {
            connected: connectionStatus
        };
        return result;
    };
    return viewModel;
}

module.exports = {
    Connection: Connection,
}; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./models/check-connection.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./models/check-connection.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./models/liveUpdate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const application = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
const AppSync = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").AppSync;
const InstallMode = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").InstallMode;
const SyncStatus  = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").SyncStatus ;
const platform = __webpack_require__("../node_modules/@nativescript/core/platform/platform.js");

function liveUpdate(config) {
    if (platform.isIOS) {
        console.log("IOS device token: " + config.ios);
    } else {
        console.log("Android device token: " + config.android);
    }
    application.on(application.resumeEvent, function() {
        AppSync.sync({
            enabledWhenUsingHmr: false,
            deploymentKey: platform.isIOS ? config.ios : config.android,
            installMode: InstallMode.IMMEDIATE,
            mandatoryInstallMode: platform.isIOS
                ? InstallMode.ON_NEXT_RESUME
                : InstallMode.IMMEDIATE,
            updateDialog: {
                optionalUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                updateTitle: "อัพเดทเวอร์ชันใหม่",
                mandatoryUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                optionalIgnoreButtonLabel: "ยกเลิก",
                mandatoryContinueButtonLabel: platform.isIOS
                    ? "ปิด"
                    : "รีสตาร์ท",
                appendReleaseDescription: true
            }
        },
         (syncStatus) => {
            if (syncStatus === SyncStatus.UP_TO_DATE) {
              console.log("No pending updates; you’re running the latest version.");
            } else if (syncStatus === SyncStatus.UPDATE_INSTALLED) {
              console.log("Update found and installed. It will be activated upon next cold reboot");
            }
         });
    });
}

module.exports = liveUpdate;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./models/liveUpdate.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./models/liveUpdate.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./models/refresh-token.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const LoginViewModel = __webpack_require__("./views/login-page/login-model.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
let user = LoginViewModel();

function refreshToken() {
  user
    .refresh()
    .catch(function (error) {
      console.log(error);
      return Promise.reject();
    })
    .then(function (response) {
      if (response != null) {
        let data = response.data;
        if (data.status) {
          appSettings.setString("token", data.token);
        } else {
          appSettings.setString("token", "tokenexpired");
        }
      }
    });
}

module.exports = refreshToken;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./models/refresh-token.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./models/refresh-token.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"main":"app.js","android":{"v8Flags":"--expose_gc","markingMode":"none"}};

/***/ }),

/***/ "./views/activity-page/activity.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/activity-page/activity.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/activity-page/activity.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/activity-page/activity.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page loaded=\"onPageLoaded\" xmlns:calendar=\"nativescript-ui-calendar\" actionBarHidden=\"true\" xmlns:lv=\"nativescript-ui-listview\" androidStatusBarBackground=\"#415c5a\">\n   \n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/activity-page/activity.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/activity-page/activity.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/agerange-page/agerange.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".button"],"declarations":[{"type":"declaration","property":"border-radius","value":"15"},{"type":"declaration","property":"background-color","value":"#ff8a65"},{"type":"declaration","property":"color","value":"white"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/agerange-page/agerange.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/agerange-page/agerange.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/agerange-page/agerange.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {// Spinner
let spinnerbackground;
let spinner;

let page;
exports.onPageLoaded = (args) => {
  page = args.object.page;
  // spinnerbackground = page.getViewById("spinnerbackground");
  // spinner = page.getViewById("spinner");
  // processingDisable();
};

exports.back = () => {
  console.log("Back to homepage");
  page.frame.goBack();
};

function processingEnable() {
  spinner.busy = true;
  spinner.visibility = "visible";
  spinnerbackground.visibility = "visible";
  //   bottomNavigation.opacity = "0.3";
}

function processingDisable() {
  spinner.busy = false;
  spinner.visibility = "collapse";
  spinnerbackground.visibility = "collapse";
  //   bottomNavigation.opacity = "1";
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/agerange-page/agerange.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/agerange-page/agerange.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/agerange-page/agerange.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page loaded=\"onPageLoaded\" androidStatusBarBackground=\"#415c5a\">\n    <Page.actionBar>\n        <ActionBar id=\"actionbar\" backgroundColor=\"#415c5a\" color=\"white\">\n            <Label class=\"header\" text=\"เลือกช่วงอายุ\"></Label>\n            <NavigationButton android.systemIcon=\"ic_menu_back\" tap=\"back\" ios.position=\"left\" />\n            <!-- <ActionItem class=\"fas\" id=\"reloadicon\" icon=\"font://&#xf2f1;\" style=\"font-size: 8em;\" tap=\"refreshList\" ios.position=\"right\" />\n            <ActionItem class=\"fas\" id=\"calendaricon\" icon=\"font://&#xf073;\" style=\"font-size: 8em;\" tap=\"showMonthDialog\" ios.position=\"right\" /> -->\n        </ActionBar>\n    </Page.actionBar>\n\n    <AbsoluteLayout>\n        <StackLayout height=\"100%\" width=\"100%\">\n            <Button class=\"button kanit-text-font m-t-8\" text=\"2-5 ปี\" to=\"\" tap=\"navigateTo\"></Button>\n            <Button class=\"button kanit-text-font m-t-8\" text=\"6-11 ปี\" to=\"agerange\" tap=\"navigateTo\"></Button>\n            <Button class=\"button kanit-text-font m-t-8\" text=\"12-25 ปี\" to=\"agerange\" tap=\"navigateTo\"></Button>\n        </StackLayout>\n    </AbsoluteLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/agerange-page/agerange.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/agerange-page/agerange.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/login-page/login-model.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const Observable = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").Observable;
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const http = __webpack_require__("../node_modules/@nativescript/core/http/http.js");

function User() {
  let viewModel = new Observable();
  // viewModel.set("btnlogin", "เข้าสู่ระบบ");
  let result = new Array();
  viewModel.googleIdTokenInfo = (token) => {
    //   console.log("========================");
    //   console.log("https://oauth2.googleapis.com/tokeninfo?id_token="+idToken);
    return http
      .request({
        url: "https://oauth2.googleapis.com/tokeninfo?id_token=" + token,
        method: "GET",
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          // console.log(res);
          result = {
            statusCode: 200,
            data: res,
          };
          return result;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.facebookIdTokenInfo = (token) => {
    //   console.log("========================");
    //   console.log("https://oauth2.googleapis.com/tokeninfo?id_token="+idToken);
    return http
      .request({
        url: "https://graph.facebook.com/me?access_token=" + token,
        method: "GET",
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          // console.log(res);
          result = {
            statusCode: 200,
            data: res,
          };
          return result;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  // viewModel.getmispin = () => {
  //     return http
  //         .request({
  //             url: global.conf.api.getmispin,
  //             method: "POST",
  //             headers: {
  //                 "Content-Type": "application/json",
  //             },
  //             content: JSON.stringify({
  //                 perid: viewModel.get("perid"),
  //             }),
  //         })
  //         .then(
  //             (response) => {
  //                 var res = response.content.toJSON();
  //                 // console.log(res);
  //                 if (res.statusCode == 200) {
  //                     result = {
  //                         statusCode: 200,
  //                         data: res.data,
  //                     };
  //                 } else {
  //                     result = {
  //                         statusCode: 400,
  //                         data: res,
  //                     };
  //                 }
  //                 return result;
  //             },
  //             (e) => {
  //                 result = {
  //                     statusCode: 401,
  //                     data: JSON.stringify(e),
  //                 };
  //                 return result;
  //             }
  //         );
  // };

  viewModel.login = (username, password) => {
    return http
      .request({
        url: global.conf.api.login,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        content: JSON.stringify({
          username: username,
          password: password,
        }),
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.token = (payload) => {
    return http
      .request({
        url: global.conf.api.token,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        content: JSON.stringify({
          id: payload.PARENT_ID,
          name: payload.NAME,
          surname: payload.SURNAME,
          email: payload.EMAIL,
          phonenumber: payload.PHONENUMBER,
        }),
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          if (res.statusCode == 200) {
            appSettings.setString("token", res.token);
          }
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.refresh = (accesstoken) => {
    return http
      .request({
        url: global.conf.api.refreshtoken,
        method: "POST",
        headers: {
          Authorization: "Bearer " + accesstoken,
        },
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          if (res.statusCode == 200) { 
            appSettings.setString("token", res.data.token);
          }
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.userinfo = (accesstoken) => {
    return http
      .request({
        url: global.conf.api.userinfo,
        method: "POST",
        headers: {
          Authorization: "Bearer " + accesstoken,
        },
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          if (res.statusCode == 200) {
            appSettings.setString("name", res.data.NAME + " " + res.data.SURNAME);
          }
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  // viewModel.profile = () => {
  //     return http
  //         .request({
  //             url: global.conf.api.profile,
  //             method: "GET",
  //             headers: {
  //                 Authorization:
  //                     "Bearer " +
  //                     appSettings.getString("token", "tokenexpired"),
  //             },
  //         })
  //         .then(
  //             (response) => {
  //                 var res = response.content.toJSON();
  //                 if (res.length == 0) {
  //                     result = {
  //                         statusCode: 400,
  //                         data: res,
  //                     };
  //                 } else {
  //                     result = {
  //                         statusCode: 200,
  //                         data: res,
  //                     };
  //                 }
  //                 return result;
  //             },
  //             (e) => {
  //                 result = {
  //                     statusCode: 401,
  //                     data: JSON.stringify(e),
  //                 };
  //                 return result;
  //             }
  //         );
  // };

  return viewModel;
}

module.exports = User;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/login-page/login-model.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/login-page/login-model.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/login-page/login.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".page"],"declarations":[{"type":"declaration","property":"align-items","value":"center"},{"type":"declaration","property":"flex-direction","value":"column"},{"type":"declaration","property":"position","value":"relative"},{"type":"declaration","property":"background-color","value":"#415c5a"},{"type":"comment","comment":" background-image: url(\"~/src/img/background2.jpg\"); "},{"type":"comment","comment":" background-image: url('https://images.pexels.com/photos/1624496/pexels-photo-1624496.jpeg'); "},{"type":"comment","comment":" background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover; "}]},{"type":"rule","selectors":[".form"],"declarations":[{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"flex-grow","value":"2"},{"type":"declaration","property":"vertical-align","value":"middle"}]},{"type":"rule","selectors":[".logo"],"declarations":[{"type":"declaration","property":"margin-bottom","value":"12"},{"type":"declaration","property":"height","value":"120"},{"type":"declaration","property":"font-weight","value":"bold"}]},{"type":"rule","selectors":[".header"],"declarations":[{"type":"declaration","property":"horizontal-align","value":"center"},{"type":"declaration","property":"font-size","value":"24"},{"type":"declaration","property":"font-weight","value":"600"},{"type":"declaration","property":"margin-bottom","value":"18"},{"type":"declaration","property":"text-align","value":"center"},{"type":"comment","comment":" color: #8ebbbc; "},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""}]},{"type":"rule","selectors":[".input-style"],"declarations":[{"type":"declaration","property":"padding","value":"12 20"},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"margin-bottom","value":"8"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"comment","comment":" placeholder-color: lightgray; "},{"type":"comment","comment":" color: white; "}]},{"type":"rule","selectors":[".social"],"declarations":[{"type":"declaration","property":"border-radius","value":"100"}]},{"type":"rule","selectors":[".btn-img"],"declarations":[{"type":"declaration","property":"border-radius","value":"5"},{"type":"declaration","property":"border-width","value":"1"},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"font-size","value":"16"}]},{"type":"rule","selectors":[".login-button"],"declarations":[{"type":"declaration","property":"font-size","value":"18"},{"type":"declaration","property":"font-weight","value":"bold"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"background-color","value":"#bddae6"},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"margin-top","value":"8"}]},{"type":"rule","selectors":[".register-button"],"declarations":[{"type":"declaration","property":"font-size","value":"18"},{"type":"declaration","property":"font-weight","value":"bold"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"background-color","value":"tomato"},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"margin-top","value":"8"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/login-page/login.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/login-page/login.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/login-page/login.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const LoginViewModel = __webpack_require__("./views/login-page/login-model.js");
// const fromObject = require("tns-core-modules/data/observable").fromObject;
// let user = LoginViewModel();
const nstoasts = __webpack_require__("../node_modules/nativescript-toasts/index.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const { tnsOauthLogin } = __webpack_require__("./models/auth-service.js");
const dialogModule = __webpack_require__("../node_modules/@nativescript/core/ui/dialogs/dialogs.js");
// const { login } = require("tns-core-modules/ui/dialogs");

let user = LoginViewModel();
let page;

let username;
let password;
exports.onPageLoaded = (args) => {
  page = args.object.page;
  page.bindingContext = user;
  // page.bindingContext = user;
  username = page.getViewById("username");
  password = page.getViewById("password");
  let token = appSettings.getString("token", "tokenexpired");
  if (token != "tokenexpired") {
    // console.log("ไปหน้าหลัก");
    const navigationEntry = {
      moduleName: "views/main-page/main",
      clearHistory: true,
      animated: true,
      transition: {
        name: "fade",
        duration: 500,
        curve: "linear",
      },
    };
    page.frame.navigate(navigationEntry);
  }
};

exports.onRegister = () => {
  // console.log("Register Page");
  const navigationEntry = {
    moduleName: "views/register-page/register",
    clearHistory: true,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
};

exports.onLogin = () => {
  const navigationEntry = {
    moduleName: "views/main-page/main",
    clearHistory: true,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
  // user
  //   .login(username.text, password.text)
  //   .catch(function (error) {
  //     console.log(error);
  //     return Promise.reject();
  //   })
  //   .then(function (response) {
  //     if (response.statusCode == 200) {
  //       if (response.data.length != 0) {
  //         let payload = response.data[0];
  //         console.log(payload);
  //         // เช็คว่ามี token แล้วหรือยัง
  //         // ถ้ายังไม่มีให้ทำการสร้าง token แล้วเก็บลง Database
  //         // ถ้ามีแล้วให้นำ token เดิมไปทำการ refresh ก่อนแล้วค่อยเก็บลง Database
  //         if (payload.TOKEN != "") {
  //           console.log("===Refresh Token===");
  //           user
  //             .refresh(payload.TOKEN)
  //             .catch((error) => {
  //               console.log(error);
  //               return Promise.reject();
  //             })
  //             .then((refreshresponse) => {
  //               if (refreshresponse.statusCode == 200) {
  //                 LoginSuccess();
  //               } else {
  //                 console.log("Refresh token failed!");
  //               }
  //             });
  //         } else {
  //           console.log("===Generate new token===");
  //           user
  //             .token(payload)
  //             .catch((error) => {
  //               console.log(error);
  //               return Promise.reject();
  //             })
  //             .then((tokenresponse) => {
  //               console.log(tokenresponse);
  //               if (tokenresponse.statusCode == 200) {
  //                 LoginSuccess();
  //               } else {
  //                 console.log("Generate new token failed!");
  //               }
  //             });
  //         }
  //       } else {
  //         console.log("Not found user data");
  //         // dialogModule.alert({
  //         //   message:
  //         //     "ไม่พบข้อมูลผู้ใช้, กรุณาลงทะเบียนเพื่อใช้งานก่อน",
  //         //   okButtonText: "OK",
  //         // });
  //       }
  //     } else {
  //       console.log("Log in failed!");
  //       dialogModule.alert({
  //         message:
  //           "Username หรือ Password ไม่ถูกต้อง, กรุณาตรวจสอบและลองใหม่อีกครั้ง",
  //         okButtonText: "OK",
  //       });
  //     }
  //   });
};

exports.onGoogleLogin = () => {
  tnsOauthLogin("google");
};

exports.onFacebookLogin = () => {
  tnsOauthLogin("facebook");
};

function LoginSuccess() {
  user
    .userinfo(appSettings.getString("token"))
    .catch((error) => {
      console.log(error);
      return Promise.reject();
    })
    .then((inforesponse) => {
      // console.log(inforesponse);
      if (inforesponse.statusCode == 200) {
        let options = {
          text: "เข้าสู่ระบบสำเร็จ",
          duration: nstoasts.DURATION.SHORT,
          position: nstoasts.POSITION.BOTTOM, //optional
        };
        nstoasts.show(options);

        const navigationEntry = {
          moduleName: "views/main-page/main",
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 380,
            curve: "easeIn",
          },
        };
        page.frame.navigate(navigationEntry);
      }
    });
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/login-page/login.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/login-page/login.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/login-page/login.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page loaded=\"onPageLoaded\" actionBarHidden=\"true\" androidStatusBarBackground=\"#415c5a\">\n\n    <FlexboxLayout class=\"page\">\n        <StackLayout class=\"form m-l-8 m-r-8\">\n            <Image class=\"logo\" src=\"~/src/img/firstcheck.png\" />\n            <Label class=\"header\" text=\"Self Esteem\" />\n            <TextField id=\"username\" class=\"kanit-text-font input-style\" hint=\"ชื่อผู้ใช้\"></TextField>\n            <TextField id=\"password\" secure=\"true\" class=\"kanit-text-font input-style\" hint=\"รหัสผ่าน\"></TextField>\n            <Button class=\"login-button\" text=\"เข้าสู่ระบบ\" tap=\"onLogin\"></Button>\n            <GridLayout class=\"m-t-8\" horizontalAlignment=\"center\" columns=\"auto, auto, auto\" rows=\"auto\">\n                <Label class=\"kanit-text-font-semibold text-center\" col=\"0\" row=\"0\" text=\"ลงทะเบียน\" color=\"white\" tap=\"onRegister\"></Label>\n                <Label col=\"1\" row=\"0\" text=\" | \" color=\"white\" />\n                <Label class=\"kanit-text-font-semibold text-right\" col=\"2\" row=\"0\" text=\"ลืมรหัสผ่าน\" color=\"tomato\" />\n            </GridLayout>\n\n            <GridLayout class=\"m-t-8 m-l-8 m-r-8\" horizontalAlignment=\"center\" columns=\"auto, auto, auto\" rows=\"auto\">\n                <StackLayout col=\"0\" row=\"0\" height=\"2\" width=\"25%\" backgroundColor=\"#a2a2a2\" />\n                <Label class=\"text-center m-l-8 m-r-8\" col=\"1\" row=\"0\" text=\"OR CONECT WITH\" color=\"#a2a2a2\" />\n                <StackLayout col=\"2\" row=\"0\" height=\"2\" width=\"25%\" backgroundColor=\"#a2a2a2\" />\n            </GridLayout>\n            <GridLayout horizontalAlignment=\"center\" columns=\"auto, auto\" rows=\"auto\">\n                <Image class=\"social m-r-16\" stretch=\"fill\" tap=\"onFacebookLogin\" col=\"0\" row=\"0\" src=\"~/src/img/facebook.png\" width=\"48\" height=\"48\" boderRadius=\"100\" />\n                <!-- <Image class=\"m-16 social\" stretch=\"fill\" col=\"1\" row=\"0\" src=\"~/src/img/line.png\" width=\"48\" height=\"48\" /> -->\n                <Image class=\"social\" stretch=\"fill\" tap=\"onGoogleLogin\" col=\"1\" row=\"0\" src=\"~/src/img/google.png\" backgroundColor=\"white\" width=\"48\" height=\"48\" />\n            </GridLayout>\n        </StackLayout>\n    </FlexboxLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/login-page/login.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/login-page/login.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main-page/main-view-model.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const Observable = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js");

function MainData() {
  let viewModel = new Observable();
  let result = array();
  viewModel.facebooklogout = (token) => {
    return http
      .request({
        url: "https://www.facebook.com/logout.php?access_token=" + token,
        method: "GET",
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          console.log(res);
          result = {
            statusCode: 200,
            data: res,
          };
          return result;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };
  return viewModel;
}

module.exports = {
  MainData: MainData,
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main-page/main-view-model.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main-page/main-view-model.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main-page/main.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".header"],"declarations":[{"type":"comment","comment":" color: white; "},{"type":"declaration","property":"text-align","value":"center"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".tab-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-SemiBold\""},{"type":"declaration","property":"font-size","value":"14"}]},{"type":"rule","selectors":[".button"],"declarations":[{"type":"declaration","property":"border-radius","value":"15"},{"type":"declaration","property":"background-color","value":"#ff8a65"},{"type":"declaration","property":"color","value":"black"}]},{"type":"rule","selectors":[".ripple-margin-top"],"declarations":[{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"margin-top","value":"15"},{"type":"declaration","property":"margin-left","value":"15"},{"type":"declaration","property":"margin-right","value":"15"},{"type":"declaration","property":"border-radius","value":"8 8 0 0"}]},{"type":"rule","selectors":[".ripple-margin-middle"],"declarations":[{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"margin-left","value":"15"},{"type":"declaration","property":"margin-right","value":"15"},{"type":"comment","comment":" border-radius: 8 8 8 8; "}]},{"type":"rule","selectors":[".ripple-margin-bottom"],"declarations":[{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"margin-left","value":"15"},{"type":"declaration","property":"margin-right","value":"15"},{"type":"declaration","property":"border-radius","value":"0 0 8 8"}]},{"type":"rule","selectors":[".topmenu-content"],"declarations":[{"type":"comment","comment":" background-image: url(\"app/views/main-page/src/checkin.jpg\") no-repeat; "},{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"height","value":"50"},{"type":"declaration","property":"border-radius","value":"8 8 0 0"},{"type":"declaration","property":"border-width","value":"0 0 1 0"},{"type":"declaration","property":"color","value":"black"},{"type":"declaration","property":"background-color","value":"#ff8a65"}]},{"type":"rule","selectors":[".middlemenu-content"],"declarations":[{"type":"comment","comment":" background-image: url(\"app/views/main-page/src/checkin.jpg\") no-repeat; "},{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"height","value":"50"},{"type":"declaration","property":"color","value":"black"},{"type":"comment","comment":" border-radius: 8 8 8 8; "},{"type":"declaration","property":"border-width","value":"0 0 1 0"},{"type":"declaration","property":"background-color","value":"#ff8a65"}]},{"type":"rule","selectors":[".bottommenu-content"],"declarations":[{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"height","value":"50"},{"type":"declaration","property":"border-radius","value":"0 0 8 8"},{"type":"declaration","property":"color","value":"black"},{"type":"comment","comment":" border-width: 0 0 1 0; "},{"type":"declaration","property":"background-color","value":"#ff8a65"}]},{"type":"rule","selectors":["TabStrip"],"declarations":[{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"color","value":"darkgray"}]},{"type":"rule","selectors":["TabStripItem.tabstripitem"],"declarations":[{"type":"declaration","property":"color","value":"darkgray"},{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"margin-top","value":"4"}]},{"type":"rule","selectors":["TabStripItem.tabstripitem:active"],"declarations":[{"type":"declaration","property":"color","value":"#8ca8b4"},{"type":"declaration","property":"background-color","value":"#f0ffff"},{"type":"declaration","property":"margin-top","value":"4"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main-page/main.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/main-page/main.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main-page/main.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const MainViewModel = __webpack_require__("./views/main-page/main-view-model.js");
const fromObject = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").fromObject;
let user = MainViewModel;
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const nstoasts = __webpack_require__("../node_modules/nativescript-toasts/index.js");
const dialogs = __webpack_require__("../node_modules/@nativescript/core/ui/dialogs/dialogs.js");
const { tnsOauthLogout } = __webpack_require__("./models/auth-service.js");

let page;
let bottomNavigation;

// Spinner
let spinner;
let spinnerbackground;

let sociallogo;
exports.onPageLoaded = (args) => {
  const page = args.object.page;
  // sociallogo = page.getViewById("sociallogo");
  // bottomNavigation = page.getViewById("bottomNavigation");
  // spinnerbackground = page.getViewById("spinnerbackground");
  // spinner = page.getViewById("spinner");

  let data = [];
  for (let index = 0; index < 15; index++) {
    data.push({
      itemName: index + 1,
      itemDescription: "Patient HN",
    });
  }

  const viewModel = fromObject({
    dataItems: data,
  });
  page.bindingContext = viewModel;
  // processingDisable();

  // // Check Token expired
  // let token = appSettings.getString("token", "tokenexpired");
  // if (token == "tokenexpired") {
  //   processingEnable();
  //   const navigationEntry = {
  //     moduleName: "views/login-page/login",
  //     clearHistory: true,
  //     animated: true,
  //     transition: {
  //       name: "fade",
  //       duration: 500,
  //       curve: "linear",
  //     },
  //   };
  //   page.frame.navigate(navigationEntry);
  // } else {
  //   let social = appSettings.getString("social");
  //   switch (social) {
  //     case "google":
  //       sociallogo.src = "~/src/img/google.png";
  //       break;
  //     case "facebook":
  //       sociallogo.src = "~/src/img/facebook.png";
  //       break;
  //     default:
  //       break;
  //   }
  //   let viewModel = fromObject({
  //     fullname: appSettings.getString("name"),
  //   });
  //   page.bindingContext = viewModel;
  //   processingDisable();
  // }
};

exports.navigateTo = (args) => {
  const namepage = args.object.to;
  const navigationEntry = {
    moduleName: "views/" + namepage + "-page/" + namepage,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
};

exports.logout = () => {
  // console.log(appSettings.getString("token"));
  dialogs
    .confirm({
      title: "ต้องการออกจากระบบ?",
      // message: "ต้องการออกจากระบบ?",
      okButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
      cancelable: false,
    })
    .then(function (result) {
      if (result) {
        let token = appSettings.getString("token", "tokenexpired");
        // user
        //   .facebooklogout(token)
        //   .catch(function (error) {
        //     console.log(error);
        //     return Promise.reject();
        //   })
        //   .then(function (response) {
        //     console.log(response);
        //   });

        tnsOauthLogout();
        console.log("ออกจากระบบสำเร็จ");
        appSettings.clear();
        var options = {
          text: "ออกจากระบบสำเร็จ",
          duration: nstoasts.DURATION.SHORT,
          position: nstoasts.POSITION.BOTTOM, //optional
        };
        nstoasts.show(options);
        const navigationEntry = {
          moduleName: "views/login-page/login",
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideRight",
            duration: 380,
            curve: "easeIn",
          },
        };
        page.frame.navigate(navigationEntry);
        // frameModule.Frame.topmost().navigate(navigationEntry);
      } else {
        console.log("ยกเลิกออกจากระบบ");
      }
    });
};

function processingEnable() {
  spinner.busy = true;
  spinner.visibility = "visible";
  spinnerbackground.visibility = "visible";
  bottomNavigation.opacity = "0.3";
}

function processingDisable() {
  spinner.busy = false;
  spinner.visibility = "collapse";
  spinnerbackground.visibility = "collapse";
  bottomNavigation.opacity = "1";
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main-page/main.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main-page/main.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main-page/main.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-ui-listview", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.itemTemplate", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });

module.exports = "<Page xmlns=\"http://schemas.nativescript.org/tns.xsd\" loaded=\"onPageLoaded\" xmlns:lv=\"nativescript-ui-listview\">\n    <ActionBar title=\"Self Esteem\"></ActionBar>\n    <!-- <StackLayout class=\"m-8\" height=\"100%\">\n        <lv:RadListView items=\"{{dataItems}}\">\n            <lv:RadListView.itemTemplate>\n                <StackLayout orientation=\"vertical\">\n                    <Label fontSize=\"20\" text=\"{{itemName}}\"></Label>\n                    <Label fontSize=\"14\" text=\"{{itemDescription}}\"></Label>\n                </StackLayout>\n            </lv:RadListView.itemTemplate>\n        </lv:RadListView>\n    </StackLayout> -->\n\n    <AbsoluteLayout backgroundColor=\"#eee\">\n        <BottomNavigation id=\"bottomNavigation\" selectedIndex=\"0\" opacity=\"1\" width=\"100%\" height=\"100%\">\n            <TabStrip>\n                <TabStripItem class=\"tabstripitem\">\n                    <Label class=\"kanit-text-font\" text=\"หน้าแรก\"></Label>\n                    <Image src=\"font://&#xf015;\" class=\"fas t-36\"></Image>\n                </TabStripItem>\n                <TabStripItem class=\"tabstripitem\">\n                    <Label class=\"kanit-text-font\" text=\"กิจกรรม \"></Label>\n                    <Image src=\"font://&#xf1b3;\" class=\"fas t-36\"></Image>\n                </TabStripItem>\n                <TabStripItem class=\"tabstripitem\">\n                    <Label class=\"kanit-text-font\" text=\"ตั้งค่า\"></Label>\n                    <Image src=\"font://&#xf013;\" class=\"fas t-36\"></Image>\n                </TabStripItem>\n            </TabStrip>\n\n            <TabContentItem>\n                <Label text=\"Radlistview\"></Label>\n                <GridLayout class=\"m-8\" height=\"100%\">\n                    <lv:RadListView items=\"{{dataItems}}\" height=\"100%\" backgroundColor=\"crimson\">\n                        <lv:RadListView.itemTemplate>\n                            <StackLayout orientation=\"vertical\">\n                                <Label fontSize=\"20\" text=\"{{itemName}}\"></Label>\n                                <Label fontSize=\"14\" text=\"{{itemDescription}}\"></Label>\n                            </StackLayout>\n                        </lv:RadListView.itemTemplate>\n                    </lv:RadListView>\n                </GridLayout>\n\n            </TabContentItem>\n\n            <TabContentItem></TabContentItem>\n\n            <TabContentItem></TabContentItem>\n        </BottomNavigation>\n    </AbsoluteLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main-page/main.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/main-page/main.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/register-page/register.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".page"],"declarations":[{"type":"declaration","property":"align-items","value":"center"},{"type":"declaration","property":"flex-direction","value":"column"},{"type":"declaration","property":"position","value":"relative"},{"type":"declaration","property":"background-color","value":"#415c5a"},{"type":"comment","comment":" background-image: url(\"~/src/img/background2.jpg\"); "},{"type":"comment","comment":" background-image: url('https://images.pexels.com/photos/1624496/pexels-photo-1624496.jpeg'); "},{"type":"comment","comment":" background-repeat: no-repeat;\n  background-position: center;\n  background-size: cover; "}]},{"type":"rule","selectors":[".form"],"declarations":[{"type":"declaration","property":"width","value":"100%"},{"type":"declaration","property":"flex-grow","value":"2"},{"type":"declaration","property":"vertical-align","value":"middle"}]},{"type":"rule","selectors":[".logo"],"declarations":[{"type":"declaration","property":"margin-bottom","value":"12"},{"type":"declaration","property":"height","value":"90"},{"type":"declaration","property":"font-weight","value":"bold"}]},{"type":"rule","selectors":[".header"],"declarations":[{"type":"declaration","property":"horizontal-align","value":"center"},{"type":"declaration","property":"font-size","value":"18"},{"type":"declaration","property":"font-weight","value":"600"},{"type":"declaration","property":"margin-bottom","value":"24"},{"type":"declaration","property":"text-align","value":"center"},{"type":"declaration","property":"color","value":"#8ebbbc"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""}]},{"type":"rule","selectors":[".input-style"],"declarations":[{"type":"declaration","property":"padding","value":"12 20"},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"margin-bottom","value":"8"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"comment","comment":" placeholder-color: lightgray; "},{"type":"comment","comment":" color: white; "}]},{"type":"rule","selectors":[".register-button"],"declarations":[{"type":"declaration","property":"font-size","value":"18"},{"type":"declaration","property":"font-weight","value":"bold"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"background-color","value":"tomato"},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"margin-top","value":"8"}]},{"type":"rule","selectors":[".back-button"],"declarations":[{"type":"declaration","property":"font-size","value":"18"},{"type":"declaration","property":"font-weight","value":"bold"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"comment","comment":" color: white; "},{"type":"declaration","property":"background-color","value":"#eee"},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"margin-top","value":"8"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/register-page/register.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/register-page/register.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/register-page/register.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {let page
exports.onPageLoaded = (args) => {
    page = args.object.page;
}

exports.onBackPreviousPage = () => {
    // console.log("Back to Home Page");
    const navigationEntry = {
        moduleName: "views/login-page/login",
        clearHistory: true,
        animated: true,
        transition: {
            name: "slideRight",
            duration: 380,
            curve: "easeIn",
        },
    };
    page.frame.navigate(navigationEntry);
}; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/register-page/register.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/register-page/register.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/register-page/register.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page loaded=\"onPageLoaded\" actionBarHidden=\"true\" androidStatusBarBackground=\"#1e4b25\">\n\n    <FlexboxLayout class=\"page\">\n        <StackLayout class=\"form m-l-8 m-r-8\">\n            <Label class=\"header\" text=\"ลงทะเบียน\" />\n            <TextField class=\"kanit-text-font input-style\" hint=\"อีเมล\"></TextField>\n            <TextField class=\"kanit-text-font input-style\" hint=\"รหัสผ่าน\"></TextField>\n            <TextField class=\"kanit-text-font input-style\" hint=\"ยืนยันรหัสผ่าน\"></TextField>\n            <Button class=\"register-button\" text=\"ลงทะเบียน\"></Button>\n            <Button class=\"back-button\" text=\"ยกเลิก\" tap=\"onBackPreviousPage\"></Button>\n        </StackLayout>\n    </FlexboxLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/register-page/register.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/register-page/register.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ })

},[["./app.js","runtime","vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLiBzeW5jIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvYXBwXFwuKGNzc3xzY3NzfGxlc3N8c2FzcykkIiwid2VicGFjazovLy9cXGJfW1xcdy1dKlxcLilzY3NzKSQiLCJ3ZWJwYWNrOi8vLy4vYXBwLXJvb3QtbWFpbi54bWwiLCJ3ZWJwYWNrOi8vLy4vYXBwLXJvb3QueG1sIiwid2VicGFjazovLy8uL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwLmpzIiwid2VicGFjazovLy8uL2NvbmZpZ3MuanMiLCJ3ZWJwYWNrOi8vLy4vbW9kZWxzL2F1dGgtc2VydmljZS5qcyIsIndlYnBhY2s6Ly8vLi9tb2RlbHMvY2hlY2stY29ubmVjdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9tb2RlbHMvbGl2ZVVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9tb2RlbHMvcmVmcmVzaC10b2tlbi5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9hY3Rpdml0eS1wYWdlL2FjdGl2aXR5LmpzIiwid2VicGFjazovLy8uL3ZpZXdzL2FjdGl2aXR5LXBhZ2UvYWN0aXZpdHkueG1sIiwid2VicGFjazovLy8uL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UuY3NzIiwid2VicGFjazovLy8uL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvYWdlcmFuZ2UtcGFnZS9hZ2VyYW5nZS54bWwiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbG9naW4tcGFnZS9sb2dpbi1tb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLmNzcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLmpzIiwid2VicGFjazovLy8uL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4ueG1sIiwid2VicGFjazovLy8uL3ZpZXdzL21haW4tcGFnZS9tYWluLXZpZXctbW9kZWwuanMiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbWFpbi1wYWdlL21haW4uY3NzIiwid2VicGFjazovLy8uL3ZpZXdzL21haW4tcGFnZS9tYWluLmpzIiwid2VicGFjazovLy8uL3ZpZXdzL21haW4tcGFnZS9tYWluLnhtbCIsIndlYnBhY2s6Ly8vLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLmNzcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLmpzIiwid2VicGFjazovLy8uL3ZpZXdzL3JlZ2lzdGVyLXBhZ2UvcmVnaXN0ZXIueG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRTs7Ozs7OztBQ3ZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUo7Ozs7Ozs7O0FDOUNBLHdGO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsOENBQThDO0FBQ3pFLEtBQUs7QUFDTCxDOzs7Ozs7Ozs7QUNQQSx5RjtBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHlDQUF5QztBQUNwRSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNSQSwrR0FBaUUsbUJBQU8sQ0FBQyxrREFBa0M7QUFDM0csZ0VBQWdFLG1CQUFPLENBQUMsa0RBQWtDLEdBQUcsa0JBQWtCLGtDQUFrQyxVQUFVLGtqQkFBa2pCLEVBQUUsaUVBQWlFLEVBQUUsOEVBQThFLEdBQUcsRUFBRSwySkFBMkosRUFBRSx5SUFBeUksRUFBRSwrREFBK0QseURBQXlELEVBQUUsRUFBRSxnRUFBZ0UsbUZBQW1GLEVBQUUsd0RBQXdELEVBQUUsRUFBRSx5RUFBeUUsc0ZBQXNGLEVBQUUsd0RBQXdELEVBQUUsRUFBRSx1REFBdUQsMENBQTBDLEdBQUcsRUFBRSw4REFBOEQsRUFBRSxtRkFBbUYsRUFBRSx5REFBeUQsRUFBRSxFQUFFLG9EQUFvRCxrR0FBa0csRUFBRSw0REFBNEQsRUFBRSx5REFBeUQsRUFBRSxFQUFFLG9EQUFvRCxvR0FBb0csRUFBRSw0REFBNEQsRUFBRSwyQ0FBMkMsR0FBRyxFQUFFLEVBQUUsOERBQThELDJEQUEyRCxFQUFFLG9GQUFvRixFQUFFLEVBQUUsOERBQThELDJEQUEyRCxFQUFFLEVBQUUsaUVBQWlFLHNHQUFzRyxHQUFHLEVBQUUsdURBQXVELEVBQUUsMENBQTBDLEdBQUcsRUFBRSxxREFBcUQsRUFBRSw0Q0FBNEMsb0JBQW9CLEdBQUcsRUFBRSw4Q0FBOEMsR0FBRyxFQUFFLDREQUE0RCxFQUFFLDBDQUEwQyxHQUFHLEVBQUUsRUFBRSwrREFBK0QseURBQXlELEVBQUUsNkRBQTZELEVBQUUsbUZBQW1GLEVBQUUsd0RBQXdELEVBQUUscUVBQXFFLEVBQUUsNkRBQTZELEVBQUUsRUFBRSxxRUFBcUUseURBQXlELEVBQUUsNkRBQTZELEVBQUUsbUZBQW1GLEVBQUUsd0RBQXdELEVBQUUscUVBQXFFLEVBQUUsNkRBQTZELEVBQUUsd0I7QUFDbDBJLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLG1DQUFtQztBQUM5RCxLQUFLO0FBQ0wsQzs7Ozs7Ozs7O0FDUEEsdUNBQXVDLG1CQUFPLENBQUMsK0RBQThCO0FBQzdFO0FBQ0EsWUFBWSxtQkFBTyxDQUFDLHNEQUEyQjtBQUMvQyxtQkFBTyxDQUFDLHlEQUFvQztBQUM1Qzs7O0FBR0EsWUFBWSxtQkFBTyxDQUFDLDBFQUF1RDs7O0FBRzNFLFlBQVksSUFBVTtBQUN0Qiw4QkFBOEIsbUJBQU8sQ0FBQyx1REFBOEI7QUFDcEU7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsMENBQTBDLGFBQWEsS0FBSztBQUM1RDtBQUNBO0FBQ0Esa0RBQWtELGFBQWE7QUFDL0QsaUJBQWlCO0FBQ2pCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSw0QkFBNEIseUpBQWtJO0FBQzlKO0FBQ0EsZ0JBQWdCLElBQVU7QUFDMUIscUQ7QUFDQSxtRkFBbUYsUUFBUyxRO0FBQzVGLGlCQUFpQjtBQUNqQjs7QUFFQSxRQUFRLG1CQUFPLENBQUMsMkRBQXNDO0FBQ3RELDBCQUEwQixtQkFBTyxDQUFDLCtEQUE4QjtBQUNoRSxvQkFBb0IsbUJBQU8sQ0FBQyxpRkFBdUM7QUFDbkUsbUJBQW1CLG1CQUFPLENBQUMsd0JBQXFCO0FBQ2hELE9BQU8sMEJBQTBCLEdBQUcsbUJBQU8sQ0FBQywwQkFBdUI7QUFDbkUsY0FBYyxtQkFBTyxDQUFDLGNBQVc7O0FBRWpDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLDhCQUE4QjtBQUNuRCxDQUFDLE87QUFDRCxxQkFBcUIseUJBQXlCO0FBQzlDOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixtQ0FBbUM7QUFDOUQsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7OztBQ3JFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsdUNBQXVDO0FBQ2xFLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ3pEQSxxRUFBdUIsbUJBQU8sQ0FBQyxtQ0FBaUM7QUFDaEUsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDO0FBQ25FLE9BQU8sb0NBQW9DLEdBQUcsbUJBQU8sQ0FBQyw4Q0FBcUI7QUFDM0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxHQUFHLG1CQUFPLENBQUMsd0RBQStCOztBQUUzQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsbURBQW1EO0FBQzlFLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ3ZIQSxpRUFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDN0QsMkJBQTJCLG1CQUFPLENBQUMsaUVBQStCO0FBQ2xFLGdCQUFnQixtQkFBTyxDQUFDLDBEQUE2Qjs7QUFFckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEU7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQix1REFBdUQ7QUFDbEYsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDakVBLGtFQUFvQixtQkFBTyxDQUFDLCtEQUE4QjtBQUMxRCxnQkFBZ0IsbUJBQU8sQ0FBQyxtREFBdUI7QUFDL0Msb0JBQW9CLG1CQUFPLENBQUMsbURBQXVCO0FBQ25ELG9CQUFvQixtQkFBTyxDQUFDLG1EQUF1QjtBQUNuRCxpQkFBaUIsbUJBQU8sQ0FBQyx5REFBMkI7O0FBRXBEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSw4Q0FBOEM7QUFDOUMsYUFBYTtBQUNiO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsS0FBSztBQUNMOztBQUVBO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLGlEQUFpRDtBQUM1RSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNqREEscUVBQXVCLG1CQUFPLENBQUMsbUNBQWlDO0FBQ2hFLG9CQUFvQixtQkFBTyxDQUFDLGlGQUF1QztBQUNuRTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsb0RBQW9EO0FBQy9FLEtBQUs7QUFDTCxDOzs7Ozs7Ozs7Ozs7Ozs7QUMvQkEsK0M7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiw0REFBNEQ7QUFDdkYsS0FBSztBQUNMLEM7Ozs7Ozs7OztBQ05BLGtOO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsNkRBQTZEO0FBQ3hGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ1JBLGdFQUFrQixrQ0FBa0MsVUFBVSx1REFBdUQsNkRBQTZELEVBQUUscUVBQXFFLEVBQUUsd0RBQXdELEVBQUUsd0I7QUFDclQsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsNERBQTREO0FBQ3ZGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ1BBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDREQUE0RDtBQUN2RixLQUFLO0FBQ0wsQzs7Ozs7Ozs7O0FDcENBLGtjQUFrYywwQkFBMEIsa0lBQWtJLDBCQUEwQixtbEI7QUFDeG5CLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDZEQUE2RDtBQUN4RixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNSQSxpRUFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDN0Qsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDO0FBQ25FLGFBQWEsbUJBQU8sQ0FBQyxpREFBdUI7O0FBRTVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxzQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDREQUE0RDtBQUN2RixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUMzUUEsZ0VBQWtCLGtDQUFrQyxVQUFVLHFEQUFxRCwrREFBK0QsRUFBRSxrRUFBa0UsRUFBRSw4REFBOEQsRUFBRSxxRUFBcUUsRUFBRSxrRkFBa0YsR0FBRyxFQUFFLHlIQUF5SCxHQUFHLEVBQUUsMERBQTBELGdDQUFnQywyQkFBMkIsR0FBRyxFQUFFLEVBQUUscURBQXFELHVEQUF1RCxFQUFFLHdEQUF3RCxFQUFFLGtFQUFrRSxFQUFFLEVBQUUscURBQXFELDZEQUE2RCxFQUFFLHVEQUF1RCxFQUFFLDZEQUE2RCxFQUFFLEVBQUUsdURBQXVELG9FQUFvRSxFQUFFLHlEQUF5RCxFQUFFLDREQUE0RCxFQUFFLDZEQUE2RCxFQUFFLDhEQUE4RCxFQUFFLDRDQUE0QyxHQUFHLEVBQUUsd0RBQXdELEVBQUUsbUZBQW1GLEVBQUUsRUFBRSw0REFBNEQsMERBQTBELEVBQUUsNkRBQTZELEVBQUUsbUVBQW1FLEVBQUUsNERBQTRELEVBQUUsbUZBQW1GLEVBQUUsMERBQTBELEdBQUcsRUFBRSwwQ0FBMEMsR0FBRyxFQUFFLEVBQUUsdURBQXVELDhEQUE4RCxFQUFFLEVBQUUsd0RBQXdELDREQUE0RCxFQUFFLDJEQUEyRCxFQUFFLHdEQUF3RCxFQUFFLHlEQUF5RCxFQUFFLEVBQUUsNkRBQTZELHlEQUF5RCxFQUFFLDZEQUE2RCxFQUFFLG1GQUFtRixFQUFFLHdEQUF3RCxFQUFFLHFFQUFxRSxFQUFFLDZEQUE2RCxFQUFFLHlEQUF5RCxFQUFFLEVBQUUsZ0VBQWdFLHlEQUF5RCxFQUFFLDZEQUE2RCxFQUFFLG1GQUFtRixFQUFFLHdEQUF3RCxFQUFFLG9FQUFvRSxFQUFFLDZEQUE2RCxFQUFFLHlEQUF5RCxFQUFFLHdCO0FBQ3ZwSCxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixzREFBc0Q7QUFDakYsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDUEEscUVBQXVCLG1CQUFPLENBQUMsbUNBQWU7QUFDOUM7QUFDQTtBQUNBLGlCQUFpQixtQkFBTyxDQUFDLDhDQUFxQjtBQUM5QyxvQkFBb0IsbUJBQU8sQ0FBQyxpRkFBdUM7QUFDbkUsT0FBTyxnQkFBZ0IsR0FBRyxtQkFBTyxDQUFDLDBCQUEyQjtBQUM3RCxxQkFBcUIsbUJBQU8sQ0FBQywwREFBNkI7QUFDMUQsVUFBVSxRQUFROztBQUVsQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQSxRQUFRO0FBQ1I7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHNEQUFzRDtBQUNqRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7O0FDOUtBLHl6RTtBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHVEQUF1RDtBQUNsRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNSQSxpRUFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7O0FBRTdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLCtEQUErRDtBQUMxRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUMzQ0EsZ0VBQWtCLGtDQUFrQyxVQUFVLHVEQUF1RCwwQ0FBMEMsR0FBRyxFQUFFLDhEQUE4RCxFQUFFLG1GQUFtRixFQUFFLHlEQUF5RCxFQUFFLEVBQUUsMERBQTBELHNGQUFzRixFQUFFLHlEQUF5RCxFQUFFLEVBQUUsdURBQXVELDZEQUE2RCxFQUFFLHFFQUFxRSxFQUFFLHdEQUF3RCxFQUFFLEVBQUUsa0VBQWtFLHVEQUF1RCxFQUFFLDBEQUEwRCxFQUFFLDJEQUEyRCxFQUFFLDREQUE0RCxFQUFFLGtFQUFrRSxFQUFFLEVBQUUscUVBQXFFLHVEQUF1RCxFQUFFLDJEQUEyRCxFQUFFLDREQUE0RCxFQUFFLG9EQUFvRCxHQUFHLEVBQUUsRUFBRSxxRUFBcUUsdURBQXVELEVBQUUsMkRBQTJELEVBQUUsNERBQTRELEVBQUUsa0VBQWtFLEVBQUUsRUFBRSxnRUFBZ0Usc0dBQXNHLEdBQUcsRUFBRSx1REFBdUQsRUFBRSxzREFBc0QsRUFBRSxrRUFBa0UsRUFBRSxpRUFBaUUsRUFBRSx3REFBd0QsRUFBRSxxRUFBcUUsRUFBRSxFQUFFLG1FQUFtRSxzR0FBc0csR0FBRyxFQUFFLHVEQUF1RCxFQUFFLHNEQUFzRCxFQUFFLHdEQUF3RCxFQUFFLG9EQUFvRCxHQUFHLEVBQUUsaUVBQWlFLEVBQUUscUVBQXFFLEVBQUUsRUFBRSxtRUFBbUUsdURBQXVELEVBQUUsc0RBQXNELEVBQUUsa0VBQWtFLEVBQUUsd0RBQXdELEVBQUUsbURBQW1ELEdBQUcsRUFBRSxxRUFBcUUsRUFBRSxFQUFFLHdEQUF3RCxtRUFBbUUsRUFBRSwyREFBMkQsRUFBRSxFQUFFLHlFQUF5RSwyREFBMkQsRUFBRSxtRUFBbUUsRUFBRSx5REFBeUQsRUFBRSxFQUFFLGdGQUFnRiwwREFBMEQsRUFBRSxxRUFBcUUsRUFBRSx5REFBeUQsRUFBRSx3QjtBQUNsL0gsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsb0RBQW9EO0FBQy9FLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ1BBLG9FQUFzQixtQkFBTyxDQUFDLHNDQUFtQjtBQUNqRCxtQkFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDN0Q7QUFDQSxvQkFBb0IsbUJBQU8sQ0FBQyxpRkFBdUM7QUFDbkUsaUJBQWlCLG1CQUFPLENBQUMsOENBQXFCO0FBQzlDLGdCQUFnQixtQkFBTyxDQUFDLDBEQUE2QjtBQUNyRCxPQUFPLGlCQUFpQixHQUFHLG1CQUFPLENBQUMsMEJBQTJCOztBQUU5RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFCQUFxQixZQUFZO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDtBQUNBO0FBQ0EsY0FBYzs7QUFFZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsb0RBQW9EO0FBQy9FLEtBQUs7QUFDTCxDOzs7Ozs7OztBQzVKQSw0R0FBOEQsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7QUFDNUcsdUZBQXVGLFFBQVEsbUJBQU8sQ0FBQyx5REFBMEIsRUFBRSxFQUFFO0FBQ3JJLDBFQUEwRSxRQUFRLG1CQUFPLENBQUMseURBQTBCLEVBQUUsRUFBRTs7QUFFeEgscVJBQXFSLFdBQVcsNEpBQTRKLFVBQVUsaUVBQWlFLGlCQUFpQixpZ0JBQWlnQix5UEFBeVAsd1BBQXdQLDBSQUEwUixXQUFXLDRPQUE0TyxVQUFVLDZFQUE2RSxpQkFBaUIsdVg7QUFDbm9FLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHFEQUFxRDtBQUNoRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNYQSxnRUFBa0Isa0NBQWtDLFVBQVUscURBQXFELCtEQUErRCxFQUFFLGtFQUFrRSxFQUFFLDhEQUE4RCxFQUFFLHFFQUFxRSxFQUFFLGtGQUFrRixHQUFHLEVBQUUseUhBQXlILEdBQUcsRUFBRSwwREFBMEQsZ0NBQWdDLDJCQUEyQixHQUFHLEVBQUUsRUFBRSxxREFBcUQsdURBQXVELEVBQUUsd0RBQXdELEVBQUUsa0VBQWtFLEVBQUUsRUFBRSxxREFBcUQsNkRBQTZELEVBQUUsc0RBQXNELEVBQUUsNkRBQTZELEVBQUUsRUFBRSx1REFBdUQsb0VBQW9FLEVBQUUseURBQXlELEVBQUUsNERBQTRELEVBQUUsNkRBQTZELEVBQUUsOERBQThELEVBQUUsMERBQTBELEVBQUUsbUZBQW1GLEVBQUUsRUFBRSw0REFBNEQsMERBQTBELEVBQUUsNkRBQTZELEVBQUUsbUVBQW1FLEVBQUUsNERBQTRELEVBQUUsbUZBQW1GLEVBQUUsMERBQTBELEdBQUcsRUFBRSwwQ0FBMEMsR0FBRyxFQUFFLEVBQUUsZ0VBQWdFLHlEQUF5RCxFQUFFLDZEQUE2RCxFQUFFLG1GQUFtRixFQUFFLHdEQUF3RCxFQUFFLG9FQUFvRSxFQUFFLDZEQUE2RCxFQUFFLHlEQUF5RCxFQUFFLEVBQUUsNERBQTRELHlEQUF5RCxFQUFFLDZEQUE2RCxFQUFFLG1GQUFtRixFQUFFLDBDQUEwQyxHQUFHLEVBQUUsa0VBQWtFLEVBQUUsNkRBQTZELEVBQUUseURBQXlELEVBQUUsd0I7QUFDcnJHLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDREQUE0RDtBQUN2RixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsRTtBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDREQUE0RDtBQUN2RixLQUFLO0FBQ0wsQzs7Ozs7Ozs7O0FDeEJBLGt3QjtBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDZEQUE2RDtBQUN4RixLQUFLO0FBQ0wsQyIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgbWFwID0ge1xuXHRcIi4vYXBwLmNzc1wiOiBcIi4vYXBwLmNzc1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHR2YXIgaWQgPSBtYXBbcmVxXTtcblx0aWYoIShpZCArIDEpKSB7IC8vIGNoZWNrIGZvciBudW1iZXIgb3Igc3RyaW5nXG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9XG5cdHJldHVybiBpZDtcbn1cbndlYnBhY2tDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmU7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tDb250ZXh0O1xud2VicGFja0NvbnRleHQuaWQgPSBcIi4vIHN5bmMgXlxcXFwuXFxcXC9hcHBcXFxcLihjc3N8c2Nzc3xsZXNzfHNhc3MpJFwiOyIsInZhciBtYXAgPSB7XG5cdFwiLi9hcHAtcm9vdC1tYWluLnhtbFwiOiBcIi4vYXBwLXJvb3QtbWFpbi54bWxcIixcblx0XCIuL2FwcC1yb290LnhtbFwiOiBcIi4vYXBwLXJvb3QueG1sXCIsXG5cdFwiLi9hcHAuY3NzXCI6IFwiLi9hcHAuY3NzXCIsXG5cdFwiLi9hcHAuanNcIjogXCIuL2FwcC5qc1wiLFxuXHRcIi4vY29uZmlncy5qc1wiOiBcIi4vY29uZmlncy5qc1wiLFxuXHRcIi4vbW9kZWxzL2F1dGgtc2VydmljZS5qc1wiOiBcIi4vbW9kZWxzL2F1dGgtc2VydmljZS5qc1wiLFxuXHRcIi4vbW9kZWxzL2NoZWNrLWNvbm5lY3Rpb24uanNcIjogXCIuL21vZGVscy9jaGVjay1jb25uZWN0aW9uLmpzXCIsXG5cdFwiLi9tb2RlbHMvbGl2ZVVwZGF0ZS5qc1wiOiBcIi4vbW9kZWxzL2xpdmVVcGRhdGUuanNcIixcblx0XCIuL21vZGVscy9yZWZyZXNoLXRva2VuLmpzXCI6IFwiLi9tb2RlbHMvcmVmcmVzaC10b2tlbi5qc1wiLFxuXHRcIi4vdmlld3MvYWN0aXZpdHktcGFnZS9hY3Rpdml0eS5qc1wiOiBcIi4vdmlld3MvYWN0aXZpdHktcGFnZS9hY3Rpdml0eS5qc1wiLFxuXHRcIi4vdmlld3MvYWN0aXZpdHktcGFnZS9hY3Rpdml0eS54bWxcIjogXCIuL3ZpZXdzL2FjdGl2aXR5LXBhZ2UvYWN0aXZpdHkueG1sXCIsXG5cdFwiLi92aWV3cy9hZ2VyYW5nZS1wYWdlL2FnZXJhbmdlLmNzc1wiOiBcIi4vdmlld3MvYWdlcmFuZ2UtcGFnZS9hZ2VyYW5nZS5jc3NcIixcblx0XCIuL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UuanNcIjogXCIuL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UuanNcIixcblx0XCIuL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UueG1sXCI6IFwiLi92aWV3cy9hZ2VyYW5nZS1wYWdlL2FnZXJhbmdlLnhtbFwiLFxuXHRcIi4vdmlld3MvbG9naW4tcGFnZS9sb2dpbi1tb2RlbC5qc1wiOiBcIi4vdmlld3MvbG9naW4tcGFnZS9sb2dpbi1tb2RlbC5qc1wiLFxuXHRcIi4vdmlld3MvbG9naW4tcGFnZS9sb2dpbi5jc3NcIjogXCIuL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4uY3NzXCIsXG5cdFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLmpzXCI6IFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLmpzXCIsXG5cdFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLnhtbFwiOiBcIi4vdmlld3MvbG9naW4tcGFnZS9sb2dpbi54bWxcIixcblx0XCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLXZpZXctbW9kZWwuanNcIjogXCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLXZpZXctbW9kZWwuanNcIixcblx0XCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLmNzc1wiOiBcIi4vdmlld3MvbWFpbi1wYWdlL21haW4uY3NzXCIsXG5cdFwiLi92aWV3cy9tYWluLXBhZ2UvbWFpbi5qc1wiOiBcIi4vdmlld3MvbWFpbi1wYWdlL21haW4uanNcIixcblx0XCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLnhtbFwiOiBcIi4vdmlld3MvbWFpbi1wYWdlL21haW4ueG1sXCIsXG5cdFwiLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLmNzc1wiOiBcIi4vdmlld3MvcmVnaXN0ZXItcGFnZS9yZWdpc3Rlci5jc3NcIixcblx0XCIuL3ZpZXdzL3JlZ2lzdGVyLXBhZ2UvcmVnaXN0ZXIuanNcIjogXCIuL3ZpZXdzL3JlZ2lzdGVyLXBhZ2UvcmVnaXN0ZXIuanNcIixcblx0XCIuL3ZpZXdzL3JlZ2lzdGVyLXBhZ2UvcmVnaXN0ZXIueG1sXCI6IFwiLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLnhtbFwiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHR2YXIgaWQgPSBtYXBbcmVxXTtcblx0aWYoIShpZCArIDEpKSB7IC8vIGNoZWNrIGZvciBudW1iZXIgb3Igc3RyaW5nXG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9XG5cdHJldHVybiBpZDtcbn1cbndlYnBhY2tDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmU7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tDb250ZXh0O1xud2VicGFja0NvbnRleHQuaWQgPSBcIi4vIHN5bmMgcmVjdXJzaXZlICg/PCFcXFxcYkFwcF9SZXNvdXJjZXNcXFxcYi4qKSg/PCFcXFxcLlxcXFwvXFxcXGJ0ZXN0c1xcXFxiXFxcXC8uKj8pXFxcXC4oeG1sfGNzc3xqc3xrdHwoPzwhXFxcXC5kXFxcXC4pdHN8KD88IVxcXFxiX1tcXFxcdy1dKlxcXFwuKXNjc3MpJFwiOyIsIlxubW9kdWxlLmV4cG9ydHMgPSBcIjxGcmFtZSBpZD1cXFwibWFpblxcXCIgZGVmYXVsdFBhZ2U9XFxcInZpZXdzL21haW4tcGFnZS9tYWluXFxcIj5cXG48L0ZyYW1lPlxcblwiOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL2FwcC1yb290LW1haW4ueG1sXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcIm1hcmt1cFwiLCBwYXRoOiBcIi4vYXBwLXJvb3QtbWFpbi54bWxcIiB9KTtcbiAgICB9KTtcbn0gIiwiXG5tb2R1bGUuZXhwb3J0cyA9IFwiPEZyYW1lIGlkPVxcXCJsb2dpblxcXCIgZGVmYXVsdFBhZ2U9XFxcInZpZXdzL2xvZ2luLXBhZ2UvbG9naW5cXFwiPlxcbjwvRnJhbWU+XCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vYXBwLXJvb3QueG1sXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcIm1hcmt1cFwiLCBwYXRoOiBcIi4vYXBwLXJvb3QueG1sXCIgfSk7XG4gICAgfSk7XG59ICIsImdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIn5AbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcIikpO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcIiwgKCkgPT4gcmVxdWlyZShcIkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2NvcmUuY3NzXCIpKTttb2R1bGUuZXhwb3J0cyA9IHtcInR5cGVcIjpcInN0eWxlc2hlZXRcIixcInN0eWxlc2hlZXRcIjp7XCJydWxlc1wiOlt7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCJcXG5JbiBOYXRpdmVTY3JpcHQsIHRoZSBhcHAuY3NzIGZpbGUgaXMgd2hlcmUgeW91IHBsYWNlIENTUyBydWxlcyB0aGF0XFxueW91IHdvdWxkIGxpa2UgdG8gYXBwbHkgdG8geW91ciBlbnRpcmUgYXBwbGljYXRpb24uIENoZWNrIG91dFxcbmh0dHA6Ly9kb2NzLm5hdGl2ZXNjcmlwdC5vcmcvdWkvc3R5bGluZyBmb3IgYSBmdWxsIGxpc3Qgb2YgdGhlIENTU1xcbnNlbGVjdG9ycyBhbmQgcHJvcGVydGllcyB5b3UgY2FuIHVzZSB0byBzdHlsZSBVSSBjb21wb25lbnRzLlxcblxcbi8qXFxuSW4gbWFueSBjYXNlcyB5b3UgbWF5IHdhbnQgdG8gdXNlIHRoZSBOYXRpdmVTY3JpcHQgY29yZSB0aGVtZSBpbnN0ZWFkXFxub2Ygd3JpdGluZyB5b3VyIG93biBDU1MgcnVsZXMuIFlvdSBjYW4gbGVhcm4gbW9yZSBhYm91dCB0aGUgXFxuTmF0aXZlU2NyaXB0IGNvcmUgdGhlbWUgYXQgaHR0cHM6Ly9naXRodWIuY29tL25hdGl2ZXNjcmlwdC90aGVtZVxcblRoZSBpbXBvcnRlZCBDU1MgcnVsZXMgbXVzdCBwcmVjZWRlIGFsbCBvdGhlciB0eXBlcyBvZiBydWxlcy5cXG5cIn0se1widHlwZVwiOlwiaW1wb3J0XCIsXCJpbXBvcnRcIjpcIlxcXCJ+QG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcXFwiXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBAaW1wb3J0IFxcXCJ+QG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcXFwiOyBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIFBsYWNlIGFueSBDU1MgcnVsZXMgeW91IHdhbnQgdG8gYXBwbHkgb24gYm90aCBpT1MgYW5kIEFuZHJvaWQgaGVyZS5cXG5UaGlzIGlzIHdoZXJlIHRoZSB2YXN0IG1ham9yaXR5IG9mIHlvdXIgQ1NTIGNvZGUgZ29lcy4gXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIlxcblRoZSBmb2xsb3dpbmcgQ1NTIHJ1bGUgY2hhbmdlcyB0aGUgZm9udCBzaXplIG9mIGFsbCBCdXR0b25zIHRoYXQgaGF2ZSB0aGVcXG5cXFwiLXByaW1hcnlcXFwiIGNsYXNzIG1vZGlmaWVyLlxcblwifSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCJCdXR0b24uLXByaW1hcnlcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5rYW5pdC10ZXh0LWZvbnRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcImJsYWNrXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmthbml0LXRleHQtZm9udC1zZW1pYm9sZFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LVNlbWlCb2xkXFxcIlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiYmxhY2tcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuaGVhZGVyXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBjb2xvcjogd2hpdGU7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInRleHQtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5mYXNcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJGb250IEF3ZXNvbWUgNSBGcmVlXFxcIiwgXFxcImZhLXNvbGlkLTkwMFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXdlaWdodFwiLFwidmFsdWVcIjpcIjkwMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjM2XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmZhclwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkZvbnQgQXdlc29tZSA1IEZyZWVcXFwiLCBcXFwiZmEtcmVndWxhci00MDBcXFwiXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC13ZWlnaHRcIixcInZhbHVlXCI6XCI0MDBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGZvbnQtc2l6ZTogMTA7IFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idG4tdGV4dC1mb250XCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1zaXplXCIsXCJ2YWx1ZVwiOlwiMTZweFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LU1lZGl1bVxcXCJcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuaGVhZGVyQ29udGVudFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjIycHhcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIubGlzdHZpZXctY29udGVudFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgYmFja2dyb3VuZC1pbWFnZTogdXJsKFxcXCJhcHAvdmlld3MvbWFpbi1wYWdlL3NyYy9jaGVja2luLmpwZ1xcXCIpIG5vLXJlcGVhdDsgXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwid2lkdGhcIixcInZhbHVlXCI6XCIxMDAlXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBoZWlnaHQ6IDEwMCU7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpblwiLFwidmFsdWVcIjpcIjhcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi1sZWZ0OiA4O1xcbiAgbWFyZ2luLXJpZ2h0OiA4OyBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi1ib3R0b206IDg7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCI4XCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBjb2xvcjogd2hpdGU7IFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5jb25maXJtLWJ1dHRvblwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE2XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC13ZWlnaHRcIixcInZhbHVlXCI6XCJib2xkXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1mYW1pbHlcIixcInZhbHVlXCI6XCJcXFwiS2FuaXRcXFwiLCBcXFwiS2FuaXQtTGlnaHRcXFwiXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjNGE3NzRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmVycm9yLWNvbmZpcm0tYnV0dG9uXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1zaXplXCIsXCJ2YWx1ZVwiOlwiMTZcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXdlaWdodFwiLFwidmFsdWVcIjpcImJvbGRcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIiM0YTc3NGVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMjBcIn1dfV0sXCJwYXJzaW5nRXJyb3JzXCI6W119fTs7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vYXBwLmNzc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzdHlsZVwiLCBwYXRoOiBcIi4vYXBwLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJcbiAgICAgICAgbGV0IGFwcGxpY2F0aW9uQ2hlY2tQbGF0Zm9ybSA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uXCIpO1xuICAgICAgICBpZiAoYXBwbGljYXRpb25DaGVja1BsYXRmb3JtLmFuZHJvaWQgJiYgIWdsb2JhbFtcIl9fc25hcHNob3RcIl0pIHtcbiAgICAgICAgICAgIHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2ZyYW1lXCIpO1xucmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvZnJhbWUvYWN0aXZpdHlcIik7XG4gICAgICAgIH1cblxuICAgICAgICBcbiAgICAgICAgICAgIHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtZGV2LXdlYnBhY2svbG9hZC1hcHBsaWNhdGlvbi1jc3MtcmVndWxhclwiKSgpO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgaWYgKG1vZHVsZS5ob3QpIHtcbiAgICAgICAgICAgIGNvbnN0IGhtclVwZGF0ZSA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtZGV2LXdlYnBhY2svaG1yXCIpLmhtclVwZGF0ZTtcbiAgICAgICAgICAgIGdsb2JhbC5fX2NvcmVNb2R1bGVzTGl2ZVN5bmMgPSBnbG9iYWwuX19vbkxpdmVTeW5jO1xuXG4gICAgICAgICAgICBnbG9iYWwuX19vbkxpdmVTeW5jID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBob3QgdXBkYXRlZCBvbiBMaXZlU3luY1xuICAgICAgICAgICAgICAgIGhtclVwZGF0ZSgpO1xuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2ggPSBmdW5jdGlvbih7IHR5cGUsIHBhdGggfSA9IHt9KSB7XG4gICAgICAgICAgICAgICAgLy8gdGhlIGhvdCB1cGRhdGVzIGFyZSBhcHBsaWVkLCBhc2sgdGhlIG1vZHVsZXMgdG8gYXBwbHkgdGhlIGNoYW5nZXNcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZ2xvYmFsLl9fY29yZU1vZHVsZXNMaXZlU3luYyh7IHR5cGUsIHBhdGggfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAvLyBoYW5kbGUgaG90IHVwZGF0ZWQgb24gaW5pdGlhbCBhcHAgc3RhcnRcbiAgICAgICAgICAgIGhtclVwZGF0ZSgpO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICAgICAgY29uc3QgY29udGV4dCA9IHJlcXVpcmUuY29udGV4dChcIn4vXCIsIHRydWUsIC8oPzwhXFxiQXBwX1Jlc291cmNlc1xcYi4qKSg/PCFcXC5cXC9cXGJ0ZXN0c1xcYlxcLy4qPylcXC4oeG1sfGNzc3xqc3xrdHwoPzwhXFwuZFxcLil0c3woPzwhXFxiX1tcXHctXSpcXC4pc2NzcykkLyk7XG4gICAgICAgICAgICBnbG9iYWwucmVnaXN0ZXJXZWJwYWNrTW9kdWxlcyhjb250ZXh0KTtcbiAgICAgICAgICAgIGlmIChtb2R1bGUuaG90KSB7XG4gICAgICAgICAgICAgICAgbW9kdWxlLmhvdC5hY2NlcHQoY29udGV4dC5pZCwgKCkgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJITVI6IEFjY2VwdCBtb2R1bGUgJ1wiICsgY29udGV4dC5pZCArIFwiJyBmcm9tICdcIiArIG1vZHVsZS5pZCArIFwiJ1wiKTsgXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYnVuZGxlLWVudHJ5LXBvaW50c1wiKTtcbiAgICAgICAgbGV0IGFwcGxpY2F0aW9uID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIik7XG5jb25zdCBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCIpO1xuY29uc3QgTGl2ZVVwZGF0ZSA9IHJlcXVpcmUoXCIuL21vZGVscy9saXZlVXBkYXRlXCIpO1xuY29uc3QgeyBjb25maWd1cmVPQXV0aFByb3ZpZGVycyB9ID0gcmVxdWlyZShcIi4vbW9kZWxzL2F1dGgtc2VydmljZVwiKTtcbmdsb2JhbC5jb25mID0gcmVxdWlyZShcIi4vY29uZmlnc1wiKTtcblxubmV3IExpdmVVcGRhdGUoZ2xvYmFsLmNvbmYudXBkYXRlS2V5KTtcbmNvbmZpZ3VyZU9BdXRoUHJvdmlkZXJzKCk7XG5cbmxldCB0b2tlbiA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcInRva2VuXCIsIFwidG9rZW5leHBpcmVkXCIpO1xuLy8gY29uc29sZS5sb2coXCJSb290LVVzZXIgVG9rZW46IFwiICsgdG9rZW4pO1xuaWYgKHRva2VuICE9IFwidG9rZW5leHBpcmVkXCIpIHtcbiAgICBhcHBsaWNhdGlvbi5ydW4oeyBtb2R1bGVOYW1lOiBcImFwcC1yb290LW1haW5cIiB9KTtcbn0gZWxzZSB7IFxuICAgIGFwcGxpY2F0aW9uLnJ1bih7IG1vZHVsZU5hbWU6IFwiYXBwLXJvb3RcIiB9KTtcbn1cblxuXG4vKlxuRG8gbm90IHBsYWNlIGFueSBjb2RlIGFmdGVyIHRoZSBhcHBsaWNhdGlvbiBoYXMgYmVlbiBzdGFydGVkIGFzIGl0IHdpbGwgbm90XG5iZSBleGVjdXRlZCBvbiBpT1MuXG4qL1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL2FwcC5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL2FwcC5qc1wiIH0pO1xuICAgIH0pO1xufSBcbiAgICBcbiAgICAgICAgXG4gICAgICAgIFxuICAgICIsImNvbmZpZyA9IHtcbiAgbmFtZTogXCJGaXJzdCBDaGVja1wiLFxuICB2ZXI6IFwiVmVyc2lvbiAxLjEgKGJ1aWxkIDIwMjAwNjE2KVwiLFxuXG4gIGFwaToge1xuICAgIC8vIGFjY2VzczogXCJodHRwczovL21lZGhyLm1lZGljaW5lLnBzdS5hYy50aC9hcHAtYXBpL3YxLz8vYWNjZXNzXCIsXG4gICAgLy8gcHJvZmlsZTogXCJodHRwczovL21lZGhyLm1lZGljaW5lLnBzdS5hYy50aC9hcHAtYXBpL3YxLz8vaW5mb1wiLFxuICAgIC8vIGRpc3BsYXk6IFwiaHR0cHM6Ly9tZWRoci5tZWRpY2luZS5wc3UuYWMudGgvYXBwLWFwaS92MS8/L2Rpc3BsYXlcIixcblxuICAgIGxvZ2luOiBcImh0dHBzOi8vNDQyMWJmMmZiZTVlLm5ncm9rLmlvL2FwaS9sb2dpblwiLFxuICAgIHRva2VuOiBcImh0dHBzOi8vNDQyMWJmMmZiZTVlLm5ncm9rLmlvL2FwaS90b2tlblwiLFxuICAgIHJlZnJlc2h0b2tlbjogXCJodHRwczovLzQ0MjFiZjJmYmU1ZS5uZ3Jvay5pby9hcGkvcmVmcmVzaHRva2VuXCIsXG4gICAgdXNlcmluZm86IFwiaHR0cHM6Ly80NDIxYmYyZmJlNWUubmdyb2suaW8vYXBpL3VzZXJpbmZvXCIsXG4gICAgLy8gZ2V0bWlzcGluOiBcImh0dHBzOi8vMWFlZmJmMzkwOThmLm5ncm9rLmlvL2FwaS9nZXRtaXNwaW5cIixcbiAgICAvLyBmaXJzdGNoZWNrOiBcImh0dHBzOi8vMWFlZmJmMzkwOThmLm5ncm9rLmlvL2FwaS9jdXJmaXJzdGNoZWNrXCIsXG4gICAgLy8gZmlyc3RjaGVja2hpc3Rvcnk6IFwiaHR0cHM6Ly8xYWVmYmYzOTA5OGYubmdyb2suaW8vYXBpL2hpc3RvcnlcIixcbiAgICAvLyBmaXJzdGNoZWNrY29uZmlybTogXCJodHRwczovLzFhZWZiZjM5MDk4Zi5uZ3Jvay5pby9hcGkvY29uZmlybVwiLFxuICAgIC8vIGZpcnN0Y2hlY2tkdXBsaWNhdGVkOiBcImh0dHBzOi8vMWFlZmJmMzkwOThmLm5ncm9rLmlvL2FwaS9kdXBsaWNhdGVkXCIsXG4gICAgLy8gZHJ1Z25hbWU6IFwiaHR0cHM6Ly8xYWVmYmYzOTA5OGYubmdyb2suaW8vYXBpL2RydWduYW1lXCIsXG4gICAgLy8gZHJ1Z21pc3Rha2U6IFwiaHR0cHM6Ly8xYWVmYmYzOTA5OGYubmdyb2suaW8vYXBpL2RydWdtaXN0YWtlXCIsXG4gICAgLy8gbmV3ZHJ1Z21pc3Rha2U6IFwiaHR0cHM6Ly8xYWVmYmYzOTA5OGYubmdyb2suaW8vYXBpL25ld2RydWdtaXN0YWtlXCIsXG4gICAgLy8gdXBkYXRlZHJ1Z21pc3Rha2U6XG4gICAgLy8gICAgIFwiaHR0cHM6Ly8xYWVmYmYzOTA5OGYubmdyb2suaW8vYXBpL3VwZGF0ZWRydWdtaXN0YWtlXCIsXG4gICAgLy8gcHJlZGlzcG51bWJlcjogXCJodHRwczovLzFhZWZiZjM5MDk4Zi5uZ3Jvay5pby9hcGkvcHJlZGlzcG51bWJlclwiLFxuICAgIC8vIHVwZGF0ZWRydWdtaXN0YWtlYW1vdW50OlxuICAgIC8vICAgICBcImh0dHBzOi8vMWFlZmJmMzkwOThmLm5ncm9rLmlvL2FwaS91cGRhdGVkcnVnbWlzdGFrZWFtb3VudFwiLFxuICAgIC8vIHNlbmRyZXBvcnQ6IFwiaHR0cHM6Ly8xYWVmYmYzOTA5OGYubmdyb2suaW8vYXBpL3NlbmRyZXBvcnRcIixcblxuICAgIC8vIGxvZ2luOiBcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvZmlyc3RjaGVjay9zdGFmZmxvZ2luXCIsXG4gICAgLy8gZ2V0bWlzcGluOiBcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvZmlyc3RjaGVjay9nZXRtaXNwaW5cIixcbiAgICAvLyBmaXJzdGNoZWNrOiBcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvZmlyc3RjaGVjay9jdXJmaXJzdGNoZWNrXCIsXG4gICAgLy8gZmlyc3RjaGVja2hpc3Rvcnk6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9maXJzdGNoZWNrL2hpc3RvcnlcIixcbiAgICAvLyBmaXJzdGNoZWNrY29uZmlybTogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2ZpcnN0Y2hlY2svY29uZmlybVwiLFxuICAgIC8vIGZpcnN0Y2hlY2tkdXBsaWNhdGVkOiBcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvZmlyc3RjaGVjay9kdXBsaWNhdGVkXCIsXG4gICAgLy8gZHJ1Z25hbWU6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9maXJzdGNoZWNrL2RydWduYW1lXCIsXG4gICAgLy8gZHJ1Z21pc3Rha2U6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9maXJzdGNoZWNrL2RydWdtaXN0YWtlXCIsXG4gICAgLy8gbmV3ZHJ1Z21pc3Rha2U6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9maXJzdGNoZWNrL25ld2RydWdtaXN0YWtlXCIsXG4gICAgLy8gdXBkYXRlZHJ1Z21pc3Rha2U6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9maXJzdGNoZWNrL3VwZGF0ZWRydWdtaXN0YWtlXCIsXG4gICAgLy8gcHJlZGlzcG51bWJlcjpcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvZmlyc3RjaGVjay9wcmVkaXNwbnVtYmVyXCIsXG4gICAgLy8gdXBkYXRlZHJ1Z21pc3Rha2VhbW91bnQ6XCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2ZpcnN0Y2hlY2svdXBkYXRlZHJ1Z21pc3Rha2VhbW91bnRcIixcbiAgICAvLyBzZW5kcmVwb3J0OlwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9maXJzdGNoZWNrL3NlbmRyZXBvcnRcIlxuICB9LFxuXG4gIHVwZGF0ZUtleToge1xuICAgIGlvczogXCJsRFRrRHJvaWlBV2RPYTg5YWRFYUwzN3ZBTEZGcGJWWWljMHc1XCIsXG4gICAgYW5kcm9pZDogXCJ1MExTU0RpclUyRkFnT3B5bVNlbzlXUkVxeG85cGJWWWljMHc1XCIsXG4gIH0sXG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbmZpZztcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9jb25maWdzLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vY29uZmlncy5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBMb2dpblZpZXdNb2RlbCA9IHJlcXVpcmUoXCIuLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLW1vZGVsXCIpO1xuY29uc3QgYXBwU2V0dGluZ3MgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiKTtcbmNvbnN0IHsgY29uZmlndXJlVG5zT0F1dGgsIFRuc09BdXRoQ2xpZW50IH0gPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LW9hdXRoMlwiKTtcbmNvbnN0IHtcbiAgVG5zT2FQcm92aWRlcixcbiAgVG5zT2FQcm92aWRlck9wdGlvbnNHb29nbGUsXG4gIFRuc09hUHJvdmlkZXJHb29nbGUsXG4gIFRuc09hUHJvdmlkZXJPcHRpb25zRmFjZWJvb2ssXG4gIFRuc09hUHJvdmlkZXJGYWNlYm9vayxcbn0gPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LW9hdXRoMi9wcm92aWRlcnNcIik7XG5cbmxldCB1c2VyID0gTG9naW5WaWV3TW9kZWwoKTtcbmxldCBjbGllbnQgPSBudWxsO1xuXG5leHBvcnRzLmNvbmZpZ3VyZU9BdXRoUHJvdmlkZXJzID0gKCkgPT4ge1xuICBjb25zdCBnb29nbGVQcm92aWRlciA9IGNvbmZpZ3VyZU9BdXRoUHJvdmlkZXJHb29nbGUoKTtcbiAgY29uc3QgZmFjZWJvb2tQcm92aWRlciA9IGNvbmZpZ3VyZU9BdXRoUHJvdmlkZXJGYWNlYm9vaygpO1xuICBjb25maWd1cmVUbnNPQXV0aChbZ29vZ2xlUHJvdmlkZXIsIGZhY2Vib29rUHJvdmlkZXJdKTtcbn07XG5cbmZ1bmN0aW9uIGNvbmZpZ3VyZU9BdXRoUHJvdmlkZXJHb29nbGUoKSB7XG4gIGNvbnN0IGdvb2dsZVByb3ZpZGVyT3B0aW9ucyA9IHtcbiAgICBvcGVuSWRTdXBwb3J0OiBcIm9pZC1mdWxsXCIsXG4gICAgY2xpZW50SWQ6XG4gICAgICBcIjE5Njc1OTYyMDU1Mi1vNTBlM3ZocnU5cG10ZzEwYTJwMW50cWliMXJ1ZGh0aC5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbVwiLFxuICAgIHJlZGlyZWN0VXJpOlxuICAgICAgXCJjb20uZ29vZ2xldXNlcmNvbnRlbnQuYXBwcy4xOTY3NTk2MjA1NTItbzUwZTN2aHJ1OXBtdGcxMGEycDFudHFpYjFydWRodGg6L2F1dGhcIixcbiAgICB1cmxTY2hlbWU6XG4gICAgICBcImNvbS5nb29nbGV1c2VyY29udGVudC5hcHBzLjE5Njc1OTYyMDU1Mi1vNTBlM3ZocnU5cG10ZzEwYTJwMW50cWliMXJ1ZGh0aFwiLFxuICAgIHNjb3BlczogW1wiZW1haWxcIl0sXG4gIH07XG4gIGNvbnN0IGdvb2dsZVByb3ZpZGVyID0gbmV3IFRuc09hUHJvdmlkZXJHb29nbGUoZ29vZ2xlUHJvdmlkZXJPcHRpb25zKTtcbiAgcmV0dXJuIGdvb2dsZVByb3ZpZGVyO1xufVxuXG5mdW5jdGlvbiBjb25maWd1cmVPQXV0aFByb3ZpZGVyRmFjZWJvb2soKSB7XG4gIGNvbnN0IGZhY2Vib29rUHJvdmlkZXJPcHRpb25zID0ge1xuICAgIG9wZW5JZFN1cHBvcnQ6IFwib2lkLW5vbmVcIixcbiAgICBjbGllbnRJZDogXCI4Nzc1MzI1MDMwMDI3MTFcIixcbiAgICBjbGllbnRTZWNyZXQ6IFwiMmU5NjllNmY5MDM2NTU1ZTExNGZjOTIyNWRmNTVjZGJcIixcbiAgICByZWRpcmVjdFVyaTogXCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vY29ubmVjdC9sb2dpbl9zdWNjZXNzLmh0bWxcIixcbiAgICBzY29wZXM6IFtcImVtYWlsXCJdLFxuICB9O1xuICBjb25zdCBmYWNlYm9va1Byb3ZpZGVyID0gbmV3IFRuc09hUHJvdmlkZXJGYWNlYm9vayhmYWNlYm9va1Byb3ZpZGVyT3B0aW9ucyk7XG4gIHJldHVybiBmYWNlYm9va1Byb3ZpZGVyO1xufVxuXG5leHBvcnRzLnRuc09hdXRoTG9naW4gPSAocHJvdmlkZXJUeXBlKSA9PiB7XG4gIGNsaWVudCA9IG5ldyBUbnNPQXV0aENsaWVudChwcm92aWRlclR5cGUpO1xuICBjbGllbnQubG9naW5XaXRoQ29tcGxldGlvbigodG9rZW5SZXN1bHQsIGVycikgPT4ge1xuICAgIGlmIChlcnIpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoXCJsb2dpbmcgaW4gc29tdGhpbmdzIHdlbnQgd3JvbmchXCIpO1xuICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZyhcIj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cIik7XG4gICAgICBpZiAodG9rZW5SZXN1bHQuYWNjZXNzVG9rZW4gIT0gXCJcIikge1xuICAgICAgICBsZXQgYWNjZXNzVG9rZW4gPSB0b2tlblJlc3VsdC5hY2Nlc3NUb2tlbjtcbiAgICAgICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFwidG9rZW5cIiwgYWNjZXNzVG9rZW4pO1xuICAgICAgICBzd2l0Y2ggKHByb3ZpZGVyVHlwZSkge1xuICAgICAgICAgIGNhc2UgXCJnb29nbGVcIjpcbiAgICAgICAgICAgIC8vICoqKiBHZXQgaW5mbyDguYLguJTguKLguYPguIrguYkgXCJpZFRva2VuXCJcbiAgICAgICAgICAgIGxldCBpZFRva2VuID0gdG9rZW5SZXN1bHQuaWRUb2tlbjtcbiAgICAgICAgICAgIHVzZXJcbiAgICAgICAgICAgICAgLmdvb2dsZUlkVG9rZW5JbmZvKGlkVG9rZW4pXG4gICAgICAgICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KCk7XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIGxldCBrZXkgPSBPYmplY3Qua2V5cyhyZXNwb25zZS5kYXRhKTtcbiAgICAgICAgICAgICAgICBpZiAoa2V5Lmxlbmd0aCA+IDIpIHtcbiAgICAgICAgICAgICAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcInNvY2lhbFwiLCBcImdvb2dsZVwiKTtcbiAgICAgICAgICAgICAgICAgIGxldCBnbWFpbCA9IHJlc3BvbnNlLmRhdGEuZW1haWwuc3BsaXQoXCJAXCIpO1xuICAgICAgICAgICAgICAgICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFwibmFtZVwiLCBnbWFpbFswXSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiSW52YWxpZCBUb2tlblwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgY2FzZSBcImZhY2Vib29rXCI6XG4gICAgICAgICAgICAvLyAqKiogR2V0IGluZm8g4LmC4LiU4Lii4LmD4LiK4LmJIFwiYWNjZXNzdG9rZW5cIlxuICAgICAgICAgICAgdXNlclxuICAgICAgICAgICAgICAuZmFjZWJvb2tJZFRva2VuSW5mbyhhY2Nlc3NUb2tlbilcbiAgICAgICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoKTtcbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgbGV0IGtleSA9IE9iamVjdC5rZXlzKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgICAgIGlmIChrZXlbMF0gIT0gXCJlcnJvclwiKSB7XG4gICAgICAgICAgICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJzb2NpYWxcIiwgXCJmYWNlYm9va1wiKTtcbiAgICAgICAgICAgICAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcIm5hbWVcIiwgcmVzcG9uc2UuZGF0YS5uYW1lKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJJbnZhbGlkIFRva2VuXCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0pO1xufTtcblxuZXhwb3J0cy50bnNPYXV0aExvZ291dCA9ICgpID0+IHtcbiAgaWYgKGNsaWVudCkge1xuICAgIGNvbnNvbGUubG9nKFwiPT09Q2xpZW50PT09XCIpO1xuICAgIGNvbnNvbGUubG9nKGNsaWVudCk7XG4gICAgY2xpZW50LmxvZ291dCgpO1xuICB9XG59O1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL21vZGVscy9hdXRoLXNlcnZpY2UuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi9tb2RlbHMvYXV0aC1zZXJ2aWNlLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IE9ic2VydmFibGUgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIikuT2JzZXJ2YWJsZTtcbmNvbnN0IGNvbm5lY3Rpdml0eU1vZHVsZSA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2Nvbm5lY3Rpdml0eVwiKTtcbmNvbnN0IGRpYWxvZ3MgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCIpO1xuXG5mdW5jdGlvbiBDb25uZWN0aW9uKCkge1xuICAgIGxldCB2aWV3TW9kZWwgPSBuZXcgT2JzZXJ2YWJsZSgpO1xuICAgIHZpZXdNb2RlbC5jaGVja0Nvbm5lY3Rpb24gPSAoKSA9PiB7XG4gICAgICAgIGNvbnN0IHR5cGUgPSBjb25uZWN0aXZpdHlNb2R1bGUuZ2V0Q29ubmVjdGlvblR5cGUoKTtcbiAgICAgICAgbGV0IG1lc3NhZ2U7XG4gICAgICAgIGxldCBjb25uZWN0aW9uU3RhdHVzID0gdHJ1ZTtcbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgICAgICBjYXNlIGNvbm5lY3Rpdml0eU1vZHVsZS5jb25uZWN0aW9uVHlwZS5ub25lOlxuICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBcIk5vIGNvbm5lY3Rpb24hXCI7XG4gICAgICAgICAgICAgICAgY29ubmVjdGlvblN0YXR1cyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBjb25uZWN0aXZpdHlNb2R1bGUuY29ubmVjdGlvblR5cGUud2lmaTpcbiAgICAgICAgICAgICAgICBtZXNzYWdlID0gXCJXaWZpIGNvbm5lY3Rpb25cIjtcbiAgICAgICAgICAgICAgICBjb25uZWN0aW9uU3RhdHVzID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgY29ubmVjdGl2aXR5TW9kdWxlLmNvbm5lY3Rpb25UeXBlLm1vYmlsZTpcbiAgICAgICAgICAgICAgICBtZXNzYWdlID0gXCIzRy80Ry81RyBjb25uZWN0aW9uXCI7XG4gICAgICAgICAgICAgICAgY29ubmVjdGlvblN0YXR1cyA9IHRydWU7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgY29ubmVjdGl2aXR5TW9kdWxlLnN0YXJ0TW9uaXRvcmluZygobmV3Q29ubmVjdGlvbikgPT4ge1xuICAgICAgICAgICAgc3dpdGNoIChuZXdDb25uZWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgY2FzZSBjb25uZWN0aXZpdHlNb2R1bGUuY29ubmVjdGlvblR5cGUubm9uZTpcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJObyBjb25uZWN0aW9uIVwiKTtcbiAgICAgICAgICAgICAgICAgICAgY29ubmVjdGlvblN0YXR1cyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIGNvbm5lY3Rpdml0eU1vZHVsZS5jb25uZWN0aW9uVHlwZS53aWZpOlxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkNvbm5lY3Rpb24gdmlhIHdpZmlcIik7XG4gICAgICAgICAgICAgICAgICAgIGNvbm5lY3Rpb25TdGF0dXMgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIGNvbm5lY3Rpdml0eU1vZHVsZS5jb25uZWN0aW9uVHlwZS5tb2JpbGU6XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQ29ubmVjdGlvbiB2aWEgM0cvNEcvNUdcIik7XG4gICAgICAgICAgICAgICAgICAgIGNvbm5lY3Rpb25TdGF0dXMgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29ubmVjdGl2aXR5TW9kdWxlLnN0b3BNb25pdG9yaW5nKCk7XG5cbiAgICAgICAgbGV0IHJlc3VsdCA9IHtcbiAgICAgICAgICAgIGNvbm5lY3RlZDogY29ubmVjdGlvblN0YXR1c1xuICAgICAgICB9O1xuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH07XG4gICAgcmV0dXJuIHZpZXdNb2RlbDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgQ29ubmVjdGlvbjogQ29ubmVjdGlvbixcbn07IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vbW9kZWxzL2NoZWNrLWNvbm5lY3Rpb24uanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi9tb2RlbHMvY2hlY2stY29ubmVjdGlvbi5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBhcHBsaWNhdGlvbiA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uXCIpO1xuY29uc3QgQXBwU3luYyA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtYXBwLXN5bmNcIikuQXBwU3luYztcbmNvbnN0IEluc3RhbGxNb2RlID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hcHAtc3luY1wiKS5JbnN0YWxsTW9kZTtcbmNvbnN0IFN5bmNTdGF0dXMgID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hcHAtc3luY1wiKS5TeW5jU3RhdHVzIDtcbmNvbnN0IHBsYXRmb3JtID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm1cIik7XG5cbmZ1bmN0aW9uIGxpdmVVcGRhdGUoY29uZmlnKSB7XG4gICAgaWYgKHBsYXRmb3JtLmlzSU9TKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiSU9TIGRldmljZSB0b2tlbjogXCIgKyBjb25maWcuaW9zKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkFuZHJvaWQgZGV2aWNlIHRva2VuOiBcIiArIGNvbmZpZy5hbmRyb2lkKTtcbiAgICB9XG4gICAgYXBwbGljYXRpb24ub24oYXBwbGljYXRpb24ucmVzdW1lRXZlbnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICBBcHBTeW5jLnN5bmMoe1xuICAgICAgICAgICAgZW5hYmxlZFdoZW5Vc2luZ0htcjogZmFsc2UsXG4gICAgICAgICAgICBkZXBsb3ltZW50S2V5OiBwbGF0Zm9ybS5pc0lPUyA/IGNvbmZpZy5pb3MgOiBjb25maWcuYW5kcm9pZCxcbiAgICAgICAgICAgIGluc3RhbGxNb2RlOiBJbnN0YWxsTW9kZS5JTU1FRElBVEUsXG4gICAgICAgICAgICBtYW5kYXRvcnlJbnN0YWxsTW9kZTogcGxhdGZvcm0uaXNJT1NcbiAgICAgICAgICAgICAgICA/IEluc3RhbGxNb2RlLk9OX05FWFRfUkVTVU1FXG4gICAgICAgICAgICAgICAgOiBJbnN0YWxsTW9kZS5JTU1FRElBVEUsXG4gICAgICAgICAgICB1cGRhdGVEaWFsb2c6IHtcbiAgICAgICAgICAgICAgICBvcHRpb25hbFVwZGF0ZU1lc3NhZ2U6IFwi4Lij4Li14Liq4LiV4Liy4Lij4LmM4LiX4LmA4Lie4Li34LmI4Lit4Lit4Lix4Lie4LmA4LiU4LiXXCIsXG4gICAgICAgICAgICAgICAgdXBkYXRlVGl0bGU6IFwi4Lit4Lix4Lie4LmA4LiU4LiX4LmA4Lin4Lit4Lij4LmM4LiK4Lix4LiZ4LmD4Lir4Lih4LmIXCIsXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5VXBkYXRlTWVzc2FnZTogXCLguKPguLXguKrguJXguLLguKPguYzguJfguYDguJ7guLfguYjguK3guK3guLHguJ7guYDguJTguJdcIixcbiAgICAgICAgICAgICAgICBvcHRpb25hbElnbm9yZUJ1dHRvbkxhYmVsOiBcIuC4ouC4geC5gOC4peC4tOC4gVwiLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeUNvbnRpbnVlQnV0dG9uTGFiZWw6IHBsYXRmb3JtLmlzSU9TXG4gICAgICAgICAgICAgICAgICAgID8gXCLguJvguLTguJRcIlxuICAgICAgICAgICAgICAgICAgICA6IFwi4Lij4Li14Liq4LiV4Liy4Lij4LmM4LiXXCIsXG4gICAgICAgICAgICAgICAgYXBwZW5kUmVsZWFzZURlc2NyaXB0aW9uOiB0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgICAoc3luY1N0YXR1cykgPT4ge1xuICAgICAgICAgICAgaWYgKHN5bmNTdGF0dXMgPT09IFN5bmNTdGF0dXMuVVBfVE9fREFURSkge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIk5vIHBlbmRpbmcgdXBkYXRlczsgeW914oCZcmUgcnVubmluZyB0aGUgbGF0ZXN0IHZlcnNpb24uXCIpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChzeW5jU3RhdHVzID09PSBTeW5jU3RhdHVzLlVQREFURV9JTlNUQUxMRUQpIHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJVcGRhdGUgZm91bmQgYW5kIGluc3RhbGxlZC4gSXQgd2lsbCBiZSBhY3RpdmF0ZWQgdXBvbiBuZXh0IGNvbGQgcmVib290XCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgfSk7XG4gICAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGl2ZVVwZGF0ZTtcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9tb2RlbHMvbGl2ZVVwZGF0ZS5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL21vZGVscy9saXZlVXBkYXRlLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IExvZ2luVmlld01vZGVsID0gcmVxdWlyZShcIi4uL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4tbW9kZWxcIik7XG5jb25zdCBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCIpO1xubGV0IHVzZXIgPSBMb2dpblZpZXdNb2RlbCgpO1xuXG5mdW5jdGlvbiByZWZyZXNoVG9rZW4oKSB7XG4gIHVzZXJcbiAgICAucmVmcmVzaCgpXG4gICAgLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KCk7XG4gICAgfSlcbiAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgIGlmIChyZXNwb25zZSAhPSBudWxsKSB7XG4gICAgICAgIGxldCBkYXRhID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgaWYgKGRhdGEuc3RhdHVzKSB7XG4gICAgICAgICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFwidG9rZW5cIiwgZGF0YS50b2tlbik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFwidG9rZW5cIiwgXCJ0b2tlbmV4cGlyZWRcIik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSByZWZyZXNoVG9rZW47XG47IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vbW9kZWxzL3JlZnJlc2gtdG9rZW4uanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi9tb2RlbHMvcmVmcmVzaC10b2tlbi5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvYWN0aXZpdHktcGFnZS9hY3Rpdml0eS5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL3ZpZXdzL2FjdGl2aXR5LXBhZ2UvYWN0aXZpdHkuanNcIiB9KTtcbiAgICB9KTtcbn0gIiwiXG5tb2R1bGUuZXhwb3J0cyA9IFwiPFBhZ2UgbG9hZGVkPVxcXCJvblBhZ2VMb2FkZWRcXFwiIHhtbG5zOmNhbGVuZGFyPVxcXCJuYXRpdmVzY3JpcHQtdWktY2FsZW5kYXJcXFwiIGFjdGlvbkJhckhpZGRlbj1cXFwidHJ1ZVxcXCIgeG1sbnM6bHY9XFxcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1xcXCIgYW5kcm9pZFN0YXR1c0JhckJhY2tncm91bmQ9XFxcIiM0MTVjNWFcXFwiPlxcbiAgIFxcbjwvUGFnZT5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9hY3Rpdml0eS1wYWdlL2FjdGl2aXR5LnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL3ZpZXdzL2FjdGl2aXR5LXBhZ2UvYWN0aXZpdHkueG1sXCIgfSk7XG4gICAgfSk7XG59ICIsIm1vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idXR0b25cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMTVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiI2ZmOGE2NVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn1dfV0sXCJwYXJzaW5nRXJyb3JzXCI6W119fTs7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvYWdlcmFuZ2UtcGFnZS9hZ2VyYW5nZS5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UuY3NzXCIgfSk7XG4gICAgfSk7XG59ICIsIi8vIFNwaW5uZXJcbmxldCBzcGlubmVyYmFja2dyb3VuZDtcbmxldCBzcGlubmVyO1xuXG5sZXQgcGFnZTtcbmV4cG9ydHMub25QYWdlTG9hZGVkID0gKGFyZ3MpID0+IHtcbiAgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gIC8vIHNwaW5uZXJiYWNrZ3JvdW5kID0gcGFnZS5nZXRWaWV3QnlJZChcInNwaW5uZXJiYWNrZ3JvdW5kXCIpO1xuICAvLyBzcGlubmVyID0gcGFnZS5nZXRWaWV3QnlJZChcInNwaW5uZXJcIik7XG4gIC8vIHByb2Nlc3NpbmdEaXNhYmxlKCk7XG59O1xuXG5leHBvcnRzLmJhY2sgPSAoKSA9PiB7XG4gIGNvbnNvbGUubG9nKFwiQmFjayB0byBob21lcGFnZVwiKTtcbiAgcGFnZS5mcmFtZS5nb0JhY2soKTtcbn07XG5cbmZ1bmN0aW9uIHByb2Nlc3NpbmdFbmFibGUoKSB7XG4gIHNwaW5uZXIuYnVzeSA9IHRydWU7XG4gIHNwaW5uZXIudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICBzcGlubmVyYmFja2dyb3VuZC52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gIC8vICAgYm90dG9tTmF2aWdhdGlvbi5vcGFjaXR5ID0gXCIwLjNcIjtcbn1cblxuZnVuY3Rpb24gcHJvY2Vzc2luZ0Rpc2FibGUoKSB7XG4gIHNwaW5uZXIuYnVzeSA9IGZhbHNlO1xuICBzcGlubmVyLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gIHNwaW5uZXJiYWNrZ3JvdW5kLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gIC8vICAgYm90dG9tTmF2aWdhdGlvbi5vcGFjaXR5ID0gXCIxXCI7XG59XG47IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvYWdlcmFuZ2UtcGFnZS9hZ2VyYW5nZS5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UuanNcIiB9KTtcbiAgICB9KTtcbn0gIiwiXG5tb2R1bGUuZXhwb3J0cyA9IFwiPFBhZ2UgbG9hZGVkPVxcXCJvblBhZ2VMb2FkZWRcXFwiIGFuZHJvaWRTdGF0dXNCYXJCYWNrZ3JvdW5kPVxcXCIjNDE1YzVhXFxcIj5cXG4gICAgPFBhZ2UuYWN0aW9uQmFyPlxcbiAgICAgICAgPEFjdGlvbkJhciBpZD1cXFwiYWN0aW9uYmFyXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiM0MTVjNWFcXFwiIGNvbG9yPVxcXCJ3aGl0ZVxcXCI+XFxuICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJoZWFkZXJcXFwiIHRleHQ9XFxcIuC5gOC4peC4t+C4reC4geC4iuC5iOC4p+C4h+C4reC4suC4ouC4uFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICA8TmF2aWdhdGlvbkJ1dHRvbiBhbmRyb2lkLnN5c3RlbUljb249XFxcImljX21lbnVfYmFja1xcXCIgdGFwPVxcXCJiYWNrXFxcIiBpb3MucG9zaXRpb249XFxcImxlZnRcXFwiIC8+XFxuICAgICAgICAgICAgPCEtLSA8QWN0aW9uSXRlbSBjbGFzcz1cXFwiZmFzXFxcIiBpZD1cXFwicmVsb2FkaWNvblxcXCIgaWNvbj1cXFwiZm9udDovLyYjeGYyZjE7XFxcIiBzdHlsZT1cXFwiZm9udC1zaXplOiA4ZW07XFxcIiB0YXA9XFxcInJlZnJlc2hMaXN0XFxcIiBpb3MucG9zaXRpb249XFxcInJpZ2h0XFxcIiAvPlxcbiAgICAgICAgICAgIDxBY3Rpb25JdGVtIGNsYXNzPVxcXCJmYXNcXFwiIGlkPVxcXCJjYWxlbmRhcmljb25cXFwiIGljb249XFxcImZvbnQ6Ly8mI3hmMDczO1xcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogOGVtO1xcXCIgdGFwPVxcXCJzaG93TW9udGhEaWFsb2dcXFwiIGlvcy5wb3NpdGlvbj1cXFwicmlnaHRcXFwiIC8+IC0tPlxcbiAgICAgICAgPC9BY3Rpb25CYXI+XFxuICAgIDwvUGFnZS5hY3Rpb25CYXI+XFxuXFxuICAgIDxBYnNvbHV0ZUxheW91dD5cXG4gICAgICAgIDxTdGFja0xheW91dCBoZWlnaHQ9XFxcIjEwMCVcXFwiIHdpZHRoPVxcXCIxMDAlXFxcIj5cXG4gICAgICAgICAgICA8QnV0dG9uIGNsYXNzPVxcXCJidXR0b24ga2FuaXQtdGV4dC1mb250IG0tdC04XFxcIiB0ZXh0PVxcXCIyLTUg4Lib4Li1XFxcIiB0bz1cXFwiXFxcIiB0YXA9XFxcIm5hdmlnYXRlVG9cXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgIDxCdXR0b24gY2xhc3M9XFxcImJ1dHRvbiBrYW5pdC10ZXh0LWZvbnQgbS10LThcXFwiIHRleHQ9XFxcIjYtMTEg4Lib4Li1XFxcIiB0bz1cXFwiYWdlcmFuZ2VcXFwiIHRhcD1cXFwibmF2aWdhdGVUb1xcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwiYnV0dG9uIGthbml0LXRleHQtZm9udCBtLXQtOFxcXCIgdGV4dD1cXFwiMTItMjUg4Lib4Li1XFxcIiB0bz1cXFwiYWdlcmFuZ2VcXFwiIHRhcD1cXFwibmF2aWdhdGVUb1xcXCI+PC9CdXR0b24+XFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICA8L0Fic29sdXRlTGF5b3V0PlxcbjwvUGFnZT5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9hZ2VyYW5nZS1wYWdlL2FnZXJhbmdlLnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL3ZpZXdzL2FnZXJhbmdlLXBhZ2UvYWdlcmFuZ2UueG1sXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IE9ic2VydmFibGUgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIikuT2JzZXJ2YWJsZTtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5jb25zdCBodHRwID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvaHR0cFwiKTtcblxuZnVuY3Rpb24gVXNlcigpIHtcbiAgbGV0IHZpZXdNb2RlbCA9IG5ldyBPYnNlcnZhYmxlKCk7XG4gIC8vIHZpZXdNb2RlbC5zZXQoXCJidG5sb2dpblwiLCBcIuC5gOC4guC5ieC4suC4quC4ueC5iOC4o+C4sOC4muC4mlwiKTtcbiAgbGV0IHJlc3VsdCA9IG5ldyBBcnJheSgpO1xuICB2aWV3TW9kZWwuZ29vZ2xlSWRUb2tlbkluZm8gPSAodG9rZW4pID0+IHtcbiAgICAvLyAgIGNvbnNvbGUubG9nKFwiPT09PT09PT09PT09PT09PT09PT09PT09XCIpO1xuICAgIC8vICAgY29uc29sZS5sb2coXCJodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbmluZm8/aWRfdG9rZW49XCIraWRUb2tlbik7XG4gICAgcmV0dXJuIGh0dHBcbiAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgdXJsOiBcImh0dHBzOi8vb2F1dGgyLmdvb2dsZWFwaXMuY29tL3Rva2VuaW5mbz9pZF90b2tlbj1cIiArIHRva2VuLFxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICB9KVxuICAgICAgLnRoZW4oXG4gICAgICAgIChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgIHZhciByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHJlcyk7XG4gICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgc3RhdHVzQ29kZTogMjAwLFxuICAgICAgICAgICAgZGF0YTogcmVzLFxuICAgICAgICAgIH07XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfSxcbiAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICB9O1xuICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH1cbiAgICAgICk7XG4gIH07XG5cbiAgdmlld01vZGVsLmZhY2Vib29rSWRUb2tlbkluZm8gPSAodG9rZW4pID0+IHtcbiAgICAvLyAgIGNvbnNvbGUubG9nKFwiPT09PT09PT09PT09PT09PT09PT09PT09XCIpO1xuICAgIC8vICAgY29uc29sZS5sb2coXCJodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbmluZm8/aWRfdG9rZW49XCIraWRUb2tlbik7XG4gICAgcmV0dXJuIGh0dHBcbiAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgdXJsOiBcImh0dHBzOi8vZ3JhcGguZmFjZWJvb2suY29tL21lP2FjY2Vzc190b2tlbj1cIiArIHRva2VuLFxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICB9KVxuICAgICAgLnRoZW4oXG4gICAgICAgIChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgIHZhciByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHJlcyk7XG4gICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgc3RhdHVzQ29kZTogMjAwLFxuICAgICAgICAgICAgZGF0YTogcmVzLFxuICAgICAgICAgIH07XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfSxcbiAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICB9O1xuICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH1cbiAgICAgICk7XG4gIH07XG5cbiAgLy8gdmlld01vZGVsLmdldG1pc3BpbiA9ICgpID0+IHtcbiAgLy8gICAgIHJldHVybiBodHRwXG4gIC8vICAgICAgICAgLnJlcXVlc3Qoe1xuICAvLyAgICAgICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS5nZXRtaXNwaW4sXG4gIC8vICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gIC8vICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgLy8gICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAvLyAgICAgICAgICAgICB9LFxuICAvLyAgICAgICAgICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeSh7XG4gIC8vICAgICAgICAgICAgICAgICBwZXJpZDogdmlld01vZGVsLmdldChcInBlcmlkXCIpLFxuICAvLyAgICAgICAgICAgICB9KSxcbiAgLy8gICAgICAgICB9KVxuICAvLyAgICAgICAgIC50aGVuKFxuICAvLyAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgIHZhciByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAvLyAgICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2cocmVzKTtcbiAgLy8gICAgICAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSA9PSAyMDApIHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDIwMCxcbiAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLmRhdGEsXG4gIC8vICAgICAgICAgICAgICAgICAgICAgfTtcbiAgLy8gICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAvLyAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHJlcyxcbiAgLy8gICAgICAgICAgICAgICAgICAgICB9O1xuICAvLyAgICAgICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgLy8gICAgICAgICAgICAgfSxcbiAgLy8gICAgICAgICAgICAgKGUpID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gIC8vICAgICAgICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gIC8vICAgICAgICAgICAgICAgICB9O1xuICAvLyAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgLy8gICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICk7XG4gIC8vIH07XG5cbiAgdmlld01vZGVsLmxvZ2luID0gKHVzZXJuYW1lLCBwYXNzd29yZCkgPT4ge1xuICAgIHJldHVybiBodHRwXG4gICAgICAucmVxdWVzdCh7XG4gICAgICAgIHVybDogZ2xvYmFsLmNvbmYuYXBpLmxvZ2luLFxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gICAgICAgIH0sXG4gICAgICAgIGNvbnRlbnQ6IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgICB1c2VybmFtZTogdXNlcm5hbWUsXG4gICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkLFxuICAgICAgICB9KSxcbiAgICAgIH0pXG4gICAgICAudGhlbihcbiAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgdmFyIHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgcmV0dXJuIHJlcztcbiAgICAgICAgfSxcbiAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICB9O1xuICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH1cbiAgICAgICk7XG4gIH07XG5cbiAgdmlld01vZGVsLnRva2VuID0gKHBheWxvYWQpID0+IHtcbiAgICByZXR1cm4gaHR0cFxuICAgICAgLnJlcXVlc3Qoe1xuICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS50b2tlbixcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAgICAgICB9LFxuICAgICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgaWQ6IHBheWxvYWQuUEFSRU5UX0lELFxuICAgICAgICAgIG5hbWU6IHBheWxvYWQuTkFNRSxcbiAgICAgICAgICBzdXJuYW1lOiBwYXlsb2FkLlNVUk5BTUUsXG4gICAgICAgICAgZW1haWw6IHBheWxvYWQuRU1BSUwsXG4gICAgICAgICAgcGhvbmVudW1iZXI6IHBheWxvYWQuUEhPTkVOVU1CRVIsXG4gICAgICAgIH0pLFxuICAgICAgfSlcbiAgICAgIC50aGVuKFxuICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICB2YXIgcmVzID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKTtcbiAgICAgICAgICBpZiAocmVzLnN0YXR1c0NvZGUgPT0gMjAwKSB7XG4gICAgICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJ0b2tlblwiLCByZXMudG9rZW4pO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gcmVzO1xuICAgICAgICB9LFxuICAgICAgICAoZSkgPT4ge1xuICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgIHN0YXR1c0NvZGU6IDQwMSxcbiAgICAgICAgICAgIGRhdGE6IEpTT04uc3RyaW5naWZ5KGUpLFxuICAgICAgICAgIH07XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfVxuICAgICAgKTtcbiAgfTtcblxuICB2aWV3TW9kZWwucmVmcmVzaCA9IChhY2Nlc3N0b2tlbikgPT4ge1xuICAgIHJldHVybiBodHRwXG4gICAgICAucmVxdWVzdCh7XG4gICAgICAgIHVybDogZ2xvYmFsLmNvbmYuYXBpLnJlZnJlc2h0b2tlbixcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgIEF1dGhvcml6YXRpb246IFwiQmVhcmVyIFwiICsgYWNjZXNzdG9rZW4sXG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgICAgLnRoZW4oXG4gICAgICAgIChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgIHZhciByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSA9PSAyMDApIHsgXG4gICAgICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJ0b2tlblwiLCByZXMuZGF0YS50b2tlbik7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiByZXM7XG4gICAgICAgIH0sXG4gICAgICAgIChlKSA9PiB7XG4gICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAxLFxuICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgfTtcbiAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9XG4gICAgICApO1xuICB9O1xuXG4gIHZpZXdNb2RlbC51c2VyaW5mbyA9IChhY2Nlc3N0b2tlbikgPT4ge1xuICAgIHJldHVybiBodHRwXG4gICAgICAucmVxdWVzdCh7XG4gICAgICAgIHVybDogZ2xvYmFsLmNvbmYuYXBpLnVzZXJpbmZvLFxuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgQXV0aG9yaXphdGlvbjogXCJCZWFyZXIgXCIgKyBhY2Nlc3N0b2tlbixcbiAgICAgICAgfSxcbiAgICAgIH0pXG4gICAgICAudGhlbihcbiAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgdmFyIHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgaWYgKHJlcy5zdGF0dXNDb2RlID09IDIwMCkge1xuICAgICAgICAgICAgYXBwU2V0dGluZ3Muc2V0U3RyaW5nKFwibmFtZVwiLCByZXMuZGF0YS5OQU1FICsgXCIgXCIgKyByZXMuZGF0YS5TVVJOQU1FKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHJlcztcbiAgICAgICAgfSxcbiAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICB9O1xuICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH1cbiAgICAgICk7XG4gIH07XG5cbiAgLy8gdmlld01vZGVsLnByb2ZpbGUgPSAoKSA9PiB7XG4gIC8vICAgICByZXR1cm4gaHR0cFxuICAvLyAgICAgICAgIC5yZXF1ZXN0KHtcbiAgLy8gICAgICAgICAgICAgdXJsOiBnbG9iYWwuY29uZi5hcGkucHJvZmlsZSxcbiAgLy8gICAgICAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAvLyAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gIC8vICAgICAgICAgICAgICAgICBBdXRob3JpemF0aW9uOlxuICAvLyAgICAgICAgICAgICAgICAgICAgIFwiQmVhcmVyIFwiICtcbiAgLy8gICAgICAgICAgICAgICAgICAgICBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJ0b2tlblwiLCBcInRva2VuZXhwaXJlZFwiKSxcbiAgLy8gICAgICAgICAgICAgfSxcbiAgLy8gICAgICAgICB9KVxuICAvLyAgICAgICAgIC50aGVuKFxuICAvLyAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgIHZhciByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAvLyAgICAgICAgICAgICAgICAgaWYgKHJlcy5sZW5ndGggPT0gMCkge1xuICAvLyAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAvLyAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiByZXMsXG4gIC8vICAgICAgICAgICAgICAgICAgICAgfTtcbiAgLy8gICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gIC8vICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAvLyAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiAyMDAsXG4gIC8vICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHJlcyxcbiAgLy8gICAgICAgICAgICAgICAgICAgICB9O1xuICAvLyAgICAgICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgLy8gICAgICAgICAgICAgfSxcbiAgLy8gICAgICAgICAgICAgKGUpID0+IHtcbiAgLy8gICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgLy8gICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gIC8vICAgICAgICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gIC8vICAgICAgICAgICAgICAgICB9O1xuICAvLyAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgLy8gICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICk7XG4gIC8vIH07XG5cbiAgcmV0dXJuIHZpZXdNb2RlbDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBVc2VyO1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4tbW9kZWwuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLW1vZGVsLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsIm1vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5wYWdlXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYWxpZ24taXRlbXNcIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmbGV4LWRpcmVjdGlvblwiLFwidmFsdWVcIjpcImNvbHVtblwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInBvc2l0aW9uXCIsXCJ2YWx1ZVwiOlwicmVsYXRpdmVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiIzQxNWM1YVwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgYmFja2dyb3VuZC1pbWFnZTogdXJsKFxcXCJ+L3NyYy9pbWcvYmFja2dyb3VuZDIuanBnXFxcIik7IFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwczovL2ltYWdlcy5wZXhlbHMuY29tL3Bob3Rvcy8xNjI0NDk2L3BleGVscy1waG90by0xNjI0NDk2LmpwZWcnKTsgXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjsgXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmZvcm1cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJ3aWR0aFwiLFwidmFsdWVcIjpcIjEwMCVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmbGV4LWdyb3dcIixcInZhbHVlXCI6XCIyXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwidmVydGljYWwtYWxpZ25cIixcInZhbHVlXCI6XCJtaWRkbGVcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIubG9nb1wiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1ib3R0b21cIixcInZhbHVlXCI6XCIxMlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImhlaWdodFwiLFwidmFsdWVcIjpcIjEyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiYm9sZFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5oZWFkZXJcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJob3Jpem9udGFsLWFsaWduXCIsXCJ2YWx1ZVwiOlwiY2VudGVyXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1zaXplXCIsXCJ2YWx1ZVwiOlwiMjRcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXdlaWdodFwiLFwidmFsdWVcIjpcIjYwMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1ib3R0b21cIixcInZhbHVlXCI6XCIxOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInRleHQtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGNvbG9yOiAjOGViYmJjOyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1mYW1pbHlcIixcInZhbHVlXCI6XCJcXFwiS2FuaXRcXFwiLCBcXFwiS2FuaXQtTGlnaHRcXFwiXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmlucHV0LXN0eWxlXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwicGFkZGluZ1wiLFwidmFsdWVcIjpcIjEyIDIwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLWJvdHRvbVwiLFwidmFsdWVcIjpcIjhcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIHBsYWNlaG9sZGVyLWNvbG9yOiBsaWdodGdyYXk7IFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgY29sb3I6IHdoaXRlOyBcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuc29jaWFsXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjEwMFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idG4taW1nXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItd2lkdGhcIixcInZhbHVlXCI6XCIxXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE2XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmxvZ2luLWJ1dHRvblwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE4XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC13ZWlnaHRcIixcInZhbHVlXCI6XCJib2xkXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1mYW1pbHlcIixcInZhbHVlXCI6XCJcXFwiS2FuaXRcXFwiLCBcXFwiS2FuaXQtTGlnaHRcXFwiXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjYmRkYWU2XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLXRvcFwiLFwidmFsdWVcIjpcIjhcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIucmVnaXN0ZXItYnV0dG9uXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1zaXplXCIsXCJ2YWx1ZVwiOlwiMThcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXdlaWdodFwiLFwidmFsdWVcIjpcImJvbGRcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcInRvbWF0b1wifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCIyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi10b3BcIixcInZhbHVlXCI6XCI4XCJ9XX1dLFwicGFyc2luZ0Vycm9yc1wiOltdfX07OyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4uY3NzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInN0eWxlXCIsIHBhdGg6IFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBMb2dpblZpZXdNb2RlbCA9IHJlcXVpcmUoXCIuL2xvZ2luLW1vZGVsXCIpO1xuLy8gY29uc3QgZnJvbU9iamVjdCA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2RhdGEvb2JzZXJ2YWJsZVwiKS5mcm9tT2JqZWN0O1xuLy8gbGV0IHVzZXIgPSBMb2dpblZpZXdNb2RlbCgpO1xuY29uc3QgbnN0b2FzdHMgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXRvYXN0c1wiKTtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5jb25zdCB7IHRuc09hdXRoTG9naW4gfSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbHMvYXV0aC1zZXJ2aWNlXCIpO1xuY29uc3QgZGlhbG9nTW9kdWxlID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiKTtcbi8vIGNvbnN0IHsgbG9naW4gfSA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIik7XG5cbmxldCB1c2VyID0gTG9naW5WaWV3TW9kZWwoKTtcbmxldCBwYWdlO1xuXG5sZXQgdXNlcm5hbWU7XG5sZXQgcGFzc3dvcmQ7XG5leHBvcnRzLm9uUGFnZUxvYWRlZCA9IChhcmdzKSA9PiB7XG4gIHBhZ2UgPSBhcmdzLm9iamVjdC5wYWdlO1xuICBwYWdlLmJpbmRpbmdDb250ZXh0ID0gdXNlcjtcbiAgLy8gcGFnZS5iaW5kaW5nQ29udGV4dCA9IHVzZXI7XG4gIHVzZXJuYW1lID0gcGFnZS5nZXRWaWV3QnlJZChcInVzZXJuYW1lXCIpO1xuICBwYXNzd29yZCA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJwYXNzd29yZFwiKTtcbiAgbGV0IHRva2VuID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwidG9rZW5cIiwgXCJ0b2tlbmV4cGlyZWRcIik7XG4gIGlmICh0b2tlbiAhPSBcInRva2VuZXhwaXJlZFwiKSB7XG4gICAgLy8gY29uc29sZS5sb2coXCLguYTguJvguKvguJnguYnguLLguKvguKXguLHguIFcIik7XG4gICAgY29uc3QgbmF2aWdhdGlvbkVudHJ5ID0ge1xuICAgICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9tYWluLXBhZ2UvbWFpblwiLFxuICAgICAgY2xlYXJIaXN0b3J5OiB0cnVlLFxuICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgIG5hbWU6IFwiZmFkZVwiLFxuICAgICAgICBkdXJhdGlvbjogNTAwLFxuICAgICAgICBjdXJ2ZTogXCJsaW5lYXJcIixcbiAgICAgIH0sXG4gICAgfTtcbiAgICBwYWdlLmZyYW1lLm5hdmlnYXRlKG5hdmlnYXRpb25FbnRyeSk7XG4gIH1cbn07XG5cbmV4cG9ydHMub25SZWdpc3RlciA9ICgpID0+IHtcbiAgLy8gY29uc29sZS5sb2coXCJSZWdpc3RlciBQYWdlXCIpO1xuICBjb25zdCBuYXZpZ2F0aW9uRW50cnkgPSB7XG4gICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyXCIsXG4gICAgY2xlYXJIaXN0b3J5OiB0cnVlLFxuICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgIHRyYW5zaXRpb246IHtcbiAgICAgIG5hbWU6IFwic2xpZGVMZWZ0XCIsXG4gICAgICBkdXJhdGlvbjogMzgwLFxuICAgICAgY3VydmU6IFwiZWFzZUluXCIsXG4gICAgfSxcbiAgfTtcbiAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xufTtcblxuZXhwb3J0cy5vbkxvZ2luID0gKCkgPT4ge1xuICBjb25zdCBuYXZpZ2F0aW9uRW50cnkgPSB7XG4gICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9tYWluLXBhZ2UvbWFpblwiLFxuICAgIGNsZWFySGlzdG9yeTogdHJ1ZSxcbiAgICBhbmltYXRlZDogdHJ1ZSxcbiAgICB0cmFuc2l0aW9uOiB7XG4gICAgICBuYW1lOiBcInNsaWRlTGVmdFwiLFxuICAgICAgZHVyYXRpb246IDM4MCxcbiAgICAgIGN1cnZlOiBcImVhc2VJblwiLFxuICAgIH0sXG4gIH07XG4gIHBhZ2UuZnJhbWUubmF2aWdhdGUobmF2aWdhdGlvbkVudHJ5KTtcbiAgLy8gdXNlclxuICAvLyAgIC5sb2dpbih1c2VybmFtZS50ZXh0LCBwYXNzd29yZC50ZXh0KVxuICAvLyAgIC5jYXRjaChmdW5jdGlvbiAoZXJyb3IpIHtcbiAgLy8gICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgLy8gICAgIHJldHVybiBQcm9taXNlLnJlamVjdCgpO1xuICAvLyAgIH0pXG4gIC8vICAgLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gIC8vICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzQ29kZSA9PSAyMDApIHtcbiAgLy8gICAgICAgaWYgKHJlc3BvbnNlLmRhdGEubGVuZ3RoICE9IDApIHtcbiAgLy8gICAgICAgICBsZXQgcGF5bG9hZCA9IHJlc3BvbnNlLmRhdGFbMF07XG4gIC8vICAgICAgICAgY29uc29sZS5sb2cocGF5bG9hZCk7XG4gIC8vICAgICAgICAgLy8g4LmA4LiK4LmH4LiE4Lin4LmI4Liy4Lih4Li1IHRva2VuIOC5geC4peC5ieC4p+C4q+C4o+C4t+C4reC4ouC4seC4h1xuICAvLyAgICAgICAgIC8vIOC4luC5ieC4suC4ouC4seC4h+C5hOC4oeC5iOC4oeC4teC5g+C4q+C5ieC4l+C4s+C4geC4suC4o+C4quC4o+C5ieC4suC4hyB0b2tlbiDguYHguKXguYnguKfguYDguIHguYfguJrguKXguIcgRGF0YWJhc2VcbiAgLy8gICAgICAgICAvLyDguJbguYnguLLguKHguLXguYHguKXguYnguKfguYPguKvguYnguJnguLMgdG9rZW4g4LmA4LiU4Li04Lih4LmE4Lib4LiX4Liz4LiB4Liy4LijIHJlZnJlc2gg4LiB4LmI4Lit4LiZ4LmB4Lil4LmJ4Lin4LiE4LmI4Lit4Lii4LmA4LiB4LmH4Lia4Lil4LiHIERhdGFiYXNlXG4gIC8vICAgICAgICAgaWYgKHBheWxvYWQuVE9LRU4gIT0gXCJcIikge1xuICAvLyAgICAgICAgICAgY29uc29sZS5sb2coXCI9PT1SZWZyZXNoIFRva2VuPT09XCIpO1xuICAvLyAgICAgICAgICAgdXNlclxuICAvLyAgICAgICAgICAgICAucmVmcmVzaChwYXlsb2FkLlRPS0VOKVxuICAvLyAgICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gIC8vICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAvLyAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdCgpO1xuICAvLyAgICAgICAgICAgICB9KVxuICAvLyAgICAgICAgICAgICAudGhlbigocmVmcmVzaHJlc3BvbnNlKSA9PiB7XG4gIC8vICAgICAgICAgICAgICAgaWYgKHJlZnJlc2hyZXNwb25zZS5zdGF0dXNDb2RlID09IDIwMCkge1xuICAvLyAgICAgICAgICAgICAgICAgTG9naW5TdWNjZXNzKCk7XG4gIC8vICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgLy8gICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVmcmVzaCB0b2tlbiBmYWlsZWQhXCIpO1xuICAvLyAgICAgICAgICAgICAgIH1cbiAgLy8gICAgICAgICAgICAgfSk7XG4gIC8vICAgICAgICAgfSBlbHNlIHtcbiAgLy8gICAgICAgICAgIGNvbnNvbGUubG9nKFwiPT09R2VuZXJhdGUgbmV3IHRva2VuPT09XCIpO1xuICAvLyAgICAgICAgICAgdXNlclxuICAvLyAgICAgICAgICAgICAudG9rZW4ocGF5bG9hZClcbiAgLy8gICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAvLyAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgLy8gICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoKTtcbiAgLy8gICAgICAgICAgICAgfSlcbiAgLy8gICAgICAgICAgICAgLnRoZW4oKHRva2VucmVzcG9uc2UpID0+IHtcbiAgLy8gICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0b2tlbnJlc3BvbnNlKTtcbiAgLy8gICAgICAgICAgICAgICBpZiAodG9rZW5yZXNwb25zZS5zdGF0dXNDb2RlID09IDIwMCkge1xuICAvLyAgICAgICAgICAgICAgICAgTG9naW5TdWNjZXNzKCk7XG4gIC8vICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgLy8gICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiR2VuZXJhdGUgbmV3IHRva2VuIGZhaWxlZCFcIik7XG4gIC8vICAgICAgICAgICAgICAgfVxuICAvLyAgICAgICAgICAgICB9KTtcbiAgLy8gICAgICAgICB9XG4gIC8vICAgICAgIH0gZWxzZSB7XG4gIC8vICAgICAgICAgY29uc29sZS5sb2coXCJOb3QgZm91bmQgdXNlciBkYXRhXCIpO1xuICAvLyAgICAgICAgIC8vIGRpYWxvZ01vZHVsZS5hbGVydCh7XG4gIC8vICAgICAgICAgLy8gICBtZXNzYWdlOlxuICAvLyAgICAgICAgIC8vICAgICBcIuC5hOC4oeC5iOC4nuC4muC4guC5ieC4reC4oeC4ueC4peC4nOC4ueC5ieC5g+C4iuC5iSwg4LiB4Lij4Li44LiT4Liy4Lil4LiH4LiX4Liw4LmA4Lia4Li14Lii4LiZ4LmA4Lie4Li34LmI4Lit4LmD4LiK4LmJ4LiH4Liy4LiZ4LiB4LmI4Lit4LiZXCIsXG4gIC8vICAgICAgICAgLy8gICBva0J1dHRvblRleHQ6IFwiT0tcIixcbiAgLy8gICAgICAgICAvLyB9KTtcbiAgLy8gICAgICAgfVxuICAvLyAgICAgfSBlbHNlIHtcbiAgLy8gICAgICAgY29uc29sZS5sb2coXCJMb2cgaW4gZmFpbGVkIVwiKTtcbiAgLy8gICAgICAgZGlhbG9nTW9kdWxlLmFsZXJ0KHtcbiAgLy8gICAgICAgICBtZXNzYWdlOlxuICAvLyAgICAgICAgICAgXCJVc2VybmFtZSDguKvguKPguLfguK0gUGFzc3dvcmQg4LmE4Lih4LmI4LiW4Li54LiB4LiV4LmJ4Lit4LiHLCDguIHguKPguLjguJPguLLguJXguKPguKfguIjguKrguK3guJrguYHguKXguLDguKXguK3guIfguYPguKvguKHguYjguK3guLXguIHguITguKPguLHguYnguIdcIixcbiAgLy8gICAgICAgICBva0J1dHRvblRleHQ6IFwiT0tcIixcbiAgLy8gICAgICAgfSk7XG4gIC8vICAgICB9XG4gIC8vICAgfSk7XG59O1xuXG5leHBvcnRzLm9uR29vZ2xlTG9naW4gPSAoKSA9PiB7XG4gIHRuc09hdXRoTG9naW4oXCJnb29nbGVcIik7XG59O1xuXG5leHBvcnRzLm9uRmFjZWJvb2tMb2dpbiA9ICgpID0+IHtcbiAgdG5zT2F1dGhMb2dpbihcImZhY2Vib29rXCIpO1xufTtcblxuZnVuY3Rpb24gTG9naW5TdWNjZXNzKCkge1xuICB1c2VyXG4gICAgLnVzZXJpbmZvKGFwcFNldHRpbmdzLmdldFN0cmluZyhcInRva2VuXCIpKVxuICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdCgpO1xuICAgIH0pXG4gICAgLnRoZW4oKGluZm9yZXNwb25zZSkgPT4ge1xuICAgICAgLy8gY29uc29sZS5sb2coaW5mb3Jlc3BvbnNlKTtcbiAgICAgIGlmIChpbmZvcmVzcG9uc2Uuc3RhdHVzQ29kZSA9PSAyMDApIHtcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XG4gICAgICAgICAgdGV4dDogXCLguYDguILguYnguLLguKrguLnguYjguKPguLDguJrguJrguKrguLPguYDguKPguYfguIhcIixcbiAgICAgICAgICBkdXJhdGlvbjogbnN0b2FzdHMuRFVSQVRJT04uU0hPUlQsXG4gICAgICAgICAgcG9zaXRpb246IG5zdG9hc3RzLlBPU0lUSU9OLkJPVFRPTSwgLy9vcHRpb25hbFxuICAgICAgICB9O1xuICAgICAgICBuc3RvYXN0cy5zaG93KG9wdGlvbnMpO1xuXG4gICAgICAgIGNvbnN0IG5hdmlnYXRpb25FbnRyeSA9IHtcbiAgICAgICAgICBtb2R1bGVOYW1lOiBcInZpZXdzL21haW4tcGFnZS9tYWluXCIsXG4gICAgICAgICAgY2xlYXJIaXN0b3J5OiB0cnVlLFxuICAgICAgICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgICAgICAgIHRyYW5zaXRpb246IHtcbiAgICAgICAgICAgIG5hbWU6IFwic2xpZGVMZWZ0XCIsXG4gICAgICAgICAgICBkdXJhdGlvbjogMzgwLFxuICAgICAgICAgICAgY3VydmU6IFwiZWFzZUluXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICAgICAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xuICAgICAgfVxuICAgIH0pO1xufVxuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4uanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsIlxubW9kdWxlLmV4cG9ydHMgPSBcIjxQYWdlIGxvYWRlZD1cXFwib25QYWdlTG9hZGVkXFxcIiBhY3Rpb25CYXJIaWRkZW49XFxcInRydWVcXFwiIGFuZHJvaWRTdGF0dXNCYXJCYWNrZ3JvdW5kPVxcXCIjNDE1YzVhXFxcIj5cXG5cXG4gICAgPEZsZXhib3hMYXlvdXQgY2xhc3M9XFxcInBhZ2VcXFwiPlxcbiAgICAgICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJmb3JtIG0tbC04IG0tci04XFxcIj5cXG4gICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImxvZ29cXFwiIHNyYz1cXFwifi9zcmMvaW1nL2ZpcnN0Y2hlY2sucG5nXFxcIiAvPlxcbiAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwiaGVhZGVyXFxcIiB0ZXh0PVxcXCJTZWxmIEVzdGVlbVxcXCIgLz5cXG4gICAgICAgICAgICA8VGV4dEZpZWxkIGlkPVxcXCJ1c2VybmFtZVxcXCIgY2xhc3M9XFxcImthbml0LXRleHQtZm9udCBpbnB1dC1zdHlsZVxcXCIgaGludD1cXFwi4LiK4Li34LmI4Lit4Lic4Li54LmJ4LmD4LiK4LmJXFxcIj48L1RleHRGaWVsZD5cXG4gICAgICAgICAgICA8VGV4dEZpZWxkIGlkPVxcXCJwYXNzd29yZFxcXCIgc2VjdXJlPVxcXCJ0cnVlXFxcIiBjbGFzcz1cXFwia2FuaXQtdGV4dC1mb250IGlucHV0LXN0eWxlXFxcIiBoaW50PVxcXCLguKPguKvguLHguKrguJzguYjguLLguJlcXFwiPjwvVGV4dEZpZWxkPlxcbiAgICAgICAgICAgIDxCdXR0b24gY2xhc3M9XFxcImxvZ2luLWJ1dHRvblxcXCIgdGV4dD1cXFwi4LmA4LiC4LmJ4Liy4Liq4Li54LmI4Lij4Liw4Lia4LiaXFxcIiB0YXA9XFxcIm9uTG9naW5cXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNsYXNzPVxcXCJtLXQtOFxcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJrYW5pdC10ZXh0LWZvbnQtc2VtaWJvbGQgdGV4dC1jZW50ZXJcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiB0ZXh0PVxcXCLguKXguIfguJfguLDguYDguJrguLXguKLguJlcXFwiIGNvbG9yPVxcXCJ3aGl0ZVxcXCIgdGFwPVxcXCJvblJlZ2lzdGVyXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIHRleHQ9XFxcIiB8IFxcXCIgY29sb3I9XFxcIndoaXRlXFxcIiAvPlxcbiAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcImthbml0LXRleHQtZm9udC1zZW1pYm9sZCB0ZXh0LXJpZ2h0XFxcIiBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgdGV4dD1cXFwi4Lil4Li34Lih4Lij4Lir4Lix4Liq4Lic4LmI4Liy4LiZXFxcIiBjb2xvcj1cXFwidG9tYXRvXFxcIiAvPlxcbiAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG5cXG4gICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibS10LTggbS1sLTggbS1yLThcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY29sdW1ucz1cXFwiYXV0bywgYXV0bywgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgaGVpZ2h0PVxcXCIyXFxcIiB3aWR0aD1cXFwiMjUlXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiNhMmEyYTJcXFwiIC8+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1jZW50ZXIgbS1sLTggbS1yLThcXFwiIGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiB0ZXh0PVxcXCJPUiBDT05FQ1QgV0lUSFxcXCIgY29sb3I9XFxcIiNhMmEyYTJcXFwiIC8+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgaGVpZ2h0PVxcXCIyXFxcIiB3aWR0aD1cXFwiMjUlXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiNhMmEyYTJcXFwiIC8+XFxuICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgIDxHcmlkTGF5b3V0IGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgY29sdW1ucz1cXFwiYXV0bywgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwic29jaWFsIG0tci0xNlxcXCIgc3RyZXRjaD1cXFwiZmlsbFxcXCIgdGFwPVxcXCJvbkZhY2Vib29rTG9naW5cXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBzcmM9XFxcIn4vc3JjL2ltZy9mYWNlYm9vay5wbmdcXFwiIHdpZHRoPVxcXCI0OFxcXCIgaGVpZ2h0PVxcXCI0OFxcXCIgYm9kZXJSYWRpdXM9XFxcIjEwMFxcXCIgLz5cXG4gICAgICAgICAgICAgICAgPCEtLSA8SW1hZ2UgY2xhc3M9XFxcIm0tMTYgc29jaWFsXFxcIiBzdHJldGNoPVxcXCJmaWxsXFxcIiBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgc3JjPVxcXCJ+L3NyYy9pbWcvbGluZS5wbmdcXFwiIHdpZHRoPVxcXCI0OFxcXCIgaGVpZ2h0PVxcXCI0OFxcXCIgLz4gLS0+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwic29jaWFsXFxcIiBzdHJldGNoPVxcXCJmaWxsXFxcIiB0YXA9XFxcIm9uR29vZ2xlTG9naW5cXFwiIGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBzcmM9XFxcIn4vc3JjL2ltZy9nb29nbGUucG5nXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIndoaXRlXFxcIiB3aWR0aD1cXFwiNDhcXFwiIGhlaWdodD1cXFwiNDhcXFwiIC8+XFxuICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgPC9GbGV4Ym94TGF5b3V0PlxcbjwvUGFnZT5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9sb2dpbi1wYWdlL2xvZ2luLnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL3ZpZXdzL2xvZ2luLXBhZ2UvbG9naW4ueG1sXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IE9ic2VydmFibGUgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIik7XG5cbmZ1bmN0aW9uIE1haW5EYXRhKCkge1xuICBsZXQgdmlld01vZGVsID0gbmV3IE9ic2VydmFibGUoKTtcbiAgbGV0IHJlc3VsdCA9IGFycmF5KCk7XG4gIHZpZXdNb2RlbC5mYWNlYm9va2xvZ291dCA9ICh0b2tlbikgPT4ge1xuICAgIHJldHVybiBodHRwXG4gICAgICAucmVxdWVzdCh7XG4gICAgICAgIHVybDogXCJodHRwczovL3d3dy5mYWNlYm9vay5jb20vbG9nb3V0LnBocD9hY2Nlc3NfdG9rZW49XCIgKyB0b2tlbixcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgfSlcbiAgICAgIC50aGVuKFxuICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICB2YXIgcmVzID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKTtcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xuICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgIHN0YXR1c0NvZGU6IDIwMCxcbiAgICAgICAgICAgIGRhdGE6IHJlcyxcbiAgICAgICAgICB9O1xuICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH0sXG4gICAgICAgIChlKSA9PiB7XG4gICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAxLFxuICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgfTtcbiAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICB9XG4gICAgICApO1xuICB9O1xuICByZXR1cm4gdmlld01vZGVsO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgTWFpbkRhdGE6IE1haW5EYXRhLFxufTtcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluLXBhZ2UvbWFpbi12aWV3LW1vZGVsLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vdmlld3MvbWFpbi1wYWdlL21haW4tdmlldy1tb2RlbC5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJtb2R1bGUuZXhwb3J0cyA9IHtcInR5cGVcIjpcInN0eWxlc2hlZXRcIixcInN0eWxlc2hlZXRcIjp7XCJydWxlc1wiOlt7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuaGVhZGVyXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBjb2xvcjogd2hpdGU7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInRleHQtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi50YWItbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1TZW1pQm9sZFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxNFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5idXR0b25cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMTVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiI2ZmOGE2NVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiYmxhY2tcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIucmlwcGxlLW1hcmdpbi10b3BcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJ3aWR0aFwiLFwidmFsdWVcIjpcIjEwMCVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tdG9wXCIsXCJ2YWx1ZVwiOlwiMTVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tbGVmdFwiLFwidmFsdWVcIjpcIjE1XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLXJpZ2h0XCIsXCJ2YWx1ZVwiOlwiMTVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiOCA4IDAgMFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5yaXBwbGUtbWFyZ2luLW1pZGRsZVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIndpZHRoXCIsXCJ2YWx1ZVwiOlwiMTAwJVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1sZWZ0XCIsXCJ2YWx1ZVwiOlwiMTVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tcmlnaHRcIixcInZhbHVlXCI6XCIxNVwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgYm9yZGVyLXJhZGl1czogOCA4IDggODsgXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnJpcHBsZS1tYXJnaW4tYm90dG9tXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwid2lkdGhcIixcInZhbHVlXCI6XCIxMDAlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLWxlZnRcIixcInZhbHVlXCI6XCIxNVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1yaWdodFwiLFwidmFsdWVcIjpcIjE1XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjAgMCA4IDhcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIudG9wbWVudS1jb250ZW50XCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXFxcImFwcC92aWV3cy9tYWluLXBhZ2Uvc3JjL2NoZWNraW4uanBnXFxcIikgbm8tcmVwZWF0OyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJ3aWR0aFwiLFwidmFsdWVcIjpcIjEwMCVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJoZWlnaHRcIixcInZhbHVlXCI6XCI1MFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCI4IDggMCAwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXdpZHRoXCIsXCJ2YWx1ZVwiOlwiMCAwIDEgMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiYmxhY2tcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiI2ZmOGE2NVwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5taWRkbGVtZW51LWNvbnRlbnRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGJhY2tncm91bmQtaW1hZ2U6IHVybChcXFwiYXBwL3ZpZXdzL21haW4tcGFnZS9zcmMvY2hlY2tpbi5qcGdcXFwiKSBuby1yZXBlYXQ7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIndpZHRoXCIsXCJ2YWx1ZVwiOlwiMTAwJVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImhlaWdodFwiLFwidmFsdWVcIjpcIjUwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJibGFja1wifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgYm9yZGVyLXJhZGl1czogOCA4IDggODsgXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXdpZHRoXCIsXCJ2YWx1ZVwiOlwiMCAwIDEgMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjZmY4YTY1XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmJvdHRvbW1lbnUtY29udGVudFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIndpZHRoXCIsXCJ2YWx1ZVwiOlwiMTAwJVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImhlaWdodFwiLFwidmFsdWVcIjpcIjUwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjAgMCA4IDhcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcImJsYWNrXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBib3JkZXItd2lkdGg6IDAgMCAxIDA7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCIjZmY4YTY1XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiVGFiU3RyaXBcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcImRhcmtncmF5XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiVGFiU3RyaXBJdGVtLnRhYnN0cmlwaXRlbVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiZGFya2dyYXlcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tdG9wXCIsXCJ2YWx1ZVwiOlwiNFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIlRhYlN0cmlwSXRlbS50YWJzdHJpcGl0ZW06YWN0aXZlXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCIjOGNhOGI0XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIiNmMGZmZmZcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tdG9wXCIsXCJ2YWx1ZVwiOlwiNFwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluLXBhZ2UvbWFpbi5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBNYWluVmlld01vZGVsID0gcmVxdWlyZShcIi4vbWFpbi12aWV3LW1vZGVsXCIpO1xuY29uc3QgZnJvbU9iamVjdCA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2RhdGEvb2JzZXJ2YWJsZVwiKS5mcm9tT2JqZWN0O1xubGV0IHVzZXIgPSBNYWluVmlld01vZGVsO1xuY29uc3QgYXBwU2V0dGluZ3MgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiKTtcbmNvbnN0IG5zdG9hc3RzID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC10b2FzdHNcIik7XG5jb25zdCBkaWFsb2dzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiKTtcbmNvbnN0IHsgdG5zT2F1dGhMb2dvdXQgfSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbHMvYXV0aC1zZXJ2aWNlXCIpO1xuXG5sZXQgcGFnZTtcbmxldCBib3R0b21OYXZpZ2F0aW9uO1xuXG4vLyBTcGlubmVyXG5sZXQgc3Bpbm5lcjtcbmxldCBzcGlubmVyYmFja2dyb3VuZDtcblxubGV0IHNvY2lhbGxvZ287XG5leHBvcnRzLm9uUGFnZUxvYWRlZCA9IChhcmdzKSA9PiB7XG4gIGNvbnN0IHBhZ2UgPSBhcmdzLm9iamVjdC5wYWdlO1xuICAvLyBzb2NpYWxsb2dvID0gcGFnZS5nZXRWaWV3QnlJZChcInNvY2lhbGxvZ29cIik7XG4gIC8vIGJvdHRvbU5hdmlnYXRpb24gPSBwYWdlLmdldFZpZXdCeUlkKFwiYm90dG9tTmF2aWdhdGlvblwiKTtcbiAgLy8gc3Bpbm5lcmJhY2tncm91bmQgPSBwYWdlLmdldFZpZXdCeUlkKFwic3Bpbm5lcmJhY2tncm91bmRcIik7XG4gIC8vIHNwaW5uZXIgPSBwYWdlLmdldFZpZXdCeUlkKFwic3Bpbm5lclwiKTtcblxuICBsZXQgZGF0YSA9IFtdO1xuICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgMTU7IGluZGV4KyspIHtcbiAgICBkYXRhLnB1c2goe1xuICAgICAgaXRlbU5hbWU6IGluZGV4ICsgMSxcbiAgICAgIGl0ZW1EZXNjcmlwdGlvbjogXCJQYXRpZW50IEhOXCIsXG4gICAgfSk7XG4gIH1cblxuICBjb25zdCB2aWV3TW9kZWwgPSBmcm9tT2JqZWN0KHtcbiAgICBkYXRhSXRlbXM6IGRhdGEsXG4gIH0pO1xuICBwYWdlLmJpbmRpbmdDb250ZXh0ID0gdmlld01vZGVsO1xuICAvLyBwcm9jZXNzaW5nRGlzYWJsZSgpO1xuXG4gIC8vIC8vIENoZWNrIFRva2VuIGV4cGlyZWRcbiAgLy8gbGV0IHRva2VuID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwidG9rZW5cIiwgXCJ0b2tlbmV4cGlyZWRcIik7XG4gIC8vIGlmICh0b2tlbiA9PSBcInRva2VuZXhwaXJlZFwiKSB7XG4gIC8vICAgcHJvY2Vzc2luZ0VuYWJsZSgpO1xuICAvLyAgIGNvbnN0IG5hdmlnYXRpb25FbnRyeSA9IHtcbiAgLy8gICAgIG1vZHVsZU5hbWU6IFwidmlld3MvbG9naW4tcGFnZS9sb2dpblwiLFxuICAvLyAgICAgY2xlYXJIaXN0b3J5OiB0cnVlLFxuICAvLyAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gIC8vICAgICB0cmFuc2l0aW9uOiB7XG4gIC8vICAgICAgIG5hbWU6IFwiZmFkZVwiLFxuICAvLyAgICAgICBkdXJhdGlvbjogNTAwLFxuICAvLyAgICAgICBjdXJ2ZTogXCJsaW5lYXJcIixcbiAgLy8gICAgIH0sXG4gIC8vICAgfTtcbiAgLy8gICBwYWdlLmZyYW1lLm5hdmlnYXRlKG5hdmlnYXRpb25FbnRyeSk7XG4gIC8vIH0gZWxzZSB7XG4gIC8vICAgbGV0IHNvY2lhbCA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcInNvY2lhbFwiKTtcbiAgLy8gICBzd2l0Y2ggKHNvY2lhbCkge1xuICAvLyAgICAgY2FzZSBcImdvb2dsZVwiOlxuICAvLyAgICAgICBzb2NpYWxsb2dvLnNyYyA9IFwifi9zcmMvaW1nL2dvb2dsZS5wbmdcIjtcbiAgLy8gICAgICAgYnJlYWs7XG4gIC8vICAgICBjYXNlIFwiZmFjZWJvb2tcIjpcbiAgLy8gICAgICAgc29jaWFsbG9nby5zcmMgPSBcIn4vc3JjL2ltZy9mYWNlYm9vay5wbmdcIjtcbiAgLy8gICAgICAgYnJlYWs7XG4gIC8vICAgICBkZWZhdWx0OlxuICAvLyAgICAgICBicmVhaztcbiAgLy8gICB9XG4gIC8vICAgbGV0IHZpZXdNb2RlbCA9IGZyb21PYmplY3Qoe1xuICAvLyAgICAgZnVsbG5hbWU6IGFwcFNldHRpbmdzLmdldFN0cmluZyhcIm5hbWVcIiksXG4gIC8vICAgfSk7XG4gIC8vICAgcGFnZS5iaW5kaW5nQ29udGV4dCA9IHZpZXdNb2RlbDtcbiAgLy8gICBwcm9jZXNzaW5nRGlzYWJsZSgpO1xuICAvLyB9XG59O1xuXG5leHBvcnRzLm5hdmlnYXRlVG8gPSAoYXJncykgPT4ge1xuICBjb25zdCBuYW1lcGFnZSA9IGFyZ3Mub2JqZWN0LnRvO1xuICBjb25zdCBuYXZpZ2F0aW9uRW50cnkgPSB7XG4gICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9cIiArIG5hbWVwYWdlICsgXCItcGFnZS9cIiArIG5hbWVwYWdlLFxuICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgIHRyYW5zaXRpb246IHtcbiAgICAgIG5hbWU6IFwic2xpZGVMZWZ0XCIsXG4gICAgICBkdXJhdGlvbjogMzgwLFxuICAgICAgY3VydmU6IFwiZWFzZUluXCIsXG4gICAgfSxcbiAgfTtcbiAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xufTtcblxuZXhwb3J0cy5sb2dvdXQgPSAoKSA9PiB7XG4gIC8vIGNvbnNvbGUubG9nKGFwcFNldHRpbmdzLmdldFN0cmluZyhcInRva2VuXCIpKTtcbiAgZGlhbG9nc1xuICAgIC5jb25maXJtKHtcbiAgICAgIHRpdGxlOiBcIuC4leC5ieC4reC4h+C4geC4suC4o+C4reC4reC4geC4iOC4suC4geC4o+C4sOC4muC4mj9cIixcbiAgICAgIC8vIG1lc3NhZ2U6IFwi4LiV4LmJ4Lit4LiH4LiB4Liy4Lij4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4LiaP1wiLFxuICAgICAgb2tCdXR0b25UZXh0OiBcIuC4ouC4t+C4meC4ouC4seC4mVwiLFxuICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCLguKLguIHguYDguKXguLTguIFcIixcbiAgICAgIGNhbmNlbGFibGU6IGZhbHNlLFxuICAgIH0pXG4gICAgLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICBsZXQgdG9rZW4gPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJ0b2tlblwiLCBcInRva2VuZXhwaXJlZFwiKTtcbiAgICAgICAgLy8gdXNlclxuICAgICAgICAvLyAgIC5mYWNlYm9va2xvZ291dCh0b2tlbilcbiAgICAgICAgLy8gICAuY2F0Y2goZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgIC8vICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoKTtcbiAgICAgICAgLy8gICB9KVxuICAgICAgICAvLyAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAvLyAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgICAgICAvLyAgIH0pO1xuXG4gICAgICAgIHRuc09hdXRoTG9nb3V0KCk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4Lia4Liq4Liz4LmA4Lij4LmH4LiIXCIpO1xuICAgICAgICBhcHBTZXR0aW5ncy5jbGVhcigpO1xuICAgICAgICB2YXIgb3B0aW9ucyA9IHtcbiAgICAgICAgICB0ZXh0OiBcIuC4reC4reC4geC4iOC4suC4geC4o+C4sOC4muC4muC4quC4s+C5gOC4o+C5h+C4iFwiLFxuICAgICAgICAgIGR1cmF0aW9uOiBuc3RvYXN0cy5EVVJBVElPTi5TSE9SVCxcbiAgICAgICAgICBwb3NpdGlvbjogbnN0b2FzdHMuUE9TSVRJT04uQk9UVE9NLCAvL29wdGlvbmFsXG4gICAgICAgIH07XG4gICAgICAgIG5zdG9hc3RzLnNob3cob3B0aW9ucyk7XG4gICAgICAgIGNvbnN0IG5hdmlnYXRpb25FbnRyeSA9IHtcbiAgICAgICAgICBtb2R1bGVOYW1lOiBcInZpZXdzL2xvZ2luLXBhZ2UvbG9naW5cIixcbiAgICAgICAgICBjbGVhckhpc3Rvcnk6IHRydWUsXG4gICAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgICAgICAgdHJhbnNpdGlvbjoge1xuICAgICAgICAgICAgbmFtZTogXCJzbGlkZVJpZ2h0XCIsXG4gICAgICAgICAgICBkdXJhdGlvbjogMzgwLFxuICAgICAgICAgICAgY3VydmU6IFwiZWFzZUluXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICAgICAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xuICAgICAgICAvLyBmcmFtZU1vZHVsZS5GcmFtZS50b3Btb3N0KCkubmF2aWdhdGUobmF2aWdhdGlvbkVudHJ5KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4Lii4LiB4LmA4Lil4Li04LiB4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4LiaXCIpO1xuICAgICAgfVxuICAgIH0pO1xufTtcblxuZnVuY3Rpb24gcHJvY2Vzc2luZ0VuYWJsZSgpIHtcbiAgc3Bpbm5lci5idXN5ID0gdHJ1ZTtcbiAgc3Bpbm5lci52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gIHNwaW5uZXJiYWNrZ3JvdW5kLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgYm90dG9tTmF2aWdhdGlvbi5vcGFjaXR5ID0gXCIwLjNcIjtcbn1cblxuZnVuY3Rpb24gcHJvY2Vzc2luZ0Rpc2FibGUoKSB7XG4gIHNwaW5uZXIuYnVzeSA9IGZhbHNlO1xuICBzcGlubmVyLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gIHNwaW5uZXJiYWNrZ3JvdW5kLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gIGJvdHRvbU5hdmlnYXRpb24ub3BhY2l0eSA9IFwiMVwiO1xufVxuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vdmlld3MvbWFpbi1wYWdlL21haW4uanNcIiB9KTtcbiAgICB9KTtcbn0gIiwiZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXdcIiwgZnVuY3Rpb24oKSB7IHJldHVybiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIpOyB9KTtcblxubW9kdWxlLmV4cG9ydHMgPSBcIjxQYWdlIHhtbG5zPVxcXCJodHRwOi8vc2NoZW1hcy5uYXRpdmVzY3JpcHQub3JnL3Rucy54c2RcXFwiIGxvYWRlZD1cXFwib25QYWdlTG9hZGVkXFxcIiB4bWxuczpsdj1cXFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XFxcIj5cXG4gICAgPEFjdGlvbkJhciB0aXRsZT1cXFwiU2VsZiBFc3RlZW1cXFwiPjwvQWN0aW9uQmFyPlxcbiAgICA8IS0tIDxTdGFja0xheW91dCBjbGFzcz1cXFwibS04XFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiPlxcbiAgICAgICAgPGx2OlJhZExpc3RWaWV3IGl0ZW1zPVxcXCJ7e2RhdGFJdGVtc319XFxcIj5cXG4gICAgICAgICAgICA8bHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlPlxcbiAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCBmb250U2l6ZT1cXFwiMjBcXFwiIHRleHQ9XFxcInt7aXRlbU5hbWV9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCBmb250U2l6ZT1cXFwiMTRcXFwiIHRleHQ9XFxcInt7aXRlbURlc2NyaXB0aW9ufX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICA8L2x2OlJhZExpc3RWaWV3PlxcbiAgICA8L1N0YWNrTGF5b3V0PiAtLT5cXG5cXG4gICAgPEFic29sdXRlTGF5b3V0IGJhY2tncm91bmRDb2xvcj1cXFwiI2VlZVxcXCI+XFxuICAgICAgICA8Qm90dG9tTmF2aWdhdGlvbiBpZD1cXFwiYm90dG9tTmF2aWdhdGlvblxcXCIgc2VsZWN0ZWRJbmRleD1cXFwiMFxcXCIgb3BhY2l0eT1cXFwiMVxcXCIgd2lkdGg9XFxcIjEwMCVcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCI+XFxuICAgICAgICAgICAgPFRhYlN0cmlwPlxcbiAgICAgICAgICAgICAgICA8VGFiU3RyaXBJdGVtIGNsYXNzPVxcXCJ0YWJzdHJpcGl0ZW1cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJrYW5pdC10ZXh0LWZvbnRcXFwiIHRleHQ9XFxcIuC4q+C4meC5ieC4suC5geC4o+C4gVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcImZvbnQ6Ly8mI3hmMDE1O1xcXCIgY2xhc3M9XFxcImZhcyB0LTM2XFxcIj48L0ltYWdlPlxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgPFRhYlN0cmlwSXRlbSBjbGFzcz1cXFwidGFic3RyaXBpdGVtXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwia2FuaXQtdGV4dC1mb250XFxcIiB0ZXh0PVxcXCLguIHguLTguIjguIHguKPguKPguKEgXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgPEltYWdlIHNyYz1cXFwiZm9udDovLyYjeGYxYjM7XFxcIiBjbGFzcz1cXFwiZmFzIHQtMzZcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcbiAgICAgICAgICAgICAgICA8VGFiU3RyaXBJdGVtIGNsYXNzPVxcXCJ0YWJzdHJpcGl0ZW1cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJrYW5pdC10ZXh0LWZvbnRcXFwiIHRleHQ9XFxcIuC4leC4seC5ieC4h+C4hOC5iOC4slxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcImZvbnQ6Ly8mI3hmMDEzO1xcXCIgY2xhc3M9XFxcImZhcyB0LTM2XFxcIj48L0ltYWdlPlxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICA8L1RhYlN0cmlwPlxcblxcbiAgICAgICAgICAgIDxUYWJDb250ZW50SXRlbT5cXG4gICAgICAgICAgICAgICAgPExhYmVsIHRleHQ9XFxcIlJhZGxpc3R2aWV3XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibS04XFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3IGl0ZW1zPVxcXCJ7e2RhdGFJdGVtc319XFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiY3JpbXNvblxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IG9yaWVudGF0aW9uPVxcXCJ2ZXJ0aWNhbFxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgZm9udFNpemU9XFxcIjIwXFxcIiB0ZXh0PVxcXCJ7e2l0ZW1OYW1lfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgZm9udFNpemU9XFxcIjE0XFxcIiB0ZXh0PVxcXCJ7e2l0ZW1EZXNjcmlwdGlvbn19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlPlxcbiAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcblxcbiAgICAgICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxuXFxuICAgICAgICAgICAgPFRhYkNvbnRlbnRJdGVtPjwvVGFiQ29udGVudEl0ZW0+XFxuXFxuICAgICAgICAgICAgPFRhYkNvbnRlbnRJdGVtPjwvVGFiQ29udGVudEl0ZW0+XFxuICAgICAgICA8L0JvdHRvbU5hdmlnYXRpb24+XFxuICAgIDwvQWJzb2x1dGVMYXlvdXQ+XFxuPC9QYWdlPlwiOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLnhtbFwiIH0pO1xuICAgIH0pO1xufSAiLCJtb2R1bGUuZXhwb3J0cyA9IHtcInR5cGVcIjpcInN0eWxlc2hlZXRcIixcInN0eWxlc2hlZXRcIjp7XCJydWxlc1wiOlt7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIucGFnZVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImFsaWduLWl0ZW1zXCIsXCJ2YWx1ZVwiOlwiY2VudGVyXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZmxleC1kaXJlY3Rpb25cIixcInZhbHVlXCI6XCJjb2x1bW5cIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJwb3NpdGlvblwiLFwidmFsdWVcIjpcInJlbGF0aXZlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIiM0MTVjNWFcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGJhY2tncm91bmQtaW1hZ2U6IHVybChcXFwifi9zcmMvaW1nL2JhY2tncm91bmQyLmpwZ1xcXCIpOyBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnaHR0cHM6Ly9pbWFnZXMucGV4ZWxzLmNvbS9waG90b3MvMTYyNDQ5Ni9wZXhlbHMtcGhvdG8tMTYyNDQ5Ni5qcGVnJyk7IFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcXG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7IFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5mb3JtXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwid2lkdGhcIixcInZhbHVlXCI6XCIxMDAlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZmxleC1ncm93XCIsXCJ2YWx1ZVwiOlwiMlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInZlcnRpY2FsLWFsaWduXCIsXCJ2YWx1ZVwiOlwibWlkZGxlXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmxvZ29cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tYm90dG9tXCIsXCJ2YWx1ZVwiOlwiMTJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJoZWlnaHRcIixcInZhbHVlXCI6XCI5MFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiYm9sZFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5oZWFkZXJcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJob3Jpem9udGFsLWFsaWduXCIsXCJ2YWx1ZVwiOlwiY2VudGVyXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1zaXplXCIsXCJ2YWx1ZVwiOlwiMThcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXdlaWdodFwiLFwidmFsdWVcIjpcIjYwMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1ib3R0b21cIixcInZhbHVlXCI6XCIyNFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInRleHQtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIiM4ZWJiYmNcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuaW5wdXQtc3R5bGVcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJwYWRkaW5nXCIsXCJ2YWx1ZVwiOlwiMTIgMjBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMjBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tYm90dG9tXCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LUxpZ2h0XFxcIlwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgcGxhY2Vob2xkZXItY29sb3I6IGxpZ2h0Z3JheTsgXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBjb2xvcjogd2hpdGU7IFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5yZWdpc3Rlci1idXR0b25cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiYm9sZFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LUxpZ2h0XFxcIlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwidG9tYXRvXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLXRvcFwiLFwidmFsdWVcIjpcIjhcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuYmFjay1idXR0b25cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiYm9sZFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LUxpZ2h0XFxcIlwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgY29sb3I6IHdoaXRlOyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiI2VlZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCIyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi10b3BcIixcInZhbHVlXCI6XCI4XCJ9XX1dLFwicGFyc2luZ0Vycm9yc1wiOltdfX07OyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL3JlZ2lzdGVyLXBhZ2UvcmVnaXN0ZXIuY3NzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInN0eWxlXCIsIHBhdGg6IFwiLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJsZXQgcGFnZVxuZXhwb3J0cy5vblBhZ2VMb2FkZWQgPSAoYXJncykgPT4ge1xuICAgIHBhZ2UgPSBhcmdzLm9iamVjdC5wYWdlO1xufVxuXG5leHBvcnRzLm9uQmFja1ByZXZpb3VzUGFnZSA9ICgpID0+IHtcbiAgICAvLyBjb25zb2xlLmxvZyhcIkJhY2sgdG8gSG9tZSBQYWdlXCIpO1xuICAgIGNvbnN0IG5hdmlnYXRpb25FbnRyeSA9IHtcbiAgICAgICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9sb2dpbi1wYWdlL2xvZ2luXCIsXG4gICAgICAgIGNsZWFySGlzdG9yeTogdHJ1ZSxcbiAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgICAgIHRyYW5zaXRpb246IHtcbiAgICAgICAgICAgIG5hbWU6IFwic2xpZGVSaWdodFwiLFxuICAgICAgICAgICAgZHVyYXRpb246IDM4MCxcbiAgICAgICAgICAgIGN1cnZlOiBcImVhc2VJblwiLFxuICAgICAgICB9LFxuICAgIH07XG4gICAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xufTsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vdmlld3MvcmVnaXN0ZXItcGFnZS9yZWdpc3Rlci5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJcbm1vZHVsZS5leHBvcnRzID0gXCI8UGFnZSBsb2FkZWQ9XFxcIm9uUGFnZUxvYWRlZFxcXCIgYWN0aW9uQmFySGlkZGVuPVxcXCJ0cnVlXFxcIiBhbmRyb2lkU3RhdHVzQmFyQmFja2dyb3VuZD1cXFwiIzFlNGIyNVxcXCI+XFxuXFxuICAgIDxGbGV4Ym94TGF5b3V0IGNsYXNzPVxcXCJwYWdlXFxcIj5cXG4gICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiZm9ybSBtLWwtOCBtLXItOFxcXCI+XFxuICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJoZWFkZXJcXFwiIHRleHQ9XFxcIuC4peC4h+C4l+C4sOC5gOC4muC4teC4ouC4mVxcXCIgLz5cXG4gICAgICAgICAgICA8VGV4dEZpZWxkIGNsYXNzPVxcXCJrYW5pdC10ZXh0LWZvbnQgaW5wdXQtc3R5bGVcXFwiIGhpbnQ9XFxcIuC4reC4teC5gOC4oeC4pVxcXCI+PC9UZXh0RmllbGQ+XFxuICAgICAgICAgICAgPFRleHRGaWVsZCBjbGFzcz1cXFwia2FuaXQtdGV4dC1mb250IGlucHV0LXN0eWxlXFxcIiBoaW50PVxcXCLguKPguKvguLHguKrguJzguYjguLLguJlcXFwiPjwvVGV4dEZpZWxkPlxcbiAgICAgICAgICAgIDxUZXh0RmllbGQgY2xhc3M9XFxcImthbml0LXRleHQtZm9udCBpbnB1dC1zdHlsZVxcXCIgaGludD1cXFwi4Lii4Li34LiZ4Lii4Lix4LiZ4Lij4Lir4Lix4Liq4Lic4LmI4Liy4LiZXFxcIj48L1RleHRGaWVsZD5cXG4gICAgICAgICAgICA8QnV0dG9uIGNsYXNzPVxcXCJyZWdpc3Rlci1idXR0b25cXFwiIHRleHQ9XFxcIuC4peC4h+C4l+C4sOC5gOC4muC4teC4ouC4mVxcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwiYmFjay1idXR0b25cXFwiIHRleHQ9XFxcIuC4ouC4geC5gOC4peC4tOC4gVxcXCIgdGFwPVxcXCJvbkJhY2tQcmV2aW91c1BhZ2VcXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgPC9GbGV4Ym94TGF5b3V0PlxcbjwvUGFnZT5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9yZWdpc3Rlci1wYWdlL3JlZ2lzdGVyLnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL3ZpZXdzL3JlZ2lzdGVyLXBhZ2UvcmVnaXN0ZXIueG1sXCIgfSk7XG4gICAgfSk7XG59ICJdLCJzb3VyY2VSb290IjoiIn0=