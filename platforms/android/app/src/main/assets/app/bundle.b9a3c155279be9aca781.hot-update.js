webpackHotUpdate("bundle",{

/***/ "./views/main-page/main.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const MainViewModel = __webpack_require__("./views/main-page/main-view-model.js");
const fromObject = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").fromObject;
let user = MainViewModel;
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const nstoasts = __webpack_require__("../node_modules/nativescript-toasts/index.js");
const dialogs = __webpack_require__("../node_modules/@nativescript/core/ui/dialogs/dialogs.js");
const { tnsOauthLogout } = __webpack_require__("./models/auth-service.js");

let page;
let bottomNavigation;

// Spinner
let spinner;
let spinnerbackground;

let sociallogo;
exports.onPageLoaded = (args) => {
  const page = args.object.page;
  sociallogo = page.getViewById("sociallogo");
  bottomNavigation = page.getViewById("bottomNavigation");
  spinnerbackground = page.getViewById("spinnerbackground");
  spinner = page.getViewById("spinner");
  // processingDisable();

  // Check Token expired
  let token = appSettings.getString("token", "tokenexpired");
  if (token == "tokenexpired") {
    processingEnable();
    const navigationEntry = {
      moduleName: "views/login-page/login",
      clearHistory: true,
      animated: true,
      transition: {
        name: "fade",
        duration: 500,
        curve: "linear",
      },
    };
    page.frame.navigate(navigationEntry);
  } else {
    let social = appSettings.getString("social");
    switch (social) {
      case "google":
        sociallogo.src = "~/src/img/google.png";
        break;
      case "facebook":
        sociallogo.src = "~/src/img/facebook.png";
        break;
      default:
        break;
    }
    let viewModel = fromObject({
      fullname: appSettings.getString("name"),
    });
    page.bindingContext = viewModel;
    processingDisable();
  }
};

exports.navigateTo = (args) => {
  const namepage = args.object.to;
  const navigationEntry = {
    moduleName: "views/" + namepage + "-page/" + namepage,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
};

exports.logout = () => {
  // console.log(appSettings.getString("token"));
  dialogs
    .confirm({
      title: "ต้องการออกจากระบบ?",
      // message: "ต้องการออกจากระบบ?",
      okButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
      cancelable: false,
    })
    .then(function (result) {
      if (result) {
        let token = appSettings.getString("token", "tokenexpired");
        // user
        //   .facebooklogout(token)
        //   .catch(function (error) {
        //     console.log(error);
        //     return Promise.reject();
        //   })
        //   .then(function (response) {
        //     console.log(response);
        //   });

        tnsOauthLogout();
        console.log("ออกจากระบบสำเร็จ");
        appSettings.clear();
        var options = {
          text: "ออกจากระบบสำเร็จ",
          duration: nstoasts.DURATION.SHORT,
          position: nstoasts.POSITION.BOTTOM, //optional
        };
        nstoasts.show(options);
        const navigationEntry = {
          moduleName: "views/login-page/login",
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideRight",
            duration: 380,
            curve: "easeIn",
          },
        };
        page.frame.navigate(navigationEntry);
        // frameModule.Frame.topmost().navigate(navigationEntry);
      } else {
        console.log("ยกเลิกออกจากระบบ");
      }
    });
};

function processingEnable() {
  spinner.busy = true;
  spinner.visibility = "visible";
  spinnerbackground.visibility = "visible";
  bottomNavigation.opacity = "0.3";
}

function processingDisable() {
  spinner.busy = false;
  spinner.visibility = "collapse";
  spinnerbackground.visibility = "collapse";
  bottomNavigation.opacity = "1";
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main-page/main.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main-page/main.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi92aWV3cy9tYWluLXBhZ2UvbWFpbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLG9FQUFzQixtQkFBTyxDQUFDLHNDQUFtQjtBQUNqRCxtQkFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDN0Q7QUFDQSxvQkFBb0IsbUJBQU8sQ0FBQyxpRkFBdUM7QUFDbkUsaUJBQWlCLG1CQUFPLENBQUMsOENBQXFCO0FBQzlDLGdCQUFnQixtQkFBTyxDQUFDLDBEQUE2QjtBQUNyRCxPQUFPLGlCQUFpQixHQUFHLG1CQUFPLENBQUMsMEJBQTJCOztBQUU5RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBLGNBQWM7O0FBRWQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLG9EQUFvRDtBQUMvRSxLQUFLO0FBQ0wsQyIsImZpbGUiOiJidW5kbGUuYjlhM2MxNTUyNzliZTlhY2E3ODEuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IE1haW5WaWV3TW9kZWwgPSByZXF1aXJlKFwiLi9tYWluLXZpZXctbW9kZWxcIik7XG5jb25zdCBmcm9tT2JqZWN0ID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCIpLmZyb21PYmplY3Q7XG5sZXQgdXNlciA9IE1haW5WaWV3TW9kZWw7XG5jb25zdCBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCIpO1xuY29uc3QgbnN0b2FzdHMgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXRvYXN0c1wiKTtcbmNvbnN0IGRpYWxvZ3MgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCIpO1xuY29uc3QgeyB0bnNPYXV0aExvZ291dCB9ID0gcmVxdWlyZShcIi4uLy4uL21vZGVscy9hdXRoLXNlcnZpY2VcIik7XG5cbmxldCBwYWdlO1xubGV0IGJvdHRvbU5hdmlnYXRpb247XG5cbi8vIFNwaW5uZXJcbmxldCBzcGlubmVyO1xubGV0IHNwaW5uZXJiYWNrZ3JvdW5kO1xuXG5sZXQgc29jaWFsbG9nbztcbmV4cG9ydHMub25QYWdlTG9hZGVkID0gKGFyZ3MpID0+IHtcbiAgY29uc3QgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gIHNvY2lhbGxvZ28gPSBwYWdlLmdldFZpZXdCeUlkKFwic29jaWFsbG9nb1wiKTtcbiAgYm90dG9tTmF2aWdhdGlvbiA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJib3R0b21OYXZpZ2F0aW9uXCIpO1xuICBzcGlubmVyYmFja2dyb3VuZCA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJzcGlubmVyYmFja2dyb3VuZFwiKTtcbiAgc3Bpbm5lciA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJzcGlubmVyXCIpO1xuICAvLyBwcm9jZXNzaW5nRGlzYWJsZSgpO1xuXG4gIC8vIENoZWNrIFRva2VuIGV4cGlyZWRcbiAgbGV0IHRva2VuID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwidG9rZW5cIiwgXCJ0b2tlbmV4cGlyZWRcIik7XG4gIGlmICh0b2tlbiA9PSBcInRva2VuZXhwaXJlZFwiKSB7XG4gICAgcHJvY2Vzc2luZ0VuYWJsZSgpO1xuICAgIGNvbnN0IG5hdmlnYXRpb25FbnRyeSA9IHtcbiAgICAgIG1vZHVsZU5hbWU6IFwidmlld3MvbG9naW4tcGFnZS9sb2dpblwiLFxuICAgICAgY2xlYXJIaXN0b3J5OiB0cnVlLFxuICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgIG5hbWU6IFwiZmFkZVwiLFxuICAgICAgICBkdXJhdGlvbjogNTAwLFxuICAgICAgICBjdXJ2ZTogXCJsaW5lYXJcIixcbiAgICAgIH0sXG4gICAgfTtcbiAgICBwYWdlLmZyYW1lLm5hdmlnYXRlKG5hdmlnYXRpb25FbnRyeSk7XG4gIH0gZWxzZSB7XG4gICAgbGV0IHNvY2lhbCA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcInNvY2lhbFwiKTtcbiAgICBzd2l0Y2ggKHNvY2lhbCkge1xuICAgICAgY2FzZSBcImdvb2dsZVwiOlxuICAgICAgICBzb2NpYWxsb2dvLnNyYyA9IFwifi9zcmMvaW1nL2dvb2dsZS5wbmdcIjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIFwiZmFjZWJvb2tcIjpcbiAgICAgICAgc29jaWFsbG9nby5zcmMgPSBcIn4vc3JjL2ltZy9mYWNlYm9vay5wbmdcIjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICBicmVhaztcbiAgICB9XG4gICAgbGV0IHZpZXdNb2RlbCA9IGZyb21PYmplY3Qoe1xuICAgICAgZnVsbG5hbWU6IGFwcFNldHRpbmdzLmdldFN0cmluZyhcIm5hbWVcIiksXG4gICAgfSk7XG4gICAgcGFnZS5iaW5kaW5nQ29udGV4dCA9IHZpZXdNb2RlbDtcbiAgICBwcm9jZXNzaW5nRGlzYWJsZSgpO1xuICB9XG59O1xuXG5leHBvcnRzLm5hdmlnYXRlVG8gPSAoYXJncykgPT4ge1xuICBjb25zdCBuYW1lcGFnZSA9IGFyZ3Mub2JqZWN0LnRvO1xuICBjb25zdCBuYXZpZ2F0aW9uRW50cnkgPSB7XG4gICAgbW9kdWxlTmFtZTogXCJ2aWV3cy9cIiArIG5hbWVwYWdlICsgXCItcGFnZS9cIiArIG5hbWVwYWdlLFxuICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgIHRyYW5zaXRpb246IHtcbiAgICAgIG5hbWU6IFwic2xpZGVMZWZ0XCIsXG4gICAgICBkdXJhdGlvbjogMzgwLFxuICAgICAgY3VydmU6IFwiZWFzZUluXCIsXG4gICAgfSxcbiAgfTtcbiAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xufTtcblxuZXhwb3J0cy5sb2dvdXQgPSAoKSA9PiB7XG4gIC8vIGNvbnNvbGUubG9nKGFwcFNldHRpbmdzLmdldFN0cmluZyhcInRva2VuXCIpKTtcbiAgZGlhbG9nc1xuICAgIC5jb25maXJtKHtcbiAgICAgIHRpdGxlOiBcIuC4leC5ieC4reC4h+C4geC4suC4o+C4reC4reC4geC4iOC4suC4geC4o+C4sOC4muC4mj9cIixcbiAgICAgIC8vIG1lc3NhZ2U6IFwi4LiV4LmJ4Lit4LiH4LiB4Liy4Lij4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4LiaP1wiLFxuICAgICAgb2tCdXR0b25UZXh0OiBcIuC4ouC4t+C4meC4ouC4seC4mVwiLFxuICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCLguKLguIHguYDguKXguLTguIFcIixcbiAgICAgIGNhbmNlbGFibGU6IGZhbHNlLFxuICAgIH0pXG4gICAgLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgaWYgKHJlc3VsdCkge1xuICAgICAgICBsZXQgdG9rZW4gPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJ0b2tlblwiLCBcInRva2VuZXhwaXJlZFwiKTtcbiAgICAgICAgLy8gdXNlclxuICAgICAgICAvLyAgIC5mYWNlYm9va2xvZ291dCh0b2tlbilcbiAgICAgICAgLy8gICAuY2F0Y2goZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICAgIC8vICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoKTtcbiAgICAgICAgLy8gICB9KVxuICAgICAgICAvLyAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAvLyAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xuICAgICAgICAvLyAgIH0pO1xuXG4gICAgICAgIHRuc09hdXRoTG9nb3V0KCk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4Lia4Liq4Liz4LmA4Lij4LmH4LiIXCIpO1xuICAgICAgICBhcHBTZXR0aW5ncy5jbGVhcigpO1xuICAgICAgICB2YXIgb3B0aW9ucyA9IHtcbiAgICAgICAgICB0ZXh0OiBcIuC4reC4reC4geC4iOC4suC4geC4o+C4sOC4muC4muC4quC4s+C5gOC4o+C5h+C4iFwiLFxuICAgICAgICAgIGR1cmF0aW9uOiBuc3RvYXN0cy5EVVJBVElPTi5TSE9SVCxcbiAgICAgICAgICBwb3NpdGlvbjogbnN0b2FzdHMuUE9TSVRJT04uQk9UVE9NLCAvL29wdGlvbmFsXG4gICAgICAgIH07XG4gICAgICAgIG5zdG9hc3RzLnNob3cob3B0aW9ucyk7XG4gICAgICAgIGNvbnN0IG5hdmlnYXRpb25FbnRyeSA9IHtcbiAgICAgICAgICBtb2R1bGVOYW1lOiBcInZpZXdzL2xvZ2luLXBhZ2UvbG9naW5cIixcbiAgICAgICAgICBjbGVhckhpc3Rvcnk6IHRydWUsXG4gICAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgICAgICAgdHJhbnNpdGlvbjoge1xuICAgICAgICAgICAgbmFtZTogXCJzbGlkZVJpZ2h0XCIsXG4gICAgICAgICAgICBkdXJhdGlvbjogMzgwLFxuICAgICAgICAgICAgY3VydmU6IFwiZWFzZUluXCIsXG4gICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICAgICAgcGFnZS5mcmFtZS5uYXZpZ2F0ZShuYXZpZ2F0aW9uRW50cnkpO1xuICAgICAgICAvLyBmcmFtZU1vZHVsZS5GcmFtZS50b3Btb3N0KCkubmF2aWdhdGUobmF2aWdhdGlvbkVudHJ5KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4Lii4LiB4LmA4Lil4Li04LiB4Lit4Lit4LiB4LiI4Liy4LiB4Lij4Liw4Lia4LiaXCIpO1xuICAgICAgfVxuICAgIH0pO1xufTtcblxuZnVuY3Rpb24gcHJvY2Vzc2luZ0VuYWJsZSgpIHtcbiAgc3Bpbm5lci5idXN5ID0gdHJ1ZTtcbiAgc3Bpbm5lci52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gIHNwaW5uZXJiYWNrZ3JvdW5kLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgYm90dG9tTmF2aWdhdGlvbi5vcGFjaXR5ID0gXCIwLjNcIjtcbn1cblxuZnVuY3Rpb24gcHJvY2Vzc2luZ0Rpc2FibGUoKSB7XG4gIHNwaW5uZXIuYnVzeSA9IGZhbHNlO1xuICBzcGlubmVyLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gIHNwaW5uZXJiYWNrZ3JvdW5kLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gIGJvdHRvbU5hdmlnYXRpb24ub3BhY2l0eSA9IFwiMVwiO1xufVxuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21haW4tcGFnZS9tYWluLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vdmlld3MvbWFpbi1wYWdlL21haW4uanNcIiB9KTtcbiAgICB9KTtcbn0gIl0sInNvdXJjZVJvb3QiOiIifQ==