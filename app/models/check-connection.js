const Observable = require("tns-core-modules/data/observable").Observable;
const connectivityModule = require("tns-core-modules/connectivity");
const dialogs = require("tns-core-modules/ui/dialogs");

function Connection() {
    let viewModel = new Observable();
    viewModel.checkConnection = () => {
        const type = connectivityModule.getConnectionType();
        let message;
        let connectionStatus = true;
        switch (type) {
            case connectivityModule.connectionType.none:
                message = "No connection!";
                connectionStatus = false;
                break;
            case connectivityModule.connectionType.wifi:
                message = "Wifi connection";
                connectionStatus = true;
                break;
            case connectivityModule.connectionType.mobile:
                message = "3G/4G/5G connection";
                connectionStatus = true;
                break;
            default:
                break;
        }

        connectivityModule.startMonitoring((newConnection) => {
            switch (newConnection) {
                case connectivityModule.connectionType.none:
                    console.log("No connection!");
                    connectionStatus = false;
                    break;
                case connectivityModule.connectionType.wifi:
                    console.log("Connection via wifi");
                    connectionStatus = true;
                    break;
                case connectivityModule.connectionType.mobile:
                    console.log("Connection via 3G/4G/5G");
                    connectionStatus = true;
                    break;
                default:
                    break;
            }
        });

        connectivityModule.stopMonitoring();

        let result = {
            connected: connectionStatus
        };
        return result;
    };
    return viewModel;
}

module.exports = {
    Connection: Connection,
}