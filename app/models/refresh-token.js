const LoginViewModel = require("../views/login-page/login-model");
const appSettings = require("tns-core-modules/application-settings");
let user = LoginViewModel();

function refreshToken() {
  user
    .refresh()
    .catch(function (error) {
      console.log(error);
      return Promise.reject();
    })
    .then(function (response) {
      if (response != null) {
        let data = response.data;
        if (data.status) {
          appSettings.setString("token", data.token);
        } else {
          appSettings.setString("token", "tokenexpired");
        }
      }
    });
}

module.exports = refreshToken;
