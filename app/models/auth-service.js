const LoginViewModel = require("../views/login-page/login-model");
const appSettings = require("tns-core-modules/application-settings");
const { configureTnsOAuth, TnsOAuthClient } = require("nativescript-oauth2");
const {
  TnsOaProvider,
  TnsOaProviderOptionsGoogle,
  TnsOaProviderGoogle,
  TnsOaProviderOptionsFacebook,
  TnsOaProviderFacebook,
} = require("nativescript-oauth2/providers");

let user = LoginViewModel();
let client = null;

exports.configureOAuthProviders = () => {
  const googleProvider = configureOAuthProviderGoogle();
  const facebookProvider = configureOAuthProviderFacebook();
  configureTnsOAuth([googleProvider, facebookProvider]);
};

function configureOAuthProviderGoogle() {
  const googleProviderOptions = {
    openIdSupport: "oid-full",
    clientId:
      "196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth.apps.googleusercontent.com",
    redirectUri:
      "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth:/auth",
    urlScheme:
      "com.googleusercontent.apps.196759620552-o50e3vhru9pmtg10a2p1ntqib1rudhth",
    scopes: ["email"],
  };
  const googleProvider = new TnsOaProviderGoogle(googleProviderOptions);
  return googleProvider;
}

function configureOAuthProviderFacebook() {
  const facebookProviderOptions = {
    openIdSupport: "oid-none",
    clientId: "877532503002711",
    clientSecret: "2e969e6f9036555e114fc9225df55cdb",
    redirectUri: "https://www.facebook.com/connect/login_success.html",
    scopes: ["email"],
  };
  const facebookProvider = new TnsOaProviderFacebook(facebookProviderOptions);
  return facebookProvider;
}

exports.tnsOauthLogin = (providerType) => {
  client = new TnsOAuthClient(providerType);
  client.loginWithCompletion((tokenResult, err) => {
    if (err) {
      console.error("loging in somthings went wrong!");
      console.error(err);
    } else {
      console.log("=====================================");
      if (tokenResult.accessToken != "") {
        let accessToken = tokenResult.accessToken;
        appSettings.setString("token", accessToken);
        switch (providerType) {
          case "google":
            // *** Get info โดยใช้ "idToken"
            let idToken = tokenResult.idToken;
            user
              .googleIdTokenInfo(idToken)
              .catch(function (error) {
                console.log(error);
                return Promise.reject();
              })
              .then(function (response) {
                let key = Object.keys(response.data);
                if (key.length > 2) {
                  appSettings.setString("social", "google");
                  let gmail = response.data.email.split("@");
                  appSettings.setString("name", gmail[0]);
                } else {
                  console.log("Invalid Token");
                }
              });
            break;
          case "facebook":
            // *** Get info โดยใช้ "accesstoken"
            user
              .facebookIdTokenInfo(accessToken)
              .catch(function (error) {
                console.log(error);
                return Promise.reject();
              })
              .then(function (response) {
                let key = Object.keys(response.data);
                if (key[0] != "error") {
                  appSettings.setString("social", "facebook");
                  appSettings.setString("name", response.data.name);
                } else {
                  console.log("Invalid Token");
                }
              });
            break;
          default:
            break;
        }
      }
    }
  });
};

exports.tnsOauthLogout = () => {
  if (client) {
    console.log("===Client===");
    console.log(client);
    client.logout();
  }
};
