config = {
  name: "First Check",
  ver: "Version 1.1 (build 20200616)",

  api: {
    // access: "https://medhr.medicine.psu.ac.th/app-api/v1/?/access",
    // profile: "https://medhr.medicine.psu.ac.th/app-api/v1/?/info",
    // display: "https://medhr.medicine.psu.ac.th/app-api/v1/?/display",

    login: "https://4421bf2fbe5e.ngrok.io/api/login",
    token: "https://4421bf2fbe5e.ngrok.io/api/token",
    refreshtoken: "https://4421bf2fbe5e.ngrok.io/api/refreshtoken",
    userinfo: "https://4421bf2fbe5e.ngrok.io/api/userinfo",
    // getmispin: "https://1aefbf39098f.ngrok.io/api/getmispin",
    // firstcheck: "https://1aefbf39098f.ngrok.io/api/curfirstcheck",
    // firstcheckhistory: "https://1aefbf39098f.ngrok.io/api/history",
    // firstcheckconfirm: "https://1aefbf39098f.ngrok.io/api/confirm",
    // firstcheckduplicated: "https://1aefbf39098f.ngrok.io/api/duplicated",
    // drugname: "https://1aefbf39098f.ngrok.io/api/drugname",
    // drugmistake: "https://1aefbf39098f.ngrok.io/api/drugmistake",
    // newdrugmistake: "https://1aefbf39098f.ngrok.io/api/newdrugmistake",
    // updatedrugmistake:
    //     "https://1aefbf39098f.ngrok.io/api/updatedrugmistake",
    // predispnumber: "https://1aefbf39098f.ngrok.io/api/predispnumber",
    // updatedrugmistakeamount:
    //     "https://1aefbf39098f.ngrok.io/api/updatedrugmistakeamount",
    // sendreport: "https://1aefbf39098f.ngrok.io/api/sendreport",

    // login: "http://61.19.201.20:19539/firstcheck/stafflogin",
    // getmispin: "http://61.19.201.20:19539/firstcheck/getmispin",
    // firstcheck: "http://61.19.201.20:19539/firstcheck/curfirstcheck",
    // firstcheckhistory: "http://61.19.201.20:19539/firstcheck/history",
    // firstcheckconfirm: "http://61.19.201.20:19539/firstcheck/confirm",
    // firstcheckduplicated: "http://61.19.201.20:19539/firstcheck/duplicated",
    // drugname: "http://61.19.201.20:19539/firstcheck/drugname",
    // drugmistake: "http://61.19.201.20:19539/firstcheck/drugmistake",
    // newdrugmistake: "http://61.19.201.20:19539/firstcheck/newdrugmistake",
    // updatedrugmistake: "http://61.19.201.20:19539/firstcheck/updatedrugmistake",
    // predispnumber:"http://61.19.201.20:19539/firstcheck/predispnumber",
    // updatedrugmistakeamount:"http://61.19.201.20:19539/firstcheck/updatedrugmistakeamount",
    // sendreport:"http://61.19.201.20:19539/firstcheck/sendreport"
  },

  updateKey: {
    ios: "lDTkDroiiAWdOa89adEaL37vALFFpbVYic0w5",
    android: "u0LSSDirU2FAgOpymSeo9WREqxo9pbVYic0w5",
  },
};

module.exports = config;
