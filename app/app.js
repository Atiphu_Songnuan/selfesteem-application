let application = require("tns-core-modules/application");
const appSettings = require("tns-core-modules/application-settings");
const LiveUpdate = require("./models/liveUpdate");
const { configureOAuthProviders } = require("./models/auth-service");
global.conf = require("./configs");

new LiveUpdate(global.conf.updateKey);
configureOAuthProviders();

let token = appSettings.getString("token", "tokenexpired");
// console.log("Root-User Token: " + token);
if (token != "tokenexpired") {
    application.run({ moduleName: "app-root-main" });
} else { 
    application.run({ moduleName: "app-root" });
}


/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
