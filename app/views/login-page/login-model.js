const Observable = require("tns-core-modules/data/observable").Observable;
const appSettings = require("tns-core-modules/application-settings");
const http = require("tns-core-modules/http");

function User() {
  let viewModel = new Observable();
  // viewModel.set("btnlogin", "เข้าสู่ระบบ");
  let result = new Array();
  viewModel.googleIdTokenInfo = (token) => {
    //   console.log("========================");
    //   console.log("https://oauth2.googleapis.com/tokeninfo?id_token="+idToken);
    return http
      .request({
        url: "https://oauth2.googleapis.com/tokeninfo?id_token=" + token,
        method: "GET",
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          // console.log(res);
          result = {
            statusCode: 200,
            data: res,
          };
          return result;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.facebookIdTokenInfo = (token) => {
    //   console.log("========================");
    //   console.log("https://oauth2.googleapis.com/tokeninfo?id_token="+idToken);
    return http
      .request({
        url: "https://graph.facebook.com/me?access_token=" + token,
        method: "GET",
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          // console.log(res);
          result = {
            statusCode: 200,
            data: res,
          };
          return result;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  // viewModel.getmispin = () => {
  //     return http
  //         .request({
  //             url: global.conf.api.getmispin,
  //             method: "POST",
  //             headers: {
  //                 "Content-Type": "application/json",
  //             },
  //             content: JSON.stringify({
  //                 perid: viewModel.get("perid"),
  //             }),
  //         })
  //         .then(
  //             (response) => {
  //                 var res = response.content.toJSON();
  //                 // console.log(res);
  //                 if (res.statusCode == 200) {
  //                     result = {
  //                         statusCode: 200,
  //                         data: res.data,
  //                     };
  //                 } else {
  //                     result = {
  //                         statusCode: 400,
  //                         data: res,
  //                     };
  //                 }
  //                 return result;
  //             },
  //             (e) => {
  //                 result = {
  //                     statusCode: 401,
  //                     data: JSON.stringify(e),
  //                 };
  //                 return result;
  //             }
  //         );
  // };

  viewModel.login = (username, password) => {
    return http
      .request({
        url: global.conf.api.login,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        content: JSON.stringify({
          username: username,
          password: password,
        }),
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.token = (payload) => {
    return http
      .request({
        url: global.conf.api.token,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        content: JSON.stringify({
          id: payload.PARENT_ID,
          name: payload.NAME,
          surname: payload.SURNAME,
          email: payload.EMAIL,
          phonenumber: payload.PHONENUMBER,
        }),
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          if (res.statusCode == 200) {
            appSettings.setString("token", res.token);
          }
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.refresh = (accesstoken) => {
    return http
      .request({
        url: global.conf.api.refreshtoken,
        method: "POST",
        headers: {
          Authorization: "Bearer " + accesstoken,
        },
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          if (res.statusCode == 200) { 
            appSettings.setString("token", res.data.token);
          }
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  viewModel.userinfo = (accesstoken) => {
    return http
      .request({
        url: global.conf.api.userinfo,
        method: "POST",
        headers: {
          Authorization: "Bearer " + accesstoken,
        },
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          if (res.statusCode == 200) {
            appSettings.setString("name", res.data.NAME + " " + res.data.SURNAME);
          }
          return res;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };

  // viewModel.profile = () => {
  //     return http
  //         .request({
  //             url: global.conf.api.profile,
  //             method: "GET",
  //             headers: {
  //                 Authorization:
  //                     "Bearer " +
  //                     appSettings.getString("token", "tokenexpired"),
  //             },
  //         })
  //         .then(
  //             (response) => {
  //                 var res = response.content.toJSON();
  //                 if (res.length == 0) {
  //                     result = {
  //                         statusCode: 400,
  //                         data: res,
  //                     };
  //                 } else {
  //                     result = {
  //                         statusCode: 200,
  //                         data: res,
  //                     };
  //                 }
  //                 return result;
  //             },
  //             (e) => {
  //                 result = {
  //                     statusCode: 401,
  //                     data: JSON.stringify(e),
  //                 };
  //                 return result;
  //             }
  //         );
  // };

  return viewModel;
}

module.exports = User;
