const LoginViewModel = require("./login-model");
// const fromObject = require("tns-core-modules/data/observable").fromObject;
// let user = LoginViewModel();
const nstoasts = require("nativescript-toasts");
const appSettings = require("tns-core-modules/application-settings");
const { tnsOauthLogin } = require("../../models/auth-service");
const dialogModule = require("tns-core-modules/ui/dialogs");
// const { login } = require("tns-core-modules/ui/dialogs");

let user = LoginViewModel();
let page;

let username;
let password;
exports.onPageLoaded = (args) => {
  page = args.object.page;
  page.bindingContext = user;
  // page.bindingContext = user;
  username = page.getViewById("username");
  password = page.getViewById("password");
  let token = appSettings.getString("token", "tokenexpired");
  if (token != "tokenexpired") {
    // console.log("ไปหน้าหลัก");
    const navigationEntry = {
      moduleName: "views/main-page/main",
      clearHistory: true,
      animated: true,
      transition: {
        name: "fade",
        duration: 500,
        curve: "linear",
      },
    };
    page.frame.navigate(navigationEntry);
  }
};

exports.onRegister = () => {
  // console.log("Register Page");
  const navigationEntry = {
    moduleName: "views/register-page/register",
    clearHistory: true,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
};

exports.onLogin = () => {
  const navigationEntry = {
    moduleName: "views/main-page/main",
    clearHistory: true,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
  // user
  //   .login(username.text, password.text)
  //   .catch(function (error) {
  //     console.log(error);
  //     return Promise.reject();
  //   })
  //   .then(function (response) {
  //     if (response.statusCode == 200) {
  //       if (response.data.length != 0) {
  //         let payload = response.data[0];
  //         console.log(payload);
  //         // เช็คว่ามี token แล้วหรือยัง
  //         // ถ้ายังไม่มีให้ทำการสร้าง token แล้วเก็บลง Database
  //         // ถ้ามีแล้วให้นำ token เดิมไปทำการ refresh ก่อนแล้วค่อยเก็บลง Database
  //         if (payload.TOKEN != "") {
  //           console.log("===Refresh Token===");
  //           user
  //             .refresh(payload.TOKEN)
  //             .catch((error) => {
  //               console.log(error);
  //               return Promise.reject();
  //             })
  //             .then((refreshresponse) => {
  //               if (refreshresponse.statusCode == 200) {
  //                 LoginSuccess();
  //               } else {
  //                 console.log("Refresh token failed!");
  //               }
  //             });
  //         } else {
  //           console.log("===Generate new token===");
  //           user
  //             .token(payload)
  //             .catch((error) => {
  //               console.log(error);
  //               return Promise.reject();
  //             })
  //             .then((tokenresponse) => {
  //               console.log(tokenresponse);
  //               if (tokenresponse.statusCode == 200) {
  //                 LoginSuccess();
  //               } else {
  //                 console.log("Generate new token failed!");
  //               }
  //             });
  //         }
  //       } else {
  //         console.log("Not found user data");
  //         // dialogModule.alert({
  //         //   message:
  //         //     "ไม่พบข้อมูลผู้ใช้, กรุณาลงทะเบียนเพื่อใช้งานก่อน",
  //         //   okButtonText: "OK",
  //         // });
  //       }
  //     } else {
  //       console.log("Log in failed!");
  //       dialogModule.alert({
  //         message:
  //           "Username หรือ Password ไม่ถูกต้อง, กรุณาตรวจสอบและลองใหม่อีกครั้ง",
  //         okButtonText: "OK",
  //       });
  //     }
  //   });
};

exports.onGoogleLogin = () => {
  tnsOauthLogin("google");
};

exports.onFacebookLogin = () => {
  tnsOauthLogin("facebook");
};

function LoginSuccess() {
  user
    .userinfo(appSettings.getString("token"))
    .catch((error) => {
      console.log(error);
      return Promise.reject();
    })
    .then((inforesponse) => {
      // console.log(inforesponse);
      if (inforesponse.statusCode == 200) {
        let options = {
          text: "เข้าสู่ระบบสำเร็จ",
          duration: nstoasts.DURATION.SHORT,
          position: nstoasts.POSITION.BOTTOM, //optional
        };
        nstoasts.show(options);

        const navigationEntry = {
          moduleName: "views/main-page/main",
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 380,
            curve: "easeIn",
          },
        };
        page.frame.navigate(navigationEntry);
      }
    });
}
