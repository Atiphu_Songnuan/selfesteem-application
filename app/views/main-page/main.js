const MainViewModel = require("./main-view-model");
const fromObject = require("tns-core-modules/data/observable").fromObject;
let user = MainViewModel;
const appSettings = require("tns-core-modules/application-settings");
const nstoasts = require("nativescript-toasts");
const dialogs = require("tns-core-modules/ui/dialogs");
const { tnsOauthLogout } = require("../../models/auth-service");

let page;
let bottomNavigation;

// Spinner
let spinner;
let spinnerbackground;

let sociallogo;
exports.onPageLoaded = (args) => {
  page = args.object.page;
  sociallogo = page.getViewById("sociallogo");
  bottomNavigation = page.getViewById("bottomNavigation");
  spinnerbackground = page.getViewById("spinnerbackground");
  spinner = page.getViewById("spinner");
  processingDisable();

  // // Check Token expired
  // let token = appSettings.getString("token", "tokenexpired");
  // if (token == "tokenexpired") {
  //   processingEnable();
  //   const navigationEntry = {
  //     moduleName: "views/login-page/login",
  //     clearHistory: true,
  //     animated: true,
  //     transition: {
  //       name: "fade",
  //       duration: 500,
  //       curve: "linear",
  //     },
  //   };
  //   page.frame.navigate(navigationEntry);
  // } else {
  //   let social = appSettings.getString("social");
  //   switch (social) {
  //     case "google":
  //       sociallogo.src = "~/src/img/google.png";
  //       break;
  //     case "facebook":
  //       sociallogo.src = "~/src/img/facebook.png";
  //       break;
  //     default:
  //       break;
  //   }
  //   let viewModel = fromObject({
  //     fullname: appSettings.getString("name"),
  //   });
  //   page.bindingContext = viewModel;
  //   processingDisable();
  // }
};

exports.navigateTo = (args) => {
  const namepage = args.object.to;
  const navigationEntry = {
    moduleName: "views/" + namepage + "-page/" + namepage,
    animated: true,
    transition: {
      name: "slideLeft",
      duration: 380,
      curve: "easeIn",
    },
  };
  page.frame.navigate(navigationEntry);
};

exports.logout = () => {
  // console.log(appSettings.getString("token"));
  dialogs
    .confirm({
      title: "ต้องการออกจากระบบ?",
      // message: "ต้องการออกจากระบบ?",
      okButtonText: "ยืนยัน",
      cancelButtonText: "ยกเลิก",
      cancelable: false,
    })
    .then(function (result) {
      if (result) {
        let token = appSettings.getString("token", "tokenexpired");
        // user
        //   .facebooklogout(token)
        //   .catch(function (error) {
        //     console.log(error);
        //     return Promise.reject();
        //   })
        //   .then(function (response) {
        //     console.log(response);
        //   });

        tnsOauthLogout();
        console.log("ออกจากระบบสำเร็จ");
        appSettings.clear();
        var options = {
          text: "ออกจากระบบสำเร็จ",
          duration: nstoasts.DURATION.SHORT,
          position: nstoasts.POSITION.BOTTOM, //optional
        };
        nstoasts.show(options);
        const navigationEntry = {
          moduleName: "views/login-page/login",
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideRight",
            duration: 380,
            curve: "easeIn",
          },
        };
        page.frame.navigate(navigationEntry);
        // frameModule.Frame.topmost().navigate(navigationEntry);
      } else {
        console.log("ยกเลิกออกจากระบบ");
      }
    });
};

function processingEnable() {
  spinner.busy = true;
  spinner.visibility = "visible";
  spinnerbackground.visibility = "visible";
  bottomNavigation.opacity = "0.3";
}

function processingDisable() {
  spinner.busy = false;
  spinner.visibility = "collapse";
  spinnerbackground.visibility = "collapse";
  bottomNavigation.opacity = "1";
}
