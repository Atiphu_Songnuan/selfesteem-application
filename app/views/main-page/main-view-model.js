const Observable = require("tns-core-modules/data/observable");

function MainData() {
  let viewModel = new Observable();
  let result = array();
  viewModel.facebooklogout = (token) => {
    return http
      .request({
        url: "https://www.facebook.com/logout.php?access_token=" + token,
        method: "GET",
      })
      .then(
        (response) => {
          var res = response.content.toJSON();
          console.log(res);
          result = {
            statusCode: 200,
            data: res,
          };
          return result;
        },
        (e) => {
          result = {
            statusCode: 401,
            data: JSON.stringify(e),
          };
          return result;
        }
      );
  };
  return viewModel;
}

module.exports = {
  MainData: MainData,
};
