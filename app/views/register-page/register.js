let page
exports.onPageLoaded = (args) => {
    page = args.object.page;
}

exports.onBackPreviousPage = () => {
    // console.log("Back to Home Page");
    const navigationEntry = {
        moduleName: "views/login-page/login",
        clearHistory: true,
        animated: true,
        transition: {
            name: "slideRight",
            duration: 380,
            curve: "easeIn",
        },
    };
    page.frame.navigate(navigationEntry);
}