// Spinner
let spinnerbackground;
let spinner;

let page;
exports.onPageLoaded = (args) => {
  page = args.object.page;
  // spinnerbackground = page.getViewById("spinnerbackground");
  // spinner = page.getViewById("spinner");
  // processingDisable();
};

exports.back = () => {
  console.log("Back to homepage");
  page.frame.goBack();
};

function processingEnable() {
  spinner.busy = true;
  spinner.visibility = "visible";
  spinnerbackground.visibility = "visible";
  //   bottomNavigation.opacity = "0.3";
}

function processingDisable() {
  spinner.busy = false;
  spinner.visibility = "collapse";
  spinnerbackground.visibility = "collapse";
  //   bottomNavigation.opacity = "1";
}
